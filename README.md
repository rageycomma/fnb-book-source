## FnB "Hungry For Peace"

### First things first
This book is copyrighted. Personally, I do not understand the reason for this being copyrighted, however, we are where we are. This feels far more at home with a copyleft, or creative commons license. I shall endeavour to try and convince FnB's founders to do so, and to try and sell the book on a solidarity basis.

The reason I created this was, I am starting a chapter of Food Not Bombs, and the nature of the information is sparse, difficult to find, and naturally, therefore, an impediment to those people who want to start a chapter. 

In creating the database of chapters, I have found many chapters which, when I looked for more of their information, stopped their operations a few months after they began. Looking at the website, it's clear the information is partly there, but then - I looked here to the book and it has everything that is required.

I'm aware of the fact there is a need to have a shop to sell this, however, it strikes me - the book is laid out like a newspaper, and is difficult for people like me, to read. Secondarily, this information needs to be as accessible as possible, and it was originally published in a PDF, which is not an open format - it is Adobe's proprietary format.

Not just that but, this book should be translated, and available to everyone who needs it. Doing that is very hard when it's a PDF and in two columns.

The intent of this project is not *at all* to deprive Keith McHenry or any of those amazing people who work on Food Not Bombs, and who worked on this book, of an income to support them, it is expressly designed to do the opposite. Making this book more accessible and translatable to audiences makes it easier for existing chapters to read it, and new chapters to start, but also so that this book can very easily be printed independently by groups here, for instance, with printing facilities, but also so that it can be more widely disseminated.

The intent of this came from love, not from greed or a desire to deprive anyone of anything, but the exact opposite. 


### Import progress

| Section             | Has been proof-read? | Has been formatted? | Has all images? | Consistency/accuracy                |
| ------------------- | -------------------- | ------------------- | --------------- | ----------------------------------- |
| 0 - An Introduction | No                   | No                  | Yes             | Not checked                         |
| 0 - Copyright       | Yes                  | Yes                 | N/A             | Not checked                         |
| 0 - Foreword        | No                   | Yes                 | Yes             | Not checked                         |
| 0 - Preface         | No                   | Yes                 | Yes             | Not checked                         |
| 0 - TableOfContents | No                   | No                  | No              | Not checked                         |
| 1 - Introduction    | Yes                  | Yes                 | Yes             | Yes (see below)                     |
| 2 - Logistics       | No                   | No                  | No              | Not checked                         |

### Factual issues / clarifications

#### 1 - Introduction

**Clarification on 2009 study**:


- "*A study published in 2009 reported that there was enough discarded food to provide all one billion hungry people with the nutrition they require to be healthy. Another study that same year showed that over 40 percent of the food produced in the United States was discarded.* "

- "A 2009 study" that says 1 billion hungry Americans could be fed with the excess. However, I think this derives from the Oxfam Briefing Paper for 2009 (https://oi-files-d8-prod.s3.eu-west-2.amazonaws.com/s3fs-public/file_attachments/bp127-billion-hungry-people-0901_9.pdf), which is the Global Hunger Index of the WHO, which states in 2009, 1 billion people went hungry.  This isn't a direct quotation from any study, it is a rough conclusion drawn from several. 
  - In 2009, the Food and Agriculture Organisation (FAO) stated that: "*Roughly one-third of the edible parts of food produced for human consumption, gets lost or wasted globally, which is about 1.3 billion ton per year*"
  - FAO states (http://www.fao.org/3/i3347e/i3347e.pdf): "*The global volume of food wastage is estimated to be 1.6 Gtonnes of “primary product equivalents”, while the total wastage for the edible part of food is 1.3 Gtonnes. This amount can be weighed against total agricultural production (for food and non-food uses), which is about 6 Gtonnes.*"
  - Therefore if 1.3 Gtonnes is wasted, but 6 Gtonnes is made, that's 21% of production. 21% of world population is roughly 1.3 billion. 
  - <u>Action taken</u> - Refactored the paragraph to indicate this isn't directly quoted from a study.
- Study also states it is 31% of all food, or 133 billion lbs of food wasted, so 40% isn't accurate.

**Clarification on terminology**

* The heading is titled "Section", yet, in the text, it says "in this chapter". 
* <u>Action taken:</u> removed "section" and just titled it "Chapter".


#### 2 - Logistics

###### **"Steps"**

* I re-ordered these steps and made them somewhat more consistent, as some of them were suggesting to go out and speak to people when you were not aware of what places are available for food, how much is available, and therefore how many people would be required to process this food. 
* I added some pertinent questions to ask and to consider the logistics of the things first, because from looking at a lot of the chapters, many start out struggling to find sources and places to distribute their food.
* I also restructured this text because it had outdated technology in it, and also referred to Google and Yahoo. These are not good places to use for a chapter. They can, will, and *do* give information to federal authorities and are funding one half of the political machinations in the United States, so it's definitely not a good idea. 
* I also added information I have seen across lots of chapters - lack of spices, and also my idea, which is to use your local street as a source of leftover food which can easily be overlooked.

**Outreach**

* There is some duplication of information here, where this has been duplicated I have removed it, as it makes more sense to tell the person how to set up all the information at the beginning, and then simply refer to that.
* Removed some outdated terms like "web" and replaced with internet, included e-mail in that, as, really - e-mail is not a particularly popular medium now.
* Replaced the use of the term "store" for "shop", as this is more common outside of the United States, yet communicates the same reason.
* "Toll-free" doesn't mean anything outside of the United States - removed it and just reference "contact number".
* Removed multiple duplicated references and inline citation of website as it can just be placed in the suffix.

##### Food distribution

* Changed from quarts to gallons, since it's a quarter gallon, as this is easier to then directly convert into litres.

**The Kitchen**

* Changed some references to aluminium, which is a difficult subject. Oof, where to begin. So, firstly, this does apply to aluminium pans but does not apply to anodized aluminium pans according to evidence.  
  * Stahl, Falk, et al. (2017) does show that preparing acidic food (fish with lemon juice) in an aluminium pan *definitely* leads to a largely-elevated dose of aluminium in your food, particularly in specific brands (presumably cheap ones)
    * This was tested with regular cookware, camping cookware *and* with aluminium trays.
    * "*The BfR does recommend that consumers avoid the use of aluminum pots or dishes for acidic or salted foodstuffs such as apple sauce, rhubarb, tomato puree, or salt herring due to the increased solubility of aluminum under the influence of acids and salts, thus prophylactically avoiding the “unnecessary ingestion” of aluminum* "

**The Kitchen**

* Removed self-reference saying "The following is a general guide." - this is kinda implied with the entire chapter, I don't think there's much need to re-reference this.
* There is a reference to Propane, but in some countries, propane storage and transportation is problematic and could lead to local authorities cracking down on you. There are dangers associated with storing and using propane if done improperly - roughly 270 people a year die from explosions, fires, or inhalation of propane so - a little information up-front could save lives, and I think this is a justifiable reason for its inclusion.

**Food preparation**

* Re-structure "Also, try to avoid having the pots in a situation which might lead to arrest." which doesn't really make it clear what will happen. I expanded that, it's cheaper to replace plastic containers than it is to replace pots which often are more than a hundred dollars/ pounds. 
* Removed references to "management structure", replaced with the intention of the paragraphs of self-directed and collectively-determined roles.
* Restructured paragraph to actually define what a "head cook" and "bottom liner" are, before they are referenced several times. 
* Restructured around "delivery team" and "food team" again used before being defined. 
* Removed references to "cooks" in favour of "cooking team" since that's the terminology being used.

**Outdoor tables and field kitchens**

* Changed "fully equipped" to "fully-equipped"
* Made clarification of "clean and fully-equipped" as in many countries, sanitation is a large issue which is enforced by local authorities and would likely be the full 



**Street Theatre**

* Fixed typo with papier-mâchė.
* Clarified square locations in Boston to United States and included state for further clarification.
* Changed "pocketbook" to "budget", since the phrase is unused outside of the United States.
* Changed tense after "are limited to your imagination", because it is in a speculative mood, then immediately 
* Remove reference to "first" in the sentence "First, this means that our volunteers select the most visible locations to set up our table.", because there is no enumeration beyond this.
* Clarified "people at global coordination office " to a proper name.

**Fundraising**

* Refactored clunky section on resources available on the FnB website, as it breaks up the flow of the segment and is also overly enumerative.

****



### Remaining to import

| Section             | Has been imported?   |
|---------------------|----------------------|
| 3 - Story of FnB    | No                   |
| 4 - Story of FnB    | No                   |
| Afterword           | No                   |
| Appendices          | No                   |
