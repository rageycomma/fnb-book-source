# Table of contents

Foreword by Howard Zinn

Preface 

Introduction - After the riots



## Section One

Solidarity Not Charity

A New Society

Non-violence in Theory

Non-violence in Practice

Food

Not Bombs

How *Food Not Bombs* got its name



## Section Two

Logistics

Seven Steps to Organising a local *Food Not Bombs* group

The "Office"

Decision-making

Outreach

Food Collection

Food Distribution

The Kitchen

Outdoor Tables and Field Kitchen

Beyond Basic Food Collection and Distribution

Street Theater

Fundraising

Catering

Concerts, Protests, Gatherings and other *Food Not Bombs* events

Models used to organise a local *Food Not Bombs* group

*Food Not Bombs* and the state

Non-profit, tax-exempt status

Legal tips

The revolution doesn't need a permit

If the government tries to shut down your meal

If the government arrests you

Support people

Call the media

Jail Solidarity

Courts and Trials

False Allegations of Terrorism

Basic Steps to Effective Community Organizing 

Organizing Literature Tables, Meetings, Events, Tours and Gatherings

How to Have a Successful Literature Table

Packing the Literature Box 

How to Organize a Meeting 

How to Organize an Event

How to Organize a Tour 

How to Organize a Gathering 

Planning a Campaign of Nonviolent Direct Action

Basic Types of Nonviolent Direct Actions

Marches

Vigils and Tent City Protests

Occupations and Flash Mobs

Blockades and Lockdowns

Risking Arrest Sharing Free Meals

Strikes and Walkouts 

Food Not Bombs Relief Efforts

Projects of Food Not Bombs

Food Not Lawns 

Homes Not Jails 

Free Radio

Indymedia

Bikes Not Bombs

## Section Three

The Story of Food Not Bombs

The First Food Not Bombs Chapter 

The Second Food Not Bombs Chapter

Food Not Bombs Becomes Global

The Next Thirty Years of Food Not Bombs

## Section Four

Food Not Bombs Vegan Recipes

Food Logistics

Equipment

Tips on Cooking for Large Numbers of People

Recovering Food and Shopping

Food Handling and Storage

Composting

Food Not Bombs Recipes

Drinks

Simple Recipes

Breakfast

Lunch and Dinner

Salads

Salad Dressings

Soups 

Desserts

## Afterword

A Global Spring

## Appendixes

The Eight Founders of the Food Not Bombs Movement

Quotes Related to Food Not Bombs

Major Events in the History of Food Not Bombs

Books that Include Food Not Bombs

Vegan for Peace

Bibliography

Flyers and Forms

Acknowledgments

The Author

The “Bake Sale to Buy a Bomber” Poster

