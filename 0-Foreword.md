![intro-image](intro-image.jpg)

​												**With over a billion people going hungry each day how can we spend another dollar on war?**



> First they came for the Socialists, and I did not speak out— Because I was not a Socialist. 
>
> Then they came for the Trade Unionists, and I did not speak out— Because I was not a Trade Unionist. 
>
> Then they came for the Jews, and I did not speak out— Because I was not a Jew. 
>
> Then they came for me—and there was no one left to speak for me. 
>
> *Martin Niemöller (1892–1984)*



## HOWARD ZINN’S FOREWORD TO THE FIRST FOOD NOT BOMBS BOOK 

<img src="howard-zinn.jpg" alt="howard-zinn" style="zoom: 25%;" />



This is an extraordinary book, written by an extraordinary community of people. Their presence became known to me quite gradually, over a long period of time. I began to notice their tables, their signs, and cauldrons of hot soup and supplies of nutritious vegetables at meetings, at demonstrations, and on city streets. Then, one night I was invited to a gathering place for poets, musicians, and performers of all sorts who were possessed of some social consciousness, and there was a counter at the side of the room, and, again, that sign: Food Not Bombs. 

This time, I paid more than ordinary attention, because I recognized the man behind the counter, Eric Weinberger. I had met him twenty-five years before on the road from Selma to Montgomery, Alabama, in the great civil rights march of 1965, and again in 1977, in another march, this time of anti-nuclear activists, into the site of the Seabrook nuclear power plant. 

Now another dozen years had elapsed, and he was with Food Not Bombs. I thought these Food Not Bombs folk are carrying on the long march of the American people, moving slowly but inexorably towards a liveable society. The message of Food Not Bombs is simple and powerful: no one should be without food in a world so richly provided with land, sun, and human ingenuity. No consideration of money, no demand for profit, should stand in the way of any hungry or malnourished child or any adult in need. Here are people who will not be bamboozled by “the laws of the market “ that say only people who can afford to buy something can have it. 

Even before the recent collapse of the Soviet Union, it was an absurd and immoral policy to spend hundreds of billions of dollars each year to support a nuclear arsenal that, if used, would bring about the greatest genocide in human history and, if not used, would constitute an enormous theft from the American people. Today, with no “Soviet threat,” the policy of spending a trillion dollars over the next few years to maintain a nuclear arsenal, other weapons, and a worldwide network of military bases is even more absurd. The slogan “Food Not Bombs” is even more recognizable today as clear common sense. 

This slogan requires no complicated analysis. Those three words “say it all.” They point unerringly to the double challenge: to feed immediately people who are without adequate food, and to replace a system whose priorities are power and profit with one meeting the needs of all human beings.

It is rare to find a book that combines long-range wisdom with practical advice, but here is a treasury of such advice. It tells in specific detail how to form a Food Not Bombs group, how to collect food, how to prepare it (yes, wonderful recipes!), and how to distribute it. Every step in this process is intertwined with the warning: do not allow self-appointed “leaders” or elites to make important decisions. Decisions must be made democratically, with as wide a participation as possible, aiming to reach a consensus. The idea here is profound. If we want a good society, we need not shout, but rather show how life should be lived. Yes, this book is truly nutritious. 

*Professor Howard Zinn is the author of “A People’s History of the United States. Professor Zinn told me he would love to write a new forward for this book and asked me to send him the final draft, but he, unfortunately, passed away two months after I spoke with at the him at Campaign to End the Death Penalty’s ninth annual convention in Chicago*



> Because... FOOD is a RIGHT not a privilege! Because there is enough food for everyone to eat! Because SCARCITY is a patriarchal LIE! Because a woman should not have to USE HER BODY to get a meal or have a place to sleep! Because when we are hungry or homeless we have the RIGHT to get what we need by panning, busking or squatting! Because POVERTY is a form of VIOLENCE not necessary or natural! Because capitalism makes food a source of profit not a source of nutrition! BECAUSE FOOD GROWS ON TREES. Because we need COMMUNITY CONTROL. Because we need HOMES NOT JAILS! Because we need....FOOD NOT BOMBS

