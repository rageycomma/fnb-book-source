## Chapter 2 

## Logistics

At every step along the way you will be faced with many choices, some we will describe in this handbook, but others will be unique to your situation. You will need to make the decisions for yourselves which are the best for your local operation.

We can tell you, from our experience, that it will be both hard work and a lot of fun, and we will try to share with you those things we have learned which might both assist you and help you avoid problems we have already encountered. This handbook is a beginning point from which to take off on your own adventure. This handbook is based on over thirty years of experience, but does not provide all the answers. Every day brings more challenges and new learning opportunities.

The Food Not Bombs experience is a living, dynamic adventure which expands with every person who participates in it. Even today, as more and more Food Not Bombs groups start in other cities, we are discovering that each group brings with it new ideas, new visions and new ways of developing its own identity.

This handbook contains only the most basic information necessary for you to start your own group.

![](chapter2-sf-civic.jpg)

*One of the two daily meals shared at the Civic Center Plaza in San Francisco, California* 

#### Seven Steps to Organizing a Local Food Not Bombs Group 

At the outset, starting a Food Not Bombs might seem like more than you can handle. Work on the basics, take one step at a time. There is no need to feel pressured into accomplishing everything all at once. It might take a couple of weeks to get things rolling, or it may take months. One person cannot be a Food Not Bombs group, but one person can initiate a group. 

Once you have made the decision to start a local Food Not Bombs group, pick a meeting date, time and place and gather everyone interested to talk about what you would like to do. You might start with a group of friends, or members of an existing group, or it could be people who respond to posters and emails announcing your intentions. 

The following is a step-by-step process to get your food operation up and running. Because of your unique situation, you may need to add, ignore or reorder steps. Follow the path you feel will work best for your group.

##### Step 1 - Establish how you're going to communicate
You will firstly need to establish the date and time of your Chapter's weekly meeting to ensure that everyone involved can discuss how the Chapter will operate. 

You should also establish during this meeting how you will be communicating with each-other. Many sites such as Riseup can provide tools such as Crabgrass, which allows members to organise and share important information relating to food gathering and help share activities, and e-mail lists which can allow organisers to discuss topics which are shared between participants. 

Other applications such as Signal, which is a mobile-based instant messaging platform, can provide a useful instant alternative to e-mail lists, which will allow for instant co-ordination between members using mobile or desktop platforms, and also are secure and free from snooping. 

Use of platforms such as Google, Microsoft and other commercial platforms is possible, but you should consider that these corporations have previously shared data and information with state and federal authorities on their users.

You will also need to announce to others who you are, by posting information about your Food Not Bombs chapter to social media, such as Facebook, Instagram, Twitter and other platforms. In addition to this, consider creating a phone number for your branch so that potential volunteers or the public can contact you, as well as a public e-mail address. It can also be useful to establish an address for your chapter so that you can receive postal mail and are considered to have a concrete location. This can also allow you to establish information on sites like Google and others, which gives you a more permanent presence on search results. 

##### Step 2 - Building your chapter
Once you have information identifying you and how you can be contacted, start to announce your presence to your local community and attract volunteers to your cause.

Create flyers announcing who you are which include your contact information and the date and time of your information, and hand them out at events, at your local campuses, to friends, family, classmates, and other local organisations, or anywhere you think volunteers can be found.

Create posts and place them on your social media sites with the same information to provide to your digital audience with the same information to try an attract as many volunteers to your cause as possible. If you have multiple social media sites, and a website, it is a good idea to post updates consistently on your meetings on all these outlets so that as many people see the same message as possible.

Encourage both volunteers and those who may rely on the food to participate in the meetings. You can create your own fliers or, if you are unsure what to create, there are sample flyers available at the Food Not Bombs website. 

**Step 3 - Determine your food sources**

Before you will be able to determine what you can do, you will need to find out where you will get your food from. It may be a good idea to look at directories of businesses and organisations near you, from which you can collect excess food. Organisations you might want to consider might include: 

* Family-run restaurants, cafes, and small businesses that could be sympathetic to you.
* Local food co-ops
* Produce warehouses
* Farmers markets
* Organic food stores
* Bakeries

Tell them you plan to share the food with the hungry, deliver food to shelters and soup kitchens, as well as provide a regular meal once the chapter is established and, if they are interested and willing, arrange for a regular time to pick up the food each week or as often as is practical. Where it is appropriate, leave literature which explains the mission of Food Not Bombs.

You might also want to consider establishing a waste food network on your local street(s). A large part of waste comes from people who have left-over food which they will end up getting rid of. You may want to consider creating specific flyers to post to your neighbours asking them to drop off any unused vegetables, spices, and other vegan produce to your house or to a location you can collect it. You could ask them to drop it off at your house a few days before you plan to cook it. 

Because a large number of branches also note that they end up buying spices themselves to make various recipes, you may want to post flyers or talk to your local community to gather donations of spices or other useful condiments which are often not thrown away as waste due to their long shelf-lives. 

##### Step 4 - Determine the logistics

When you have gathered volunteers to attend one of your first meetings, you will need to discuss what you are doing and when you can do it, and whether you have enough people to do it. You may want to discuss on the agenda for your first meetings: 

* Where you are going to collect food from?
* Where you are going to cook the food?
* Who is going to cook the food? 
* What is going to happen to the food once it is cooked?
  * Is it going to be distributed at a location?
  * Will people be able to collect it from where you cook it? 
  * Is it going to be delivered to families in your nearby area?
* Who is going to deliver, distribute, coordinate collection of your food?

As you establish your sources of food, you will soon get an idea of the volume of food that you will receive, and whether or not you will have enough people to be able to process that amount of food. 

##### Step 5 - Starting with deliveries

Start by delivering your collected food to people in housing projects, shelters and local meal programs. It is important to get to know the food pantries and soup kitchens in your area. Learn where they are located, whom they serve, and how many they serve.

This information will help you plan your delivery route and distribute the appropriate types and amounts of food to each program. This will also give you an idea of when and where your chapter should start to share your regular prepared meals. It is usually desirable to arrange a regular delivery schedule with each location. Building relationships with the other food programs is valuable.

You should also consider where exactly you want to distribute the food and whether the volume of food you have will go to the maximum number of needy people in your area. If you live in a more affluent area, you may want to take the donations you have received them and pick a location where the maximum number of needy people can receive it - in some countries, families struggle significantly with having enough for school meals and other meals - it may be a consideration to find data to find out where most families are needing of meals, and aim to provide those families with food. 

##### Step 6 - Moving forward

Once this network becomes established, start to skim some food out of the flow. Locate a kitchen and cooking equipment. With this food, prepare meals to serve on the streets with literature about Food Not Bombs as well as current issues and related events.

It can be very helpful to share meals at rallies and demonstrations first; there your group can recruit more volunteers, and collect donations, as well as lift the spirits of those participating in the action. Giving out meals at a protest can build community and support for the cause in a very direct way. 

Once enough people are involved, consider sharing meals in a visible way one day a week to the hungry on the streets. Cooking and serving food together builds community within the group and is also great fun.

Choose a time, day and location where you will reach the most people. Always be on time. Pick a highly visible location and time where the largest and most diverse population is likely to walk past your food and literature.

The more people walking past your literature and meal, the more effective your group will be. The meal, banner and literature can be a powerful way to reach out to the community with the message of “food not bombs.” Talk with the public about the need for social change and direct their attention to your literature. 

Once you are more established and secure in how you are delivering food, you may want to add music, art, puppets and theatre to your meals to entertain those who are there.

**A sample agenda for your first few chapter meetings** 

* 7:00 to 7:15 — Introductions 
* 7:15 to 7:30 — Contacting food sources 
* 7:30 to 7:45 — Visiting local soup kitchens and shelters 
* 7:45 to 8:00 — Locating a kitchen 
* 8:00 to 8:15 — Choosing a high visibility serving location, day and time for weekly meal
* 8:15 to 8:30 — Choosing contact information for local chapter
* 8:30 to 8:45 — Setting day and time to make your banner
* 8:45 to 9:00 — Posting flyers and placing announcements in local papers and radio stations
* 9:00 to 9:15 — Planning first benefit concert
* 9:15 to 9:30 — Critique meeting and choose location, date and time of next meeting



### The “Office” 

![](chapter2-byandung-punx-fnb.jpg)

*The Food Not Bombs "office" in Bandung, Indonesia*

Food Not Bombs reclaims public space for a few hours each week or even a few hours every day in some cities, creating a “Temporary Autonomous Zone” as described by the author, Hakim Bey, in his book "*T.A.Z.: The Temporary Autonomous Zone, Ontological Anarchy, Poetic Terrorism*".

Our “office” arrives in a high visibility public location, providing a free space to share food, literature, music, art and ideas. Your Food Not Bombs group can be effective at inspiring social change if your volunteers make a point of engaging the public in conversation about subjects such as diverting resources from the military toward necessities like nutritious food, education and healthcare.

While several volunteers are sharing meals and distributing groceries, another volunteer can staff the literature table. They can let people passing by know that Food Not Bombs shares free vegan meals with the hungry in over 1,000 cities around the world to protest war, poverty and the destruction of the environment, and ask why, with over a billion people going hungry each day, how can we spend another dollar on war? 

If you live in the United States, you might talk with the public about how half the country’s federal budget is spent on the military and that many other countries also use their housing, healthcare and education. Other countries might not have military budgets as gargantuan as those of the United States, but invariably they will still have huge military budgets which are taken for granted while large numbers of households struggle to put food on the table - the message is still the same.

Your volunteers can suggest people take a flyer about a current issue and invite them to participate in a future protest or event. This is the most basic, yet most effective way to build support for social change. With all the problems we are facing today, it has never been more important to reach out to the public.

Many people think they are alone in feeling that society is failing and relieved to discover that Food Not Bombs not only shares their concerns but is offering a path to seeking solutions. Food Not Bombs reclaims public space for a few hours each week or even a few hours every day in some cities, creating a “Temporary Autonomous Zone” as described. It is the vision of Food Not Bombs to operate with as little money as possible. 

We strive to get the most out of our resources. One way to keep operating expenses low is to use only a mailbox, website, voicemail, and your regular meal as your office. Thus, by not having a standing office, there is no need to use valuable volunteer time staffing it as well as time raising money for the rent. Your “office” will reach many more people by its location being out on the street where the public will notice your project.

This allows the volunteers to spend more time on the street sharing meals and distributing information, making our table the “office” where the group’s business is conducted and where people who want to meet us can visit. One of our goals in doing street work is to bring people with different economic, ethnic or cultural backgrounds directly into contact with each other. If your office is on the street, then you are very accessible and all your actions are public. Your message will reach many more people if your office is on a sidewalk, plaza or public park.

The people who are forced to live on the streets will also, over time, develop a great deal of respect for your group because you are out on the streets with them. You will directly experience a piece of street life and you will develop first-hand knowledge of the opinions the public holds on the issues of the day. Consider the hours and location carefully so as to reach the most people as possible.

Thousands of people might pass by your location during rush hour and an hour later you might discover that only a handful of pedestrians will pass your table. Sometimes security people will claim you are not allowed to table, but unless you are on private property it is your right to express yourself with a literature table.

Your group will get more volunteers, more donations of food, your ideas will have more impact and you will feed more people if your table is placed at a location and time where lots of people pass. The cost of establishing this part of the Food Not Bombs operation is affordable for any group. Food Not Bombs is not a charity.

Our “office” is our stage where we can communicate our ideas. Our banner, literature and conversation with the public are as important as the food. You can direct visitors to a featured piece of literature, talk to them about a current example of misdirected policies, or educate them about projects that are contributing to positive social change. We are less likely to interest the public in joining our effort to end war, poverty and the destruction of the environment if we stand silently passing out meals.

Our “office” can also include music, theater, poetry, dance, art and other cultural expressions encouraging positive social change. Our goal is to end the need to feed the hungry because we have inspired the public to take action to redirect our resources from the military towards real security where everyone has what they need to flourish. 

### Decision-Making
Another goal of Food Not Bombs is the creation of opportunities for self-empowerment. The way to do this within each group is to create an environment where every member is encouraged to participate in decision-making, take initiative, and filling various roles necessary for the smooth functioning of the group. Each Food Not Bombs chapter is autonomous and independent and strives to include everyone in its decision-making process.

Each chapter makes its decisions by the process of consensus rather than voting or “Robert’s Rules of Order.” Voting is a win or lose model in which people are more concerned about the numbers it takes to win a majority than they are in making a decision that everyone in the group believes is best.

Consensus, on the other hand, is a process of synthesis, bringing together diverse elements and blending them into a decision which is acceptable to the entire group and supports the principles of the participants -- in essence, it is a qualitative rather than quantitative method of decision-making. Each person’s ideas are valued and become part of the decision. 

When everyone participates in the discussion of an idea, trust is developed and people feel valued and committed to the result. A proposal is stronger when everyone works together to create the best possible decision for the group. Any idea can be considered, but only those ideas which everyone thinks are in the best interests of the group are adopted.

There are several models of consensus which your group might choose to adopt. It is most important, however, that whatever process you use is clear, consistent and can be easily taught and learned so that all can participate fully. The use of consensus can also reduce the ability of intelligence agencies to dominate the direction of your chapter. Peace, social justice and environmental organizations in the United States that use a hierarchical structure can be taken over by federal, state and corporate agents when they are elected to leadership roles selected as directors, or hired for key staff positions.

If there is no director or president, authorities can use voting as a means to misdirect an organization, stacking meetings with paid informants and agents. After having participated in a number of peace groups that were disrupted by agents who used the voting process to misdirect the organization, the co-founders of Food Not Bombs were even more committed to the use of consensus. In nearly thirty years of dedication to a non-hierarchical consensus based process, it has become clear that this is one of the important principles that have made it possible for Food Not Bombs to flourish. 

Many progressive grassroots organizations avoid having leaders who might dominate the group. However, it is a mistake to think a group does not need leadership. To avoid having power concentrated in the hands of a few entrenched leaders, encourage leadership skills in every member of the group and rotate all roles. This can be accomplished by holding skill-building training workshops and by encouraging and supporting people to be self-empowered, especially those who are generally reserved.

This helps the group become more democratic and helps individuals feel more satisfied and, therefore, less likely to burn-out or fade away. Some chapters have a “bottom liner” overseeing the spicing of the meals. Facilitators, note takers and spokespeople for committees should change from meeting to meeting. It can even be helpful to rotate food collectors, cooks and servers from time to time so everyone has a chance to enjoy every aspect of Food Not Bombs and the tasks remain fresh and inspiring.

The Food Not Bombs movement does not have a headquarters, director or president. The Food Not Bombs cofounders have no power over the direction of the movement or decisions of any group. Food Not Bombs held its first international gathering in San Francisco in 1992, and those attending came to consensus that every group would be autonomous, have no leaders and each group would make decisions using consensus.

They also agreed that the food would always be vegetarian or vegan and free to anyone, without restriction and our volunteers would be dedicated to nonviolence.

In 1995, Food Not Bombs held its second international gathering, also in San Francisco, where we confirmed the first three principles and also agreed to organize a global coordination office to help the public start or locate local Food Not Bombs groups.

The website and the toll free number, 1-800-884-1136, were designated as the contact points for this coordination, but this collective was not granted any power. All chapters can request their ideas and information be posted on this website. (www.foodnotbombs.net) The global coordination office directs the public and media to their local Food Not Bombs chapter, as well as provide materials and suggestions on how to start a local group.


Chapters or people participating in gatherings often ask the volunteers at the global coordinating office to post on the website and other publications announcements of events, campaigns or policies adopted by the group or gathering. While we have generally had only one global coordination office, it is possible for volunteers to organize other movement-wide websites, publications or other projects to promote the goals of Food Not Bombs.

They could be based on shared languages such as a Russian, Spanish or Chinese based coordination office. Still, the responsibility of decision making rests with each chapter. It is best for ideas generated at gatherings to be introduced to each chapter so that the local group can decide whether or not to implement the proposal. Proposals for gatherings and tours are sent out to the Food Not Bombs groups that might participate to seek input from the community

Each chapter can include the proposal in the agenda of their next meeting. The global coordination office also responds to requests for information and mail out flyers, books, start-up kits, and other materials to help start or promote local Food Not Bombs groups. When a group starts, they will often email their contact information and the days, times and locations of their meals to the volunteers that maintain the website. Money raised by the global coordination office is contributed to local chapters based on requests using a questionnaire posted on the website.

The office has contributed money to groups in Africa to buy seeds, cooking equipment and a vehicle. Money has been contributed to local groups organizing Food Not Bombs gatherings and tours. Funds are also used to finance relief efforts, like Hurricane Katrina, or to help support kitchens and convergence spaces at protests, occupations and other direct actions.

### Outreach 
Outreach is very important, less expensive and more effective than you might imagine. Billions are spent on outreach for consumer products and the promotion of the current destructive economic and political system because outreach works.

Our ideas and activities are important and should be promoted to as many people as possible. While we don’t have the budgets of corporations and governments, we have something more powerful and that is an honest message that resonates with people. The flyers and banner at your regular meal is one of the most effective forms of outreach.

The internet and social media are valuable technologies for building our movement, but the simple flyer is still one of the most effective tools for social change. In a sea of electronic information your flyer provides a way for the public to find your group’s blog, website, email and your meal and meeting times and locations.

![](C:\DEVELOPMENT\FoodNotBombs\fnb-book\chapter2-wheatpaste-equip-upscale-svg.svg)



*"Wheatpasting" equipment. Wheatpasting is a simple and inexpensive way of putting posters up in your local area*



Anyone passing your table who takes a flyer can share it, passing news of your group or takes a flyer can share it, passing news of your group or your group’s ideas to a wider audience. Your chapter might want to put up its own website and include its address on the flyers and banner. 

The appendix of this book has several flyers you can use that have been effective in attracting new people to Food Not Bombs. We also provide flyers on the website you can download. You can use the flyers for your local chapter by putting your group’s phone number, email address, website or mailing address in the appropriate spots, or you can create your own flyers using any of the images from this book or off the website or any you might draw yourself.

These flyers can then be put on bulletin boards or windows in local schools, cafes, groceries, libraries, shelters, social service agencies, offices of community groups, bookstores, and laundromats. Post flyers all over your community on a regular basis.

It is good to continually bring in new people with fresh ideas and enthusiasm and to remind the public of your activities. With many people traveling, seeking work and new opportunities, you will always have a new audience, and it often takes more than one viewing for people to respond to a poster. Do not rely on postings to Facebook friends to announce events, meetings or meals. Events that are only announced on Facebook often have no participants, despite many declaring their interest on the event.

Distributing flyers will draw much more interest. Groups that add their contact information on every flyer often have many more food donations, volunteers and people participating in their meals than those that don’t. During the years that we were being arrested for sharing food in San Francisco, I made my living by posting flyers for theaters and art galleries.

My clients would buy advertisements on television, radio and in local magazines and newspapers, and they would ask everyone that bought a ticket how they heard of the event. Over half the people marked the box saying that it was a poster that alerted 43 them to the performance.

I posted one Food Not Bombs flyer at the *GreenStar Food Co+Op* in Ithaca, New York, and several months later I discovered that a woman who had seen that one flyer was inspired to start a local Food Not Bombs group. If one or two of your volunteers make a habit of posting Food Not Bombs flyers all over your community, your group will have substantial of support.

You may want to organize a benefit concert to raise money for the printing. Flyers posted around your local community communicating the benefit that a Food Not Bombs group will provide will be yet another way your chapter will become known. In addition to posting flyers in public spaces, visit all the environmental, animal rights, peace and social justice organizations in your community.

Leave your flyers and collect a stack of their literature to place on your own information table. Also, go to all the soup kitchens, pantries, shelters, and advocacy groups for those suffering from economic injustice, and distribute your literature announcing the details of your meals and meetings. Don’t be discouraged by a lukewarm reception.

At first, these groups might view Food Not Bombs as competition for scarce resources, or they may be strongly opposed to connecting the issues of hunger, homelessness and economic injustice with other political issues such as militarism. Many direct service agencies accept the role of caregiver to those most oppressed in our society without challenging 

![](chapter2-sanfran-police.jpg)

*San Francisco Police take "objectionable literature" to silence Food Not Bombs*

![](chapter2-vermont-fnb-fliers.jpg)

*Flyers at the University of Vermont inviting students to attend a Food Not Bombs event*

They may prefer to keep a low profile and support the status quo, and they will be very fearful of anyone who challenge, the system. The foundations they rely on might threaten to cut their funding if they believe the agency is associated with Food Not Bombs. However, because the vision of Food Not Bombs is the creation of abundance by the recovery of surplus food, your donation of free food can be a way to reach out to them and gradually win their support. This kind of outreach can become the foundation of widespread community solidarity and could be very valuable to your group in the future. 

As your effort grows, you can organize and sponsor special events which will attract more people to join in the work and the fun. The range of events your chapter can organize is only limited by your imagination and could include concerts, knitting circles, poetry and book readings, fashion shows, art openings, sporting events, hikes, camp outs, plays, rallies, lectures, circuses, puppet shows, workshops, Really Really Free Markets, Reclaim the Commons and film nights.

You can help your chapter flourish by organizing regular events. Your group may have a weekly film night and a concert once a month. Before these events, be sure to call all the media in your town, ask them to announce the details of the event and invite them to come to report on the outcome. Set up interviews with local radio programs, magazines, newspapers and if you are in the United States, local cable TV shows. 

Some radio programs may feature the type of music that will be at your concert and you can invite several band members to join you on a show that airs before the event. Ask the band members to bring a digital or physical copy of their album or songs, or their instruments so the audience can hear some of the music that will be featured at the event. News departments or other talk programs might be good to contact before your events. Even though the coverage can sometimes be unsympathetic, it is still valuable to have Food Not Bombs mentioned in the local media.

In our experience, most people understand the concept “food not bombs” and are not misled by negative reporting. Along with contacting the local media, post the announcement of your events on Indymedia and other websites, including your own chapter’s site. You can also email the announcement to your E-mail distribution lists, distribute flyers to local bulletin boards and local shop windows and arrange to speak to local organizations and clubs.

If you are organizing a concert, invite the band to announce it on their website and other outlets, and events they may be participating at before your event. Consider displaying a huge banner proclaiming “Food Not Bombs” at these events. This banner is very useful when the media is taking pictures because, if nothing else, the words “Food Not Bombs” will be displayed.

You can use the Food Not Bombs logo of a purple fist holding a carrot as much as you want. Feel free to redraw the logo to show a unique image reflecting your area of the world or focus of your local group. After all, if Food Not Bombs is anything, it is D.I.Y. (Do It Yourself). The Food Not Bombs global coordination office has buttons, bumper stickers, t-shirts and banners with this logo for you to use for fundraising and promotion.

You can also download the flyers and images off our websites and use them to make flyers, banners, t-shirts, patches and anything else you can dream up. You can add your group’s contact information and website to all your publications. While it is relatively rare, government officials and their infiltrators in the United States have been known to discourage volunteers from posting flyers around the community or providing literature and hanging the banner at the regular meals.

Police infiltrators have been known to volunteer to bring the banner and literature to the meal so they can discard the materials in a dumpster on the way. Sometimes an infiltrator might suggest that it is “bad” to hand out information about your meal at soup kitchens, shelters or public events in an effort discourage people from participating.

Food Not Bombs groups in the United States have even been told they can continue to provide food to the hungry, without fear of arrest, as long as they don’t hang a Food Not Bombs banner, remove all the political literature from the meals and end the practice of posting flyers throughout the community. The authorities have also sent infiltrators to help cook or share food with the goal of encouraging the local group to change the name from “Food Not Bombs” to something “less political” because after all, the main point is to “feed the hungry,” or because, Food Not Bombs has a “bad reputation. ”

Food banks have told Food Not Bombs volunteers that they will donate food to their chapter if they change the name to “something less political.” Chapters that abandon calling themselves Food Not Bombs, often find that they find it more difficult in recruiting volunteers and getting food donations. 

They may also find that fewer people turn up to receive food from them, and eventually stop feeding the hungry within weeks or months. These "less-political names" are really just as political, but it is a politics that promotes the current system of domination, exploitation, hunger, poverty and war instead of a politics of abundance, peace and liberation.

As the group changes from working for social change as part of a global network to becoming a local charity, the volunteers grow discouraged and figure they might as well help an existing charity or stop all together. If this happens in your community, one solution is to start a new Food Not Bombs group and continue the work.

Many groups have been adding the phrase “Solidarity not Charity” to their flyers and banners to counter the effort to discourage our struggle to change society so no one is forced to eat at soup kitchens because they can’t afford to feed themselves. There are many creative ways to reach out to your community. Everything from flyers posted in cafes, bookstores and other shops, to graffiti, banners, interviews on local radio stations and newspapers, concerts, film nights, websites and puppet shows, to regular literature tables on the streets and at events will help bring new volunteers and food donations and build solidarity with other community groups.

You will find volunteers eager to make one of these forms of outreach the focus of their work with Food Not Bombs. Everyone might not be good at cooking, but instead, may have a passion for art or public speaking. Food Not Bombs can use everyone’s talents.

### Food Collection
Food recovery is a core feature of the Food Not Bombs project. One of the by-products of our program is the reduction of waste in our society. Discovering sources of surplus food might at first appear to be a major challenge, but generally, it just takes confidence and patience.

Every business in the food industry is a potential source of recoverable food, from farms and produce packers to wholesale distributors and retail stores, farmers markets and bazaars and from manufacturing to distribution warehouses.

Studies show that over $100 billion worth of edible food was discarded in the United States every year during the first decade of the 21st century. As much as 35 percent or more, of all food is wasted in the United States. The United Nations has reported that there is enough discarded food to feed the one billion people that go to sleep hungry each night.

Sometimes it may take some creativity and persistence to convince a stubborn manager or produce worker to allow you to recover some of their discarded food, but in most instances, the businesses will be very cooperative.

Most of the time you can bring a flyer about Food Not Bombs to share with the people you believe might be interested in contributing food to your group. On occasion, you may need to decide if you want the business owners or managers to know that some of the food will be used for political organizing or the name of your group is Food Not Bombs.

At most stores this will not be an issue, and they will be honored to contribute to Food Not Bombs; at others, it might be better left unsaid until they get to know you better. You can simply explain that you will be using their contribution to feed the hungry.

Start making arrangements to collect discarded food at organic produce warehouses, bakeries and natural food stores by talking with the employees that take the food to the garbage. If you visit the produce department at your local grocery you will find a worker standing by a cart trimming vegetables. You will see them dropping undesirable fruit or vegetables into a box on the lower level of their cart. That is the first person you can talk with about collecting discarded food.

They may send you to a manager and, even if you get permission from the management, it will be the person trimming produce that determines what produce your Food Not Bombs group receives as donations. If the manager turns you down, return another day and speak with the worker on the floor. They are the ones who have to drag all the unwanted produce to the garbage and, typically, they are more than happy to pass it on to you.

Independently or locally owned stores are generally but not always more receptive than national chains. It is possible that the first few shops you visit will provide you with more than enough food to get started. Ask the workers at these businesses if they have any edible food that they regularly throw away and would be willing to give you. You can point out that, by collecting this food, you will be saving them money on their waste disposal bill.

Managers will certainly be aware of how expensive it is to have this surplus hauled away as waste and how costs keep growing each year as more and more landfills become exhausted. If you live in or near a city, it may be possible to collect food from the main produce distribution center. These are the warehouses where the local delivery trucks pick up their orders.

Talk with a driver unloading at your grocery and they can tell you where to locate these centers. Early every morning they start to throw away cases of produce that they know will not make it on the distribution trucks the next day. You can collect truck loads of very good produce every morning from these locations. If the brokers are reluctant to donate, offer a dollar, euro or other small denomination for a case that they plan to discard, and before long, they will be dragging cases of great food to the loading dock.

A couple of produce centers have required drivers to pay a small toll to get in, but it is well worth the price. Some groceries and bakeries in the United States may request a letter from the Internal Revenue Service (IRS) so they can claim their contribution on their taxes. If so, the Food Not Bombs global coordination office can provide your chapter with required documents. Just e-mail and request a tax letter for the collection of food. While in the process of collecting contacts in the food industry, you can also start determining the availability of drivers, vehicles or bicyclers with bike carts.

There needs to be at least one volunteer to make the pick-ups, but it is more rewarding if two or more people work together. Make a schedule which is convenient to both the store and the people collecting the contribution.

It is important to be flexible, but also reliable; businesses will be hesitant to participate if they do not feel they can rely on this “waste” removal method to be consistent. It is a tradition with Food Not Bombs to always be on time; therefore, do not overextend yourself. It is actually more common to get too much food than not enough, but only do as much as is comfortable. After all, some recovery of food is better than no recovery at all.

![](chapter2-utrecht-bike.jpg)

*Collecting discarded food by tricycle in Utrecht, Netherlands* 

Also, consider taking time to make friends with the workers at the places where you are collecting the surplus food. These workers make the day-to-day decisions about how much food is recovered, and they can make an effort to recover even more food if they feel comfortable with you. This also helps build community support, not only for the sharing of food, but for our message of peace, social justice and a sustainable future.

Inviting grocery workers to participate by sharing their discarded food gives them the opportunity to feel they are helping the community. It is almost never necessary to dive into dumpsters and doing so deprives your group of developing a relationship with people that work in the food industry. There is rarely any reason to rummage through dumpsters because there is so much better food available by talking with those responsible for discarding the surplus food. The variety of food that can be recovered is unlimited. Be creative. Any perishable food is going to be intentionally over-stocked so there will be a regular surplus destined to be waste.

Look for sources of surplus bagels, bread and pastries, organic fruits and vegetables, tofu and vegan packaged foods. Sometimes you might need to buy non-perishables like rice, beans, miso, condiments and spices at local, natural food groceries or food co-ops, but then again, many of these stores may offer to sell these items at cost or even donate them for free. Food Not Bombs groups will organize benefit concerts to raise money for these items. Natural food distributors can provide bulk food at very low prices.

In areas where natural or human caused crisis is more common, it may be wise to store hundreds of kilos of rice, beans, oats, cooking oil or other basic dry goods. A concert or other event could raise enough money to stock up for such a disaster. Your group could store this bulk food in metal trash cans or other bins so mice, rats and insects don’t ruin your supplies. Eventually, after setting up a regular schedule with local, natural food stores, produce markets and bakeries, your group may want to start collecting additional food from warehouses, farms, manufacturers and wholesale distributors.

The volume of food available to recover is immense so be selective. Take what you can use from the highest quality supplied. In many places, there is no need to recover commercial produce because there is plenty of organic food to recover! In fact, one of our political messages is that there is more edible food being thrown away each day by the food industry than there are hungry people to eat it. We live in a world of abundance if we are willing to end profit and exploitation.

### Food Distribution
At first, deliver the bulk food you collect to soup kitchens, community centers, public housing and shelters in your area. Delivery of uncooked food is a very important step that is often neglected by new Food Not Bombs chapters. Delivering produce, baked goods and other fresh food is a powerful way to build community and support for your chapter without having to prepare meals.

If delivery around the community is a problem because of the difficulty of obtaining a delivery vehicle or lack of volunteers free to drive around town, then another great option is to hand out your uncooked food at a street corner, plaza, park or other public location at the same time and days every week.

This could be the location where you also intend to start sharing prepared meals. From your earlier research and contacts, it is likely you will already know which kitchens or community centers are interested in receiving food.

After a few months or more, your chapter can start to deliver bulk food to food pantries, striking workers, day-care centers, battered women’s shelters, refugees, day laborers and the like. Contact organizations already working directly in the community and ask if their staff would take responsibility for equitable distribution of free food each week.

Since they already have a base of operation in the community, their staff will know the people in need, how great their need is and how best to distribute food to them. Encourage them to use the free food distribution program as a way to increase participation in their other programs—use the food as an organizing tool.

Once you start sharing prepared food, your chapter might continue to give out bulk uncooked food at your regular meals. It can be helpful to collect used shopping bags from local groceries so people who do not bring a cloth bag can collect what they need.

If the people seeking food are desperate, it may be better to have volunteers pack the bags before you arrive to the distribution site. Pre-bagging the food may be helpful, particularly if there are items in limited supply. If people are relaxed, let them fill their bags themselves. The free groceries can be arranged by vegetables, fruit and baked goods.

Many chapters also provide free clothing or blankets. Clean socks or soap and other sanitary items can be helpful. If your group hands out unprepared food at the time of your regular meal, consider sharing the meal after everyone has had a chance to fill their grocery bags.

In some cases, it may be better to share the prepared meal first. Your chapter might invite musicians to perform during the distribution. If you are sharing meals with several hundred people who line up to eat, it can reduce tension by having volunteers hand out baked goods starting at the back of the line working towards the front where the main course is being shared.

![](chapter2-abuja.jpg)

*Cooking for Food Not Bombs in the Nigerian capital Abuja* 

We want to discourage a hierarchy of those in need and those with privilege who have come to help. At the same time, it is not safe to let everyone serve themselves the prepared meals. Provide a way for people to wash their hands, and invite people who come to eat to help serve the food. Make sure those people that come to eat, but became servers, have all the food they want.

If you have a huge line, let them be first and they will be finished eating in time to help dish out one of the courses to the rest of the people. You can also invite them to meet at the kitchen to eat before they go out and help you with the distribution. They might also want to help you cook. Some chapters eat together before they take food to the streets when they know they will be feeding hundreds of people.

Your chapter will soon find regular support from those depending on the food, and this will have a powerful effect throughout the community. Erasing the “us and them” dynamic of the social service industry provides a solid foundation for social change.

Many Food Not Bombs groups print out a small flyer that announces the day, time and location of food distribution. They pass them out around the community the day before the meal or even that morning. Volunteers seek out places like train and bus stations, soup kitchens, social service offices and camps where people who may need free food are living.

This can be a great time to ask if anyone would be interested in volunteering with Food Not Bombs. It is important, in-keeping with the Food Not Bombs tradition of being on time, to start your distribution at the time you announce. For people depending on Food Not Bombs, each minute they wait can seem like an eternity.

These people deserve our respect by our being on time, and our reliability can be one of the most effective steps towards building trust and community support. If we are prompt and can be relied on to be on time for our meal, we can be trusted to come through in a wide range of matters.

If your chapter is consistently late, it can also have an impact on the trust and reputation of all Food Not Bombs chapters as news spreads that we are not reliable. If we are known for being prompt, this can build trust for times like rebellions. One of our goals is to encourage the awareness of food’s abundance as well as the undermining of the market of scarcity that places profits before people.

Another goal is to encourage the public to realize we can transform society, that it is possible to “do it ourselves,” and that we can be counted on to address the fundamental issues facing our community. 

### The Kitchen
Once you have your network of food collection and distribution in operation, begin using some of the recovered food to prepare hot, vegan meals.

You will need to find a kitchen to use, and you will also need several pieces of equipment which are necessary for feeding large numbers of people that are not found in the average kitchen. Depending upon the amount of food you are going to be cooking, this may be a significant amount of equipment what is required, and it may be advisable to set up an event where your local community can donate equipment to help you get started, or to start a crowdfunding campaign or benefit concert so that you can purchase the necessary equipment.

There are several methods of finding suitable kitchen space. Sometimes it is possible to arrange to use the kitchen in a community center, place of worship or public building. A large kitchen in a collective house or a number of average-sized kitchens also might be sufficient, but sometimes cooking right on the street in a field kitchen is the best solution. Each situation has its advantages and disadvantages, and the demands of your meal distribution program will determine your kitchen needs.

Often, a loose combination of kitchen spaces is necessary for different aspects of your schedule. You might use a church kitchen for preparing your regular meals for the hungry, a field kitchen for a large rally in a park and a volunteer’s kitchen for a catered lunch. The key is to find the right size kitchen for each event.

Since most Food Not Bombs groups do some amount of cooking outdoors, it is a good idea to acquire a camp stove or metal propane grill for cooking, as propane seems to be the best fuel for cooking in field kitchens. Although the initial cost of purchasing the propane tank may be slightly expensive for some chapters, tanks generally last a long time, and are refilled at a much cheaper cost. As with everything, ensure that you consult information on how to transport and store propane safely.

It is worthwhile to obtain a strong, heavy-duty stove, and while it might cost more, it will last longer and be safer with large pots. It is also possible to cook on a wood fire. Some chapters build stone or brick fire pits and make grills out of found metal. All equipment needed for food preparation and serving can be obtained from restaurant supply stores, thrift stores, yard sales, and kitchen auctions, or donated by friends or your local community.

In general, the most important pieces of equipment are the cooking pots. You will need all different sizes, but the most valuable are the very large pots. Specifically, pots which are from 11 to 15 gallons, or 41 to 56 litres. A couple hundred people generally can be fed from a pot this size, depending upon what is prepared in it, but these pots are hard to come by.

Most people who have pots this size will not lend them out. The cheapest pots are made from un-anodized or "uncoated" aluminium, which are generally not recommended because when used to cook things which are acidic, such as miso soup or tomato-based dishes, aluminium can be corroded and leach out from the pot and into the food; ruining both. Although this can be avoided by purchasing anodized aluminium, stainless steel is generally preferable because it is most readily available.

Once you have accumulated a collection of pots and lids, be very careful with them. It is not uncommon to lose pots between the kitchen and the vehicle or between the vehicle and the serving table. This can be avoided by transferring cooked food into smaller, less-valuable pots, plastic buckets, or sanitary ice chests which are less likely to go missing. Similarly, if you are providing food to a group, or in a situation where you may be arrested, seizure of plastic buckets are much easier and less costly to replace than pots, which may reduce your ability to produce food or stop operations entirely.

Another valuable piece of equipment is the five gallon (20 litre) plastic bucket. These can usually be obtained free from natural food stores, co-ops, wholesalers and larger restaurants. Ask them to save and give you peanut-butter buckets, tofu buckets and other large plastic containers in which food is delivered. Don’t forget to collect the lids, too. These containers are valuable for food storage, transportation and serving, and they can be used for many other purposes.

Some chapters use these buckets for washing their bowls, plates and utensils during the meal: one for warm soapy water, one for the rinse, and one with vinegar, hydrogen peroxide or bleach to sanitize the items. This way, fifty sets of plates and forks can be used by a couple hundred people. Because they are fairly easy to get, these buckets also are good to use in situations where you cannot be sure they will be returned to you.

Large coolers or ice chests are also a necessity. You can buy very large ice chests at camping supply stores. The coolers used on fishing boats can often be large enough for two or three pots. Ice chests will keep your food hot for several hours. They can also be used to keep fruit salad and other items cool. Your chapter will also need sharp knives, cutting boards, spatulas, large serving spoons and ladles for preparing food.

If you have six volunteers that help cook, have six knives and cutting boards. You can also get large silver bowls at restaurant supply stores which are useful for holding food you are cutting to size before placing it into the cook pots. You can also use five-gallon, plastic buckets for this purpose. Cleaning supplies like sponges, cloth rags, soap and sanitary solutions like vinegar, bleach or other germ killing natural antiseptic are needed. If your chapter plans to cook outside, it is helpful to have a five-gallon water jug to rinse the servers’ hands.

You might want to provide a few buckets or a box for people to return their plates and silverware for washing so the soapy water can be used in a more relaxed way.

### Food Preparation
The major issue to address, when considering preparing food at low cost for large amounts of people, is one of logistics. Getting the proper amount of food, the necessary equipment, a suitable kitchen, and the "cooking team" all together at the same time might sometimes seem like a miracle, but it can be done. Each local chapter will develop its own method of food preparation. The following is a general guide.

The volunteer "cooking team" will usually meet at the kitchen a few hours before the meal is scheduled to be served. They often help unload the food and equipment from the Food Not Bombs vehicles. Sometimes the volunteers that collected the food will have already stored it at your kitchen the day before. Always wash your hands and wrists with soap and warm water before cooking, and plan the menu by looking at what food you have and how many people you are planning to feed. Sort out all the useful food and wash it. The most time-consuming job in this process is washing and cutting the vegetables.

Each "cooking team" usually operates with whatever roles and responsibilities they are comfortable with. Members of the team may choose to alternate or assign roles to themselves in order to ensure food is cooked and distributed to people in the most effective way. Members of this team can occupy roles such as:

* "*Head cook*" - Responsible for co-ordinating others to ensure a dish is cooked from start to finish.
* "*Bottom liner*" - Responsible for peeling, or cutting, or preparation of materials or ingredients for a dish.

For instance, a head cook may have experience of how to prepare a specific dish either from the "recipes" provided in this book, or ones they know, so they may guide one "bottom liner" to cut potatoes, while another cuts carrots, and the head cook prepares other ingredients, and cooks the food. In the case that there are new members, the head cook could also help new members with tasks along with other bottom liners.

The team can choose to do this operation co-operatively, and ensure that tasks are distributed equally among those who want to perform them, so that everyone can learn and the overall quality of the food improves, but also so that the responsibilities are shared between members of the chapter and there is not an over-dependence on one specific person to do one specific task.

Once the meal has been prepared by the "cooking team", they will clean the kitchen, package the food for transportation and load it into the Food Not Bombs vehicle, shopping trolleys or bike carts for delivery to the serving site by the "serving team". 

Sometimes the serving team and the cooking team are the same people, but often, they may consist of different volunteers. The "serving team" may arrive at the serving site and organize the food distribution and the staffing of the literature table before the "cooking team" arrive with the food. Always try to have a hand-washing bucket with soap and a rinse bucket with just a little bleach, so the volunteers can wash their hands before serving.

Try to keep the food away from the literature. If a long line develops, have someone go up and down the line and hand out bread or muffins or maybe something to drink on hot days so the wait is not too unbearable. This also helps reduce the tension created by fear that the food might run out. If you can find musicians or other street performers to come and perform while you’re serving, it will also reduce tensions and create a very positive, festive atmosphere.

This could also be a great time to have a puppet show or other program to inspire the public to support a local action, protest or support a proposed law or policy, or highlight a community garden or bike repair workshop.

The serving team is also responsible for cleaning up both the site and the equipment and returning the supplies to wherever they are stored. The collection of cash donations at the food table is an ongoing debate. Sometimes it is completely out of place to ask for donations, but in other situations people insist on being allowed to contribute to the collective work.

In any event, always encourage the idea that everyone can have as much food as they want without regard to their ability to pay for it. Food is a right, not a privilege.

### Outdoor Tables and Field Kitchens
At every outdoor event, the first decision the group needs to make is where to place the tables. There are many important issues to consider to have the most impact.

If possible, look at the location ahead of time. At demonstrations, placing the food table as close as possible to the focal point of the demonstration has been very successful. Being close to the action encourages people to stay involved and not drift away. Sometimes, the most desirable location is the one with the most foot traffic.

Other times, it is the most visible, accessible location for the hungry. Often ten feet in one direction or another can have a big impact on the number of people that will visit your table. It is also important to choose the best time to start sharing your meal.

It might be good to set up during rush hour or at noon when hundreds of people are likely to walk by. This way you not only have the chance to feed a great many hungry people, you will reach more of the public with our message and attract additional volunteers and donations from those passing by. 

It is always a good idea, however, to be sensitive to nearby restaurants and vendors with similar types of food; they might complain and try to have your operation shut down if they feel it is in competition with theirs. On the other hand, you may find that some vendors welcome you because you will be taking on a crowd that would overwhelm them at a festival or other event.

These vendors can often donate their leftovers to your chapter towards the end of the event. Unless you are providing food at a protest of a certain facility, you may not want to set up right in front of a restaurant. If providing for a protest, you might want to start sharing food as the protesters march to the site, or share food at both the start of the march while people are gathering as well as at the end as protesters arrive. 

Food Not Bombs has been central to supporting actions such as strikes, occupations, border camps, base camps and tent city protests that can last days, weeks, months and, sometimes, years. The choice of your location and the times you share meals might be determined by the strategy being used during the action.

There have been times where we had a main field kitchen at a central location and set up smaller serving tables with our literature and banner at key satellite locations. There have been times where we cook the meals outside a seized factory or occupied building, providing food to supporters outside while sneaking food inside to those taking over the site. You may need to organize a system of ropes and buckets that the occupiers use to haul in their food.

The techniques and logistics for providing food support will often need to be developed as the action faces changes in the tactics being used by the police or management. The following diagrams offer two possible layouts of your field kitchen. One is more basic, involving a minimum of equipment. The other involves more equipment and would be able to pass a health department inspection in most cities in the United States.

In general, Food Not Bombs believes that our work does not require any permits. Sharing free food and literature is an unregulated activity. The government has no say on how we share ideas and gifts. However, the city or the police can use the permit issue as a way to attempt to harass your chapter into shutting down your meal.

Therefore, it is sometimes a good idea to have a clean and fully-equipped field kitchen to take away their excuse that you may be violating a food sanitation law. There may still be attempts to shut you down, but you can point out that it is not a health but a political issue that they are raising. It is the Food Not Bombs position that we have a right to give away free food at any time, in any public space, without any permission from the state. 

## BEYOND BASIC FOOD COLLECTION AND DISTRIBUTION

### Street Theater

![](chapter2-puppets.jpg)

*Giant puppets at the 1992 protest in San Francisco's "Against Columbus Day" celebration*

From the very beginning, the founding volunteers saw all of their street activity as theater. Influenced by artists like The Living Theater and Laurie Anderson, Food Not Bombs took its message to a public that was often not aware of the ideas the group was presenting. Most Americans had no idea their government was at war in Central America or that half the federal budget was used for the military.

The idea that nuclear war was possible rarely entered the consciousness of most people, yet billions of their tax dollars were being diverted from schools and hospitals to fund the arms race. Unless you faced poverty yourself, it wasn’t a concern. It was clear that the standard hour-long protest with signs and slogans had been ineffective at inspiring the public to join our movement.

Our performances in the United States were held in public spaces like Brattle and Harvard Squares in Cambridge, Massachusetts, and at the Boston Commons or Copley Square in Boston, Massachusetts. The theater of Food Not Bombs not only included vegan meals, but also literature tables, banners, signs, props, puppets, music and a variety of other expressive activities.

From the beginning, we recognized that the personal is political. We wanted to dramatize the interconnectedness of the militarization of our society with the way we live our lives; highlighting the social costs of spending our resources on war and the human suffering caused by these policies. We created opportunities to expose these injustices through theatrical Depression era Soup Lines, depicting military generals holding a bake sale to buy a B-1 bomber, offering the “tofu challenge” instead of the “Pepsi Challenge” by sharing tofu fruit smoothies to students lined up to see if Coke or Pepsi was “best.”

We even had a silent theater piece in which one volunteer dressed as a papier-mâchė missile chased a person dressed as a papier-mâchė world, threatening to bomb the Earth to its end. Our street theater didn’t stop with the Boston chapter.

The 1989 tent city protest in Civic Center Plaza in Boston, Massachusetts, included “The Goddess of Free Food” and a “Bombs Not Food” skit outside San Francisco City Hall. We organized a “Blood For Oil Exchange” outside the Pacific Stock Exchange in San Francisco in the days before the United States launched Desert Storm and staged a “Citizens Weapons Inspection” outside Raytheon Missile Systems factories in Arizona before the war on Iraq.

We also baked bread with the sun in a solar oven outside the White House in Washington D.C. as part of “The Change We Knead Now” vigil, calling for a nationwide effort to address the crisis of climate change caused by consumerism and militarism. The Food Not Bombs banner, free food and literature can send a powerful message to all that happen to pass by. T

The only limits to what kind of theater your chapter can present are the limits of your group’s imagination, time and budget. 

Scenarios have included anything from setting up a food and literature table with some musicians and a banner; to full scale productions with amplified sound, light shows, movie projectors, puppets, clowns, poetry, free markets, dancers, signs, solar oven demonstrations and speakers all happening around your food and literature tables. Sometimes these events have been planned entirely by Food Not Bombs, sometimes they are organized by other groups and we just attend with the banner, food and literature. You can also think of ways to include your audience in the performance.

Because we have always approached our work as theater, it has always been easy to adapt to various situations. Our theater is designed to help the public recognize and value the interconnectedness of all progressive issues. We try to expose how the policies of consumerism, militarism and imperialism affect our everyday lives from our diet and the items we choose to buy, to the policies supported by our taxes like the use of nuclear weapons for “defense,” promotion of genetically modified foods and a police system that imprisons millions while denying millions more access to education.

Our theater also encourages the public to withhold its cooperation and support of the dominant political and economic system while participating in nonviolent direct action and other protests. When we participate in an event highlighting a particular issue, we try to show how it connects with the other issues Food Not Bombs is organizing around.

Food is often an excellent way to show the connections between our daily choices and their relationship to the government and corporate policies that are threatening our future. We share the ideas *The Living Theater* experimented with including involving the public in our performances in the “fourth wall,” an imaginary “wall” at the front of the stage

[^ch2_fourth_wall]: The "fourth wall" is an imaginary barrier that exists between the performers and the audience,  where performers act in a way that suggests they cannot see the audience or react to them, but the audience is able to see them.

or even the “fifth wall,” described by Peter Lichtenfels as “that semi-porous membrane that stands between individual audience members during a shared experience.”

The impact can be profound. Even the impression made on a pedestrian seeing a line of 200 hungry people before a banner stating “Food Not Bombs” can be so powerfully felt that the witness will have a deep and lasting understanding of our message. The impression is so strong that they are more likely to take action, so strong, that the state is worried that the experience could lead to public outrage or worse, rebellion and an end to their control.

Add music and other elements and an even larger audience could be attracted to the goals of Food Not Bombs. We promote and support many events in our community by carrying on our tables flyers that we have collected from other organizations. The flyers help change the dynamic of your program from a charity towards a place for discussion and social change. The simple presence of flyers will inspire conversation and community.

We strive to be as visible as possible so our work will have as much impact and reach as many people as possible, by selecting the most visible location to set up our table. Sometimes the ideal situation is in a park or plaza, and other times it is important to set up outside a bank, a corporate office, government building or military installation. 

How often to set up is equally important. The more we are outside, in the public eye, the more our message gets out, and we encourage all Food Not Bombs chapters to set up their literature and meals on a regular schedule to establish a reputation so that the public can pick up information about current events that they might not be able to find elsewhere.

The Food Not Bombs table can be a landmark for activists and travellers seeking to connect with the movement in a new city. The corporations, military and governments promote their message of obedience to their agenda in all manner of high visibility methods and they are having real impact. We don’t have their billions and control of the media so we need to use what we have as effectively as we can to counter their propaganda. Our message is too important to hide.

## Fundraising
Food Not Bombs has a long-standing tradition of being very relaxed about fundraising. We generally prefer to receive money in small amounts rather than large and difficult-to-manage donations from people or foundations that might be quite distant from us personally and politically. We feel it is better to have a wide base of support from the local community with whom we have direct contact than to rely on a few foundations or wealthy people who might manipulate or pressure us into catering to their special interests.

Large contributors might threaten to discontinue their support upon discovering your group is planning to support an action they oppose, and if your chapter has grown dependent on that funding, you might find it difficult to ignore their concerns. While grassroots fund-raising is more difficult and time-consuming, it allows us to remain on the cutting edge of political issues, and also requires constant direct contact with our supporters.

For the most part, we don’t really need much money anyway, as we have no paid staff and most of the food is recovered. Other than printing flyers, buying some gas for the delivery vehicles, and securing cooking equipment, spices and large bags of rice and beans, we have little reason to require much money. When we do need cash, we organize a concert or other creative fundraising event, print and sell t-shirts, patches and aprons, or sell buttons and stickers at rallies, protests and other events.

Your chapter can also “cater” events you feel solidarity with and the sponsoring organization might be able to provide a dollar or two per person eating to help you guarantee items the organizers may require. In a number of situations this has helped local chapters acquire large pots, serving tables or extra bags of rice, beans, spices or oats for future meals.

The people at the Food Not Bombs Global Coordination Office can also provide support from the *Dollar For Peace Campaign*

[^ch2_dollar_for_peace_campaign]: A campaign organised by Food Not Bombs which requests the public donate 1 US Dollar ($1) to support the work of Food Not Bombs towards ending hunger, poverty and war.

 and honorariums. This campaign provided funding for the Katrina Hurricane Relief Effort, bought a vehicle for Food Not Bombs in Africa and provided money for pots, pans, bulk food, and publications for Food Not Bombs groups and regional, national and world gatherings.

Selling merchandise, like buttons, patches and bumper stickers is one way to raise funds for your local Food Not Bombs chapter. Set up a literature table and request donations for buttons (badges), stickers, books, patches, and t-shirts at high volume pedestrian traffic commercial areas, community fairs, farmers markets, concerts or at political events. Being regularly out in the public eye, exercising your right to free speech and collecting donations, has a tremendous effect beyond the raising of funds.

For some groups, receiving donations for buttons and bumper stickers is a your chapter can silkscreen its own patches and t-shirts to raise funds or order items from the Food Not Bombs website as a major source of income. When people ask how much for a major source of income. When people ask how much for a button in the U. S., we might use the phrase “a dollar, more if you can, less if you can’t.” In Europe it could be “one euro, more if you can, less if you can’t,” and so on, depending on the currency.

Purposefully create a loose atmosphere so that people donate what they can without pressure or embarrassment. You will often raise more money and awareness if volunteers stand behind the literature and direct people’s attention to a particular flyer or ask them a question like, “have you heard about our next event?”

At large outdoor events, periodically remember to take the money out of the donation bucket as the day passes so that no one grabs the donation can and runs off with all the money you brought in that day. 

There are designs on the Food Not Bombs website that your chapter can print out and tape to your donations requests to make them attractive. You can also buy buttons, stickers, books, patches, DVDs, T-Shirts, and other materials which you can sell to raise money and awareness for your chapter, for which we have a suggested price list. We also have many inspiring images from other Food Not Bombs chapters which share our message, and which you could use to make materials to raise money for your chapter. 

Other chapters have done other interesting and creative things to make money for their chapter. Many Polish chapters produced their own benefit punk compact discs, and in the 1980s, Food Not Bombs groups in Czechoslovakia and Australia created a benefit album where half the songs were by Czech bands and the other half by Australian bands.

### Catering
Sometimes unions, community groups or political organizations ask Food Not Bombs chapters to provide meals to the people attending their events. 

It might be hot soup at an outdoor rally, lunch for a conference or full meals to activists participating in an ongoing protest in a remote, wilderness camp for the environment or against military training exercises. Most often we provide food for free even to the sponsoring organizations, but at times the sponsoring group might request food we are unlikely to find discarded or want certain meals guaranteed and provide us with a donation of a dollar or more per person in advance of the event.

If a sponsoring organization has a special arrangement like transportation or housing as part of their conference or event, they might ask for additional contributions directly from the people you will serve—this is up to the organizers.

However, if the event is outdoors or open to the general public, the food is always free and never denied to someone because of lack of money. 56 Food Not Bombs street theater in Wellington Food Not Bombs street theater in Wellington, New Zealand before the 2003 war on Iraq At some events, food is cooked at the event site where there may be a commercial kitchen or other cooking facilities, while at other events food is transported already cooked.

It is a good idea to visit the kitchen in advance to make sure you know what additional equipment you should bring when you arrive. Taos Food Not Bombs was asked to provide three meals a day for 200 students at a school that “had a kitchen,” only to discover that the kitchen had no stove. It is important to be on time at all events.

Obviously, this is especially important when you are feeding 100 people lunch from noon to 1:00 PM during a conference. Also, it is usually possible to bring your literature table and set it up next to the food table or in the lobby or hallway at the venue. A Food Not Bombs banner posted on the wall behind your serving table is another great way to let those participating in the conference know that Food Not Bombs is providing the food. Catering is not only a great way to raise a little 

### Concerts, Protests, Gatherings and other Food Not Bombs Events
At some events, food is cooked at the event site where Food Not Bombs chapters can sponsor concerts and cultural there may be a commercial kitchen orandother faevents to have fun raise moneycooking or attention for important issues. Many Food Not Bombs chapters initiate and help orcilities, while at other events food is Iftransported already ganize protests. you plan ahead, your event can be a big success. Whether for rallies, concerts or poetry readings, it is cooked.

It is a good idea to visit kitchen inatadvance importantthe to choose a location and date least six weeks to two months in advance so the details can be listed in local and newspapers. For larger events, it can be a to make sure you know whatmagazines additional equipment you good idea to start organizing as much as a year in advance. should bring when you arrive.

Taos Food Not Bombs Propose the event at the first possible Food Not Bombs meeting. Present reason, goal vision of students the event to everywas asked to provide three meals atheday forand200 one in your group, maybe first by email and then formally at the next meeting. Include a suggested date, time and possible at a school that “had a kitchen,” only to discover that the location as well as the type of performers, speakers or activ- kitchen had no stove.

It is important to be on time at all events. Obviously, this is especially important when you are feeding 100 people lunch from noon to 1:00 PM during a conference. Also, it is usually possible to bring your literature table and set it up next to the food table or in the lobby or hallway at the venue.

A Food Not Bombs banner posted on the wall behind your serving table is another great way to let those participating in the conference know that Food Not Bombs is providing the food. Catering is not only a great way to raise a little money for your chapter, but a way to show support for other movements it also provides an opportunity for other activists to learn more about the work of Food Not Bombs.

We are in a position to speak to many of the participants just because everyone is eager to eat and will often relax near the food. This could be one of the most effective aspects of catering. We are often the one group that connects organizers to each other because we provide to a before wide range of groups on a variNew food Zealand the 2003 war onworking Iraq ety of issues. Concerts, Protests, Gatherings and other Food Not Bombs Events Food Not Bombs chapters can sponsor concerts and cultural events to have fun and raise money or attention for important issues. Many Food Not Bombs chapters initiate and help or- issues.

Many Food Not Bombs chapters initiate and help organize protests. If you plan ahead, your event can be a big success. Whether for rallies, concerts or poetry readings, it is important to choose a location and date at least six weeks to two months in advance so the details can be listed in local magazines and newspapers.

For larger events, it can be a good idea to start organizing as much as a year in advance. Propose the event at the first possible Food Not Bombs meeting. Present the reason, goal and vision of the event to everyone in your group, maybe first by email and then formally at the next meeting. Include a suggested date, time and possible location as well as the type of performers, speakers or activities that could be considered part of your proposal.

You might brainstorm the idea with a few Food Not Bombs volunteers before making your proposal. Your suggestions can inspire the other volunteers to add their ideas. You can then close the agenda item with commitments to tasks such as securing the venue, contacting performers, writing the press release and producing of flyers and web announcements.

The progress of your plans can be reviewed at the next meeting, and if you see that some tasks have not been completed, see if additional people might be needed. When making the arrangements, be sure to get the correct mailing addresses, email addresses and phone numbers of all parties involved so that you can stay in touch.

Send a letter confirming the date, time and other arrangements to the managers of the venue as soon as you can, and once you have the space confirmed, contact the performers and send them letters and emails confirming the date, time, location and duration of their performance.

It would be unfortunate if the performers did not show simply because they never received their letters of confirmation. If the event goes smoothly, these performers will support you in the future. If you are having a concert, find out if the venue is equipped with a sound system or ask the bands if they have speakers, amplifiers and a sound person.

If not, they may know someone who does. Work out a complete schedule in advance with specific times for each performer, including set up and sound check, and be sure to send the schedule to all parties involved including the people whose space you are using, the people staffing the sound system, everyone in your chapter and the master of ceremonies.

If the event includes puppets, fire dancing and special speakers or props, include those details in your schedule and make sure each aspect has someone responsible for bringing the proper items: setting up the necessary support such as electricity and extension cables, costumes, banners, backdrops, lights, video equipment, fire extinguisher etc.

Create a time line for each aspect starting with the person that has the key to open the door and his or her arrival time. If organizing an event in a park or city building, you may need to reserve the space or get insurance. Because we are a low budget community group, the cost of such items can often be waived Drumming during meal in Nairobi, Kenya 57 or reduced.

In the United States, governments cannot require costly fees for free speech events in public spaces. Your volunteers may be so busy organizing the regular food and literature distribution that inviting bands, clubs or other performers and artists to take responsibility for the event can not only free you from these details, it is a great way to inspire more community members to have a deep connection to the goals of Food Not Bombs.

Another good idea is the distribution of flyers to advertise the event to local organizations six weeks in advance. A PDF of the flyer can be posted on your chapter’s website and emailed to other community websites along with a text version of your announcement that can be posted to online calendars and blogs. You may have a local Indymedia site on which you can post the announcement, add the flyer as an attachment and email your lists and listservs with the text and ask everyone to forward the announcement to their lists.

You can build an email list by having a sign-up sheet at your regular food and literature table. An announcement in monthly print newsletters or calendar listings can be very valuable.

In addition, post flyers all over town and put them on your table for one month in advance. If you are organizing a concert, you may want to hand out flyers at other concerts in the weeks before your event. Make sure all the bands and other participants have copies to distribute themselves. Hand out flyers at protests and other events where people interested in your event might be attending.

Email a thirty second public service announcement to local radio stations and make a follow-up phone call to be sure the announcement is received, suggesting it be put in their public service announcement folder. You may be able to arrange an interview with performers and volunteers of your chapter on local radio programs as well as with local publications, daily papers and weekly magazines. These larger news reports can attract not only a larger audience, but new volunteers and interest in your local effort. Your group’s volunteers can set up a literature table with flyers, buttons, stickers and shirts at the venue before the event.

Depending on the type of event being held, you may want to ask for a donation at the door or “pass the hat” during the show. At bigger events, you may want to create a printed program that can also be an opportunity for fundraising. You can sell ads to local groups and businesses. And, of course, a table with refreshments provides a good opportunity to have a donation bucket to raise additional donations.

It might be good to organize events like this every month or two as a way to attract more volunteers and build support with people who would otherwise not learn about the work of Food Not Bombs. These events can energize your volunteers and provide an opportunity to have some fun and to meet new people.

## MODELS USED TO ORGANIZE A LOCAL FOOD NOT BOMBS GROUP 
During the 30 year history of Food Not Bombs, local chapters have developed several models adapted to the needs of their community. These are a few that your chapter might adopt.

### Boston Model
The first model featured one collective working out of a couple of homes. Every morning Food Not Bombs volunteers would drive around the Boston area picking up unsold food from groceries, bakeries and produce markets.

After collecting the food they would drive to public housing proj- ects, soup kitchens, shelters and other food programs to distribute. There was a regular schedule of pickups and deliveries. We delivered groceries once a week to the community rooms of public housing projects and maybe every day to the larger shelters or soup kitchens.

As volunteers delivered the groceries, other Food Not Bombs activists would be preparing vegan meals to share that afternoon and evening. While they were cooking, a couple of other volunteers would be staffing a literature table in Brattle Square or Park Street Station. Musicians and street performers would join the literature table during the late afternoon.

The literature table might include a pot of hot cider being warmed on a propane stove. Once the food was prepared it would be delivered to the literature table. While the food was being shared, other Food Not Bombs volunteers would be playing music, performing with puppets or giving Eric Weinberger shares food in the Boston Commons Food Not Bombs in San Francisco, California speeches.

When the weather was good, other artist and political groups would join us with their information or artwork. We had a policy that we could not have two organizing meetings without sharing food inbetween. Other groups had meeting after meeting without ever getting out in the public. Boston Food Not Bombs met randomly, which made it difficult for new volunteers to join. Boston Food Not Bombs also provided meals and literature at protests all over New England.

This introduced many hundreds of other activists to Food Not Bombs and was a major way of attracting new volunteers and food donations. 

### San Francisco Model
The volunteers in San Francisco collected discarded food every morning from groceries, bakeries and food warehouses.

The food was dropped off at the kitchen where volunteers would gather to prepare that day’s lunch. Huge sixty quart pots and other cooking equipment would be delivered with the food. While that crew was preparing lunch, the drivers would head back out and pick up more food. They would return in time to load up the prepared food and some of the cooks and head to Golden Gate Park, Civic Center or U.N. Plaza where they would unload the food, set up the tables, banners, signs and literature. 

Musicians and other performers might arrive at that same time and perform near the table. We would be greeted by 100 to as many as 500 people waiting in line. Some volunteers would take bagels or other baked goods to the back of the line and slowly hand out these items to each person as they walked from the last person in line towards the front.

Boxes of produce, bread and other unprepared food would be set out near the literature table. We often brought used shopping bags for people to fill. Food Not Bombs in Portland, Oregon After lunch the driver would drop off the mid-morning pickups and cooking equipment to the kitchen preparing dinner.

Any surplus food would be delivered to soup kitchens and shelters. The driver would return to pick up the prepared meals and volunteers and deliver the food, tables, literature and banners to Civic Center or U.N. Plaza. Once again bagels and pastries would be handed down the line, and uncooked groceries would be placed near the literature table for people to “shop for free.”

The people visiting San Francisco Food Not Bombs included both the homeless and the housed so it was valuable to have both a hot meal and groceries. The homeless had no way to prepare food and the hot meal was also a great way to create a space for the housed to engage in conversation with Food Not Bombs and the unhoused about current issues.

This provided a basis for building resistance to oppressive laws and exploitive policies and inspired involvement in projects like our free FM radio stations, Homes Not Jails, and campaigns against police violence. About forty volunteers would meet every Thursday evening to plan that week's activities. The meeting would start with everyone introducing themselves and requesting, if necessary, that an item be added to the agenda.

After the introductions, one volunteer would explain how to use the process of consensus, and an- other would state the three principles. The next agenda item was pickups, then kitchens and cooks followed by servers and pot washers.

The next agenda item was called “Solidarity” where we discussed the actions we would provide with food and how we would fit these actions into our regular schedule. We sometimes had an agenda item to discuss the planning of our own benefit 59 concerts or strategies to address new anti-homeless laws.

If we were being arrested during our meals, we might have an agenda item about our legal strategy. We had a note taker who could remind us of past decisions and details of where we had been in the discussion of agenda items that we had not yet come to consensus on.

We also had committee reports. We had committees on increasing respect for women’s ideas in Food Not Bombs, the planning of gatherings, benefit concerts, tours, the writing of pamphlets or flyers and the organizing of our nearly annual free concert called Soupstock.

We encouraged the people that depended on us for food to participate in the meetings, and this created a powerful alliance and helped erase the usual hierarchy of charities. This model was very powerful because of the regular weekly meeting, which allowed new volunteers to participate fully in every aspect of San Francisco Food Not Bombs.

We often shared food and literature as many as fourteen times a week between meetings maintaining the philosophy of Boston Food Not Bombs to never have a meeting without having taken action.

### Portland Model
Portland Food Not Bombs introduced the idea of having a kitchen for every meal so they had a Sunday Food Not Bombs where housemates would have their own pots and cooking equipment, pick up the discarded food to cook and distribute and their own literature and banner.

This really reduced stress on those living at each house and it saved volunteers from burning out. It also encouraged housemates to participate and involved more people to take part. There were Sunday, Monday, Tuesday, Wednesday, Thursday, Friday and Saturday Food Not Bombs groups.

East Bay Food Not Bombs also had houses and kitchens for each day of the week, but they also added a regular meeting that brought all the volunteers together to discuss solidarity actions, publications and other projects. Portland Food Not Bombs provides food to a large homeless popula- tion and focuses on daily hot meals which they often share under a bridge near the river to help keep out of the rain.

### Richmond Model
Richmond Food Not Bombs volunteers collect discarded or surplus food in the days before their weekly meal. The food is delivered to a kitchen Sunday where volunteers gather to prepare the meal. For many years they cooked at a church. They also cooked in the homes of volunteers.

Once the meal is ready they load it into their vehicles and drive it to Monroe Park where there 60 Food Not Bombs in Richmond, Virginia are both homeless people and many students from the local college. Volunteers arrive at the park, set up a circle of tables; one for the main course, one for drinks, one for desserts, one for groceries, one for clothes and one for literature. They hang banners between the huge trees that shade the meal.

Most of the people participating are homeless, but they do have students and low income people that are excited to get free groceries. The volunteers hold a meeting in the park at ceries. The volunteers hold a meeting in the park at the end of their weekly meal. Richmond Food Not Bombs has provided support for many local efforts and often shares meals outside City Hall during public hearings on city policies against the homeless or other communities.

### Long Island Model
Long Island Food Not Bombs collects discarded food from all over the island, and they set up distribution sites once a week near transit stations or bus stops in different communities. Since most of the people depending on Long Island Food Not Bombs for food have housing, they focus on distributing free groceries and free clothing.

The volunteers meet at one house to cook, prepare the meal and load their vehicles with groceries and the hot food. Volunteers participate in each of the distributions. They don’t meet very often as the area of Long Island, where they share food, is huge and the volunteers often travel long distances to participate.

They use a sophisticated website and phone system to communicate with one another. The website is also designed to encourage discussion with the wider community.

### Nairobi Model
Nairobi Food Not Bombs buys and collects food that could be discarded because they have the funds. They hire a taxi to pickup both the food that they buy and the discarded food.

At first they took the food and their cooking equipment to the location where they intended to share their meals. Since poverty and hunger is so widespread, they focus their distribution on orphaned children and the least advantaged youth living in the roughest shanty towns of Nairobi.

Since the children were so excited when Food Not Bombs came to their community, they worried that the children might be injured by the propane stove so they started cooking at an apartment off-site and brought the prepared meal to the community center where they share their food. After sharing the meal, the organizers teach classes in journalism, writing and photography with the goal of fostering self respect among the children.

Groups in other countries also teach classes during meals. Food Not Bombs chapters in the Philippines often hold art classes for the children who came to eat. The artwork would be hung on a wall during the meal for the children to see and comment on.

### Reykjavik Model
Like many Food Not Bombs groups, they collect their food in the days before their weekly meal. The idea they introduced was, in addition to providing flyers and a Food Not Bombs banner at every meal, they also display signs where they write out the text of their main flyers in Icelandic on one side and English on the other. Since it is so cold and windy, people have an opportunity to read about the principles and goals of Food Not Bombs without having to take off their gloves.

Their location was in the most visible place in Reykjavik, and people that happen by are attracted to both the food and the signs, and they take time to read about our movement. Food Not Bombs stores their signs and table at a nearby club so they don’t have to carry everything from the kitchen. 

### Besançon Model

The volunteers in Besançon, France collect food during the days before the meal. They have constructed a system of boxes made with foam to keep their pots of food warm. They share food the first and third Saturday of each month in a popular plaza outside their city’s infoshop where they store their tables, chair, signs and banners. They set out tables and chairs around the plaza.

They hang several banners between trees and they have enlarged posters and mounted on large boards which they lean against the walls around the plaza. They share a huge number of dishes to people that sit at tables decorated with table clothes and flowers discarded by a local florist. At first, there is acoustic music, but once everyone is eating, the Capoeira music and martial arts begins. Capoeira is also spreading to other Food Not Bombs meals.

## FOOD NOT BOMBS AND THE STATE
### Non-profit, Tax-exempt
Status Food Not Bombs is considered an association and is almost never required to officially register with government officials. Each country has its own laws about fundraising, and in most cases, Food Not Bombs has little or no interaction with revenue or tax collectors.

Some African countries require groups like Food Not Bombs to pay a fee to start up, but the details can be fluid with the governments changing.The United States is a special case. Every Food Not Bombs chapter is an association and does not need to be registered with the authorities. However, it is quite common for people in the United States to ask if we are a nonprofit, tax-exempt organization. Generally, we are not profit, tax-exempt organization.

Generally, we are not interested in the bureaucracy needed to maintain such an organization. The amount of money we collect and spend is so small that each group can keep account of its finances at their regular meetings and record the small denominations in a notebook.

At times, a local foundation or community group may want to make a larger contribution but this is so rare it does not require seeking non-profit, tax exempt status which, in 2010, can cost a few hundred dollars to start with and can invite interference by the U.S. Internal Revenue Service (IRS). Your local chapter might want to use an “umbrella” or “fiscal sponsor or agent” to assist in arranging a particular donation of money that specifically needs to be given to a nonprofit, tax-exempt group.

This is fine. It is usually not too difficult to find a local tax-exempt organization to be your fiscal sponsor. The people wishing to make a large contribution would make their check out to this fiscal agent or nonprofit group, and the fiscal sponsor would make a check out to someone in your group or pay directly for the items.

Your chapter needs to provide the name of the tax exempt group to the people making the contribution before they make out a check to Food Not Bombs. Specifically, however, we urge you not seek permission from any government agency to engage in the work you do.

Once a group becomes a tax-exempt organization, the IRS or other tax collection agencies have the right to oversee all aspects of your chapter’s operation, and this can limit much of what you can do. Rather than try to hide from the IRS, we prefer to ignore them. The only time we ever need to provide proof of being a nonprofit group is when some national chain groceries or bakeries ask for an IRS letter.

The global coordination office can provide your local chapter with a letter for food donations, but if you are receiving money, you will need to contact a local nonprofit or talk with the volunteers at the global coordination office to see if they can help. While it is fine to accept some funding from foundations, it is not wise to become dependent on them. To receive money from a foundation, your chapter Police take literature and charging volunteers with possession of stolen property for the milk crates 62 will need to have a nonprofit group provide you with their tax letter and a letter from that group stating that they are your fiscal sponsor.

Foundations might use their financial support to influence the direction of your chapter by threatening to withhold funding if you provide food and support to groups taking positions on issues that may be objectionable to the board members or staff of these funding agencies. A foundation might suggest that your chapter not participate in a protest in support of the environment because their donation to you in support of the environment because their donation to you could threaten financial support to their foundation by local developers.

An action outside a weapons factory might cost the foundation the support of wealthy defense contractors, or protests against anti-homeless laws could cut off economic support for foundations funded by the owners of local retail stores.

The owners of fur coat stores might withhold contributions to a local foundation if they see Food Not Bombs is providing food to animal rights activists targeting their shops, ending future contributions to your chapter. It is better to be free to support the environment, peace, animal liberation and human rights actions than to rely on grants from foundations.

### Legal Tips 
The act of sharing food and literature is an unregulated activity in most if not all countries. Government interference is rare in every country but the United States. Considering how many groups are active in the United States, we really have few problems, but they do arise from time to time. Food Not Bombs volunteers can face a number of legal issues in the United States with its more restrictive legal system.

These legal issues may also be of interest for some of our chapters in other countries as well. The first legal issue Food Not Bombs volunteers can face is that of permits.

In most countries, the idea that anyone would be asked to apply for a permit to share free food and literature would never happen.

In some countries, police may ask a volunteer to give their name, phone number or address the first time they come to the attention of the authorities, and after that nothing more happens between Food Not Bombs and the local government.

Even in the United States it is understood that sharing free food and literature is an unregulated activity, yet government officials, police and security guards sometimes confront our volunteers in an effort to silence the meal with its message. Another possible legal issue, mostly limited to the United States, is the legality of having literature and a banner visible while sharing food.

The First Amendment of the U. S. Constitution protects the right to religion, free speech and assembly, but this has not stopped local, state and federal officials from taking our literature and banners. The police have taken our banners and literature after issuing tickets. At times, volunteers are told that they can share food as long as they don’t have a banner or literature “because then they are an organized group.” Food Not Bombs always has the constitutional right to have a banner and literature at every meal.

Any time our volunteers are arrested, issued a citation or have their literature or banner taken, it has inspired the community to rally to our support. So far, this has not been much of a problem outside the United States. Two volunteers were arrested and released on site in Brixton and police in New Zealand threatened to arrest volunteers one time during a Buskers Festival.

Police also arrested over ten volunteers in Utrecht, Netherlands but claimed it was a mistake thinking they were selling food without permits at the train station. All other arrests outside the United States have been for participation in protests, not for the sharing of food. This could change as the global economy continues to fail and more countries adopt the repressive policies of the United States.

The third legal issue, also mostly concerning Food Not Bombs groups in the United States, is the federal government’s claim that our volunteers are involved in plots and acts of terrorism or political violence. This third category of legal issues is very serious, and not only requires competent legal assistance, but political support from Food Not Bombs groups all over the world. Any Food Not Bombs volunteers arrested for any terrorist or political violence related charge should insist that their lawyers call the global coordination office and ask that their case be posted on the website.

Nearly twenty Food Not Bombs volunteers have been sentenced to long prison terms after being accused of serious felonies in the United States. These volunteers might be free today had their lawyers known that their clients were victims of a nationwide campaign of infiltration, surveillance and political repression directed against the Food Not Bombs movement. A few Food Not Bombs activists in other countries have also been accused of terrorism.

A volunteer with Minsk Food Not Bombs, Tatiana Semenishcheva, was arrested by the Belarus authorities in 2010, a few months before the national election, and charged with participation in a plot to fire bomb the Russian Embassy. Food Not Bombs activist Artyom Bystrov was arrested in February 2012 in Nizhny Novgorod, Russia and charged with organizing an “extremist community” under the exotic name of “Antifa-RASH.” The Food Not Bombs community is taking a number of measures to make sure that all our volunteers will be safe from this type of government entrapment in the future.

### The Revolution Doesn’t Need A Permit
Sharing food and literature for free with the public is an unregulated activity. The government would never issue a permit to have its policies challenged unless they intended to revoke the permit when the group’s activities start to threaten the power of the state. Common interpersonal activities like sharing coats, blankets, books, ideas and food between people for free does not require a permit, even in countries like the United States.

People sometimes tell our volunteers that it is easy for projects like Food Not Bombs to get a permit from city officials and claim that it makes sense that the authorities would need to know that your group will be using a city sidewalk or park. They make it sound simple. You give them the name of the organization, your group’s mailing address and a phone number, and they give you a permit. If the permit policy is really that simple, you might look into it, but we suggest you don’t tell the city the identity of your group until you know for sure. A Case In Point: In 1981, a Food Not Bombs volunteer filled out the street performers permit request for our food and literature table at Harvard Square in Cambridge, Massachusetts. The half size page asked for our name, mailing address and phone number. That was all.

Some years we remembered to fill out the page and turn it into the clerk at Cambridge City Hall. Some years we forgot. Our activity was considered like that of any other musician, mime or juggler busking for money in Harvard Square. The police never restricted our activities in Cambridge. The story was a bit different in San Francisco, California. On July 11, 1988, after serving for several months without city interference, the San Francisco Food Not Bombs group wrote a simple, one-page permit request to the Recreation and Parks Department at the suggestion of a community organizer believing San Francisco’s permit policy would be the same as we had experienced in Cambridge. This unfortunately alerted the government to the meal distribution program and gave the city an opportunity to deny us a permit.

On August 15, 1988, fortyfive riot police emerged from the fog of Golden Gate Park and arrested nine Food Not Bombs volunteers. The next week the police arrested twenty-nine volunteers, telling the media that they arrested us for making “a political statement.” However, the charges were for selling food in the park without a permit. The police 63 arrested fifty-four people the first weekend in September. The mayor became worried and held two afternoon’s of meetings with Food Not Bombs and community leaders. Mayor Agnos issued a six-week temporary Recreation and Parks Department permit so he could stop the arrests.

The public was outraged at the arrests and so many people volunteered with San Francisco Food Not Bombs that the chapter added a Tuesday lunch at the Federal Building and a meal Wednesday at the farmers market at United Nations Plaza. San Francisco Food Not Bombs was finally issued a food facility permit after a federal judge ordered the city to stop changing the rules each week. However, the next summer, the authorities believed that providing free meals to the hungry interfered with their efforts to drive the homeless from the city so our volunteers started to get arrested for not having permits to share meals at the Federal Building and UN Plaza.

A huge earthquake ended the arrests in October of 1989, but when the chief of police won the mayor’s race he tried to drive the homeless out of sight. Frank Jordan ordered the city attorney to get a court order against Food Not Bombs for sharing food without a permit, and ordered his Recreation and Parks Commission to and ordered his Recreation and Parks Commission to “delete” the process for obtaining a permit and the food facility permit was “temporarily suspended” because the group couldn’t get the “deleted” land use permit.

San Francisco Food Not Bombs applied for a recreation and parks department permit over 100 times, but never received a reply from the city and the police continued to make arrests. The arrests continued even when the group moved to UN Plaza where the San Francisco Board of Supervisors had final say on permits. The board of supervisors voted twice to let Food Not Bombs operate at UN Plaza but that didn’t stop the police from making hundreds of arrests at that location.

In 1997, the city finally stopped arresting San Francisco Food Not Bombs volunteers.

After nearly 1,000 arrests, many of which were very violent, and tens of thousands of tax dollars wasted, the chapter continued to share free vegan meals and literature in San Francisco. Other city and state governments in the United States have also used the permit issue in an effort to silence Food Not Bombs. These officials have made claims that we are required to have permits for a number of items including the right to display our banner, the use of a public space or the operation of a food facility.

As far as needing a permit to share meals, state laws claim that all food facilities are required to have a permit to sell food. On the other hand, sharing free food with the community is still an unregulated activity in those states. Government concerns about food safety are really an excuse to silence the message of Food Not Bombs.

The origins of these problems generally start when a military contractor takes notice of our program or when a retail area embarks on an anti-homeless campaign. 64 San Francisco Police guard food from hun San Francisco Police guard food from hungry at Golden Gate Park on August 15, 1988 There can be a progression of permit issues starting with a need to have a permit to use a public space, to a requirement to have a sign permit, to the well used excuse that we are required to have a Food Facility permit.

In the only case of its kind, Connecticut General Statute 19A-36 was changed on October 3, 2009 to allow Food Not Bombs and all other residents of the state share meals without interference as a result of the arrests of the Middletown Food Not Bombs volunteers.

The state was also ordered to pay Food Not Bombs $15,000 for the illegal arrests. concerned when he saw that the national Food Not Bombs website mentioned dumpster diving as one way There can be a progression of permit issues starting with a to collect food for the potlucks.

In fact, that website, I have maintained since its founding in 1997, need to have a permit to use awhich public space, to a requirehas never suggested dumpster diving as a way to collect food, but has always encouraged volunteers to ask ment to have a sign permit, to the well used excuse that groceries, markets and bakeries for the food theywe can’t sell. We can recover better quality and larger amounts are required to have a Food Facility permit.

In the only of food by working with employees in the food industry than by dumpster diving, even though the vegan case of its kind, Connecticut General Statute 19A-36 was case of its kind, Connecticut General Statute 19A-36 was changed on October 3, 2009 to allow Food Not Bombs and all other residents of the state share meals without interference as a result of the arrests of the Middletown Food Not Bombs volunteers. The state was also ordered to pay Food Not Bombs $15,000 for the illegal arrests. Authorities will tell the media that they are worried about the safety of our food while permitting every type of hot dog cart and fast food operation.

Salvatore Nesci, Chief Public Health Sanitarian of Middletown, Connecticut, testified that, “We have reason to believe that individuals have been taking food out of dumpsters in the name of Food Not Bombs. From the research we have done nationally on the Food Not Bombs movement, we’ve come to find they actually advocate for this. We just can’t approve something like this.

That’s why we are being adamant. ” AugustGate 12, 2009, article by Monica y The at Golden Park on August 15, 1988Polanco in the Hartford Courant reported that, “Rehm said he became concerned when he saw that the national Food Not Bombs website mentioned dumpster diving as one way to collect food for the potlucks.

In fact, that website, which I have maintained since its founding in 1997, has never suggested dumpster diving as a way to col- lect food, but has always encouraged volunteers to ask groceries, markets and bakeries for the food they can’t sell. We can recover better quality and larger amounts of food by working with employees in the food industry than by dumpster diving, even though the vegan food one finds in dumpsters is most likely just as safe.

Many of the people we feed would have to rely on eating out of dumpsters if it wasn’t for our meals, so it makes no sense to stop Food Not Bombs. The safety record of Food Not Bombs is very good. Health officials have no reason to worry.

No one has ever been made ill eating with Food Not Bombs during the past thirty years in any of the nearly 1,000 locations where we share meals. Our food is vegan and vegetarian and often organic. We also share this fresh organic food within the three hours required for commercial food facilities to comply with sanitation laws, thus making our meals much safer than non-vegan, commercial establishments. Food facility permits are designed to protect the public from becoming ill when commercial facilities cut corners to increase their income.

Food Not Bombs has no incentive, financial or otherwise, to share food that is not safe. No one has ever been made ill because of the way the Food Not Bombs program is designed. Our volunteers follow these simple steps to make sure our meals are always safe so the effort to close us down because of unsanitary conditions is not based in law or in a concern for the health of the hungry. The main safety measure we implement is that all our food is vegan.

This fact alone reduces the risk that anyone will become ill. We also share the meals before harmful bacteria have a chance to threaten people’s health. With so many people going hungry, it is not easy to have enough food prepared to provide meals for longer than the three hours it can take to make people ill. Bacteria multiply most rapidly between 40°F (4° C) and 140°F (60° C), a range known as the Food Temperature Danger Zone, and after three hours in this zone, bacteria may start to become a safety issue, particularly if the meal includes meat or dairy.

Your meals will leave the stove above 140°F (60° C) and still be at that temperature by the time you are sharing it with your community. We also ask that volunteers that smoke to wait until they are finished cooking or serving the meal and remember to wash their hands after they take a smoke break before returning to cook or share food.

Washing your hands with soap and warm water after going to the toilet is also an important way to protect the community when providing free vegan meals. On the other hand, industrial food sold with permits can be very harmful. Products like factory produced meats and corn syrup-based food are causing huge health problems. Governments are even issuing permits for genetically engineered products which are known to activate many life threatening illnesses.

Simple, low-tech practices like washing our produce and our hands, and preparing only organic, vegan meals in a short amount of time, protects the community we are feeding. So while sharing food is an unregulated activity, we still don’t want to make anyone ill.

If your chapter feels pressured to apply for a permit, don’t let the government stop your group from sharing food. Although the government may create reasons for denying you a permit, you should not be intimidated. Make it clear that you are willing to adopt any proposal which will make your operation safer and more successful, but you also will not agree to any demand that makes it impossible for you to continue your operation.

Remind the officials that sharing food, like sharing clothing, blankets, books and ideas is an unregulated activity. We have found that even after long hours of meeting with government officials, hard earned permits can be revoked at any moment. From the government’s 65 point of view, a permit is something they can take away.

If the government is pressured to stop Food Not Bombs, they can revoke your permit if you have one. If you don’t have a permit they can’t take it away and this can make it much more difficult for the government to silence Food Not Bombs. (Remember the history of “Indian Treaties” in the United States?)

Because of this, we strongly recommended that you NOT contact the local government. As we have stated, “The revolution needs no permit.” The inability of the government to control the activities of Food Not Bombs can be very threatening to officials.

Denying hungry people food is a strong way to control them. The authorities were not eager to have people provide food in New Orleans after Katrina. As the global economy fails, efforts to offer or withhold food to hungry people to control local populations may become more common. Government authorities may use another tactic against Food Not Bombs chapters. They may tell our volunteers to move their meal to a less visible location.

Officials are happy that your group is feeding the hungry, since they can’t afford to provide meals themselves, but they also don’t want your groups sharing food where the public will be able to see that there are so many people in need.

The government may pass local laws with the goal of making it illegal to share food with the hungry in designated locations where you would be able to reach large numbers of people. This has been one strategy used since the first arrests of Food Not Bombs in San Francisco. After failing to end our meals by making nearly 100 arrests, they agreed to let us share food and literature, but claimed after two days of negotiation that we had agreed to set up our meals behind a wall of bushes in Golden Gate Park.

It was not possible for people walking into the park to see us and not one person, other than those needing to eat, visited our table until we moved it back to the entrance at Haight and Stanyan when the city started arresting us in Civic Center Plaza. City officials all over the United States have made efforts to hide the homeless and meals to the homeless.

In November 2009, officials in St. Augustine, Florida, moved Food Not Bombs to a parking lot behind city hall, making outreach, on the issues like redirecting military spending towards the needs of America’s poor and working people, more difficult.

The public passed by their location at Plaza de la Constitucion’s gazebo and that was a problem for city officials. Business leaders were still unhappy. A story about the move in the October 21, 2010, issue of the St. Augustine Record, reported that “Glenn Hastings, executive director of the St. Johns County Tourist Development Council, said the new feeding site may keep attracting the homeless downtown.”

The city of Orlando tried to move Food Not Bombs out of site and, like other cities, made a special homeless-free zone and city laws designed to hide their community’s poor. If Food Not Bombs had complied with the desires of Orlando’s business interests, they would have become invisible along with Orlando’s homeless, making political organizing to change the policies causing America’s economic crisis completely ineffective.

Orlando is taking the first amendment right to freedom of expression through the federal courts. It is our right to be as effective as possible at influencing the public and that includes sharing food, literature and our intention to change society in a location where we can make an impression on as many people as possible, even if that location makes those who wish to silence our message uncomfortable.

After all, they already control almost all other aspects of mass communication and have erased us from the public dialog in most other venues. Public space is one of our last platforms to share our vision to people other than our own friends and family. We shouldn’t let the corporations and government deny us that too.

### If The Government Tries To Shut Down Your Meal
In most communities, the authorities will recognize that sharing free food and information is an unregulated activity.

In the United States, Food Not Bombs is protected by the First Amendment of the U.S. Constitution.

There are similar protections in most other countries and the Universal Declaration of Human Rights also protects our right to assemble and share food and information. Even though our right to free expression is protected, Food Not Bombs volunteers have been arrested in the United States for sharing food and literature.

While this is rare, there have been times when Food Not Bombs volunteers have faced arrest and it becomes necessary to defend our right to free expression. Noncooperation with unjust laws and policies can be a powerful way to influence society. Feeding America’s hungry is today’s version of Rosa Parks sitting in the front of the bus or Gandhi’s Salt March.

If the police or environmental health officials confront your chapter, this can provide you with a great opportunity to educate your community about hunger, militarism, capitalism and the power of noncooperation.

It is also a great opportunity to attract interest in your local group, inspiring more people to volunteer and contribute food, supplies and money. If your group takes these simple steps, you will be able money. If your group takes these simple steps, you will be able to use the threat of arrest as an opportunity to have greater impact, not only in your local community but, around the world. 

Your successful resistance will help future groups if they are faced with arrest. On the other hand, if the government stops your chapter, it will empower other cities to believe they have Police take food in San Francisco the right to stop Food Not Bombs. Follow these dignified steps and discover the power of nonviolent resistance.

This steps and discover the power of nonviolent resistance. This could change your life. When the police or other government officials ask your group if you have a permit, calmly explain that you are sharing food and literature, and that you are charging no money so that your activities do not require a permit.

If the officials are persistent, you can politely ask the police officers to provide their names or badge numbers. Take pictures, even with a cell phone if that is all you have. After the authorities confront your Food Not Bombs group, call a meeting as soon as possible and invite the community to participate, letting them know the details of what happened, in your conversations, emails and flyers.

Post these details, along with any photos of the authorities visiting your meal, to your group’s website and email the account to the local media, other community groups and the Food Not Bombs listservs. The agenda can include a short report about the events surrounding the government’s effort to silence Food Not Bombs.

Ask members of your community to bring a video camera to the meal to film any possible arrests. Video footage of volunteers being arrested sharing food can be useful in court as well as in building support. The only arrest to go to trial in San Francisco was the one arrest that was not filmed. The district attorney re- alized a jury would not convict if they saw what was really happening.

Then, discuss your plans and invite participants to volunteer to implement those plans. The most effective strategy has been to announce that you plan to risk arrest at your next regular meal. Ask for people to consider risking arrest by sharing food. People on probation or with past serious convictions might want to volunteer to be support people. So far, there have been few convictions and the penalty has generally been mild.

Each volunteer that is risking arrest should have their own support person who follows the server through the legal system. They should write down the badge number and name of the arresting officer; note the charges and time of arrest; and be prepared to feed their pets, water their plants and contact their employers if it turns out they may miss work. Each support person should have the correct name and birth date of the person risking arrest; should understand what that person intends to do once arrested; and they might have a key to their home and phone numbers of their employer, family and friends.

Your Food Not Bombs group can also choose a volunteer who will speak with the media. They might note things about your right to share food and literature, and that your community has many people that need food while billions of dollars are being spent on the military. Some groups also point out that no one has ever reported having been made ill eating with Food Not Bombs, that the food is vegan and vegetarian and is designed to be safe. Keep the message focused on the most important points in a clear and simple way.

Food is a right and not a privilege, and priorities can be redirected towards our basic needs and away from the military and profit. Make a flyer inviting everyone to attend a rally at your next regular meal and point out that the authorities are threatening to shut down your program. Post the flyer all over town and email as many groups and individuals as possible that Food Not Bombs will be defending its right to free expression and association by risking arrest for sharing the regular meal.

Email and call the local media to let them know that several of your volunteers plan to risk arrest. Contact local lawyers from groups like the American Civil Liberties Union and National Lawyers Guild and tell them what you plan to do.

In most cases, the authorities back down when they see how much community support your chapter is getting and no one gets arrested. When faced with the possibility of arrest for sharing food, we found it helpful to divide up each course of the meal into three parts; two small amounts that your group is willing to lose to the police and one last amount that your chapter can share with everyone that came to eat after the police take away your first two waves of volunteers.

It might also be wise to make several rough “Food Not Bombs” banners for the police to take during each arrest as that is often the first thing they take. Then, bring out your regular banner after the arrested volunteers are removed from the area. Let everyone know they will get to eat before the police start to arrest your volunteers.

If the people who depend on your meal know that they will get to eat after the police have made several arrests and confiscated some of the food, you will be able to avoid panic and the possibility of violence. If the police arrive and confiscate the food but don’t make any arrests, then you may want to hold onto the food containers during the second wave of sharing. It can be very disempowering if the police keep walking away with the food you have prepared, but if you hold on to the plastic buckets and boxes of food when they attempt to seize your meal this will cause them to make arrests.

These arrests will highlight the nature of the government’s true intentions and will build support. Each court appearance provides another opportunity to discuss the policies causing homelessness and hunger and also gives you a chance to share more food in defiance of the authorities. Your group will attract many new volunteers, more people to your meals, and an increase in food donations as a direct result of the arrests.

Each new wave of arrests will draw more people to your group, and before long, you will be sharing meals three or four days a week. If the arrests continue, you will have so many supporters your chapter will be sharing every day and people will be starting new Food Not Bombs chapters in cities throughout your state or region.

### Nonviolence Preparation
Once you have a number of people who have agreed to risk arrest, it would be a good idea to contact some knowledgeable local activists and supportive lawyers and tell them of your plans. Your chapter can announce that it will be holding a nonviolence preparation where you will spend a day preparing yourselves for the arrests by role-playing the scenario, discussing how you might respond to various situations, and considering the legal consequences of your actions.

In most areas, local peace groups will be able to direct you to people who can lead nonviolence training. The War Resisters League in New York City has a national directory of trainers and a handbook for preparations.

If you cannot find an experienced trainer, gather the group together for a day and conduct your own preparation. We also provide several pages in the appendix you can use for your nonviolence preparation the appendix you can use for your nonviolence preparation and the website provides materials to use as well. Talk about what might happen and some of the ways that events could lead to violence. Hungry people can get very distressed if they feel the police are going to stop them from eating so make sure you have a plan to reassure everyone. Discuss how to respond nonviolently. Then do some roleplaying and act out some of the possible scenarios, with some people playing the police and others the activists. This is both very educational and helpful for you to overcome your fear of arrest. Legal consequences, jail solidarity, and legal defense for trial, if any, can also be discussed at the preparation.

IF THE POLICE START TA IF THE POLICE START TAKING YOUR FOOD nvite the community to participate; ask the National Lawyers Guild for legal observers; and alert the media to the possibility hat the authorities may try to disrupt your meal. Before taking he food out to be shared, determine who is willing to be arrested nd who wants to volunteer to be a support person. You may want to spend an afternoon in a nonviolent preparation workshop o practice. If the police start taking your food or arrest the servers, we found that dividing the meal into three parts and sharing a mall amount of food the first two times works well. We put the oup and salad in 5 gallon plastic buckets with lids. If the police ake all your food at once, it can be very discouraging. Invite the community to participate; ask the National Lawyers Guild for legal observers; and alert the media to the possibility that the authorities may try to disrupt your meal. Before taking the food out to be shared, determine who is willing to be arrested and who wants to volunteer to be a support person. You may want to spend an afternoon in a nonviolent preparation workshop If the police start taking your food or arrest the servers, . to Tell practice. those people who are waiting for food that they are welome to stay after the first food is taken because more food is we found that dividing the meal into three parts and sharing a n the way. Let them know that the police are only taking part f the meal. This will help calm the crowd. If those waiting to small amount of food the first two times works well. We put the at feel that the police are taking their meal, the crowd can beome very angry give thein police a chance to start fightsbuckets with lids. If the police soup andandsalad 5 gallon plastic with the hungry. take all your food at once, it can be very discouraging. take all your food at once, it can be very discouraging. 

1. Tell those people who are waiting for food that they are welcome to stay after the first food is taken because more food is on the way. Let them know that the police are only taking part of the meal. This will help calm the crowd. If those waiting to eat feel that the police are taking their meal, the crowd can become very angry and give the police a chance to start fights with the hungry. 2. After the police have left the area, bring out more food but still leave some hidden so if the police come back you will still have more to share. 
3. Very rarely do the police return a third time because they are already feeling very foolish by the second time. 
4. If the police stay and guard the area, you can often get them to leave by sharing a token amount of food. After they try to stop the serving a few times they realize that it’s better to leave the area rather than stay and show that their authority can be successfully challenged. 
5. If you continue to stick to your serving schedule, the government will give up and you will build the respect of the people. Don’t stop because of the police. Make each meal an event in support of the right to share food and invite the media and community members to attend. Call our toll free number as soon as you believe that the police claim they want to stop your meals. KING YOUR FOOD Food Not Bombs P.O. Box 424 - Arroyo Seco, NM 87514 USA 1-800-884-1136 www.foodnotbombs.net


You may organize nonviolence training for your local Food Not Bombs chapter even if you do not think you will be arrested for sharing food. Your chapter is likely to provide food at protests and may even participate in nonviolent direct action with other groups. Consider reading up on the history of nonviolent direct action. Author Gene Sharp has written many books that describe the principles that make nonviolent direct action and noncooperation with authority successful. His books review the history of nonviolent civil disobedience, from campaigns in defiance of dictators like Hitler to the United Farm Workers and the Civil Rights movement in the United States.

Food Not Bombs has often initiated nonviolent campaigns in defense of local community issues, such as supporting the rights of the homeless or indigenous people. Your Food Not Bombs chapter can prepare to take action by holding regular nonviolence trainings. As the environment and economy grows more precarious, Food Not Bombs is more frequently asked to provide logistical and material support for a wide variety of campaigns.

Your chapter might initiate campaigns like Homes Not Jails to house the homeless in your community or reclaim the public space outside a military base to call for an end to military spending. Your campaign might start by mapping out a year-long strategy, starting with simple techniques like petitions, literature tables and education to build popular support. Your strategy can systematically escalate until your campaign moves to more risky actions like occupations, sitins or blockages. Regular nonviolence trainings will help strengthen your chapter and your local activist community. 

### If The Government Arrests You

Whether or not you think you actually will be arrested, will- ingness to suffer arrest can be very powerful. Your lack of fear of arrest actually makes it less likely. If you do get arrested for sharing free food with the hungry, you can engage in additional noncooperation while in custody to continue expressing your power to not be intimidated. It is good to consider the parameters of your noncooperation before you are arrested. It is possible that being arrested for feeding the hungry will have such a strong impact on you that you will not even be able to cooperate with the State once you are jailed. The most basic form of noncooperation, once arrested in the United States, is the refusal to give your name or address. Withholding your name and address must be considered carefully as it is becoming more common for governments, including the United States, to disappear political activists, so investigate this possibility before taking this form of noncooperation

In the first decades of Food Not Bombs, refusing to give your name and address was fairly safe. Refusing to identify yourself makes it more difficult for the State to dominate you while you are in custody. Some people will not give their names until they are brought before a judge. If you refuse to identify yourself, the police will often try to intimidate you by holding you in solitary confinement, refusing you access to a lawyer, 69 denying you transportation to court and engaging in sim- ilar threats and oppressive tactics. Politely but firmly tell them you won’t give them your name; most of the time, the police will give up after one or two attempts to scare you. They will book you as Jane or John Doe and take your picture and possibly your fingerprints.

In the past, most states in the U.S. limit the amount of time they will hold you, before bringing you before a judge for a bail hearing or arraignment, to fourty-eight or seventy-two hours. During this time, they couldn’t legally prevent you from seeing your lawyer if your lawyer requested to see you. Since the passage of the Military Commissions Act of 2009, the U.S.A. Patriot Act, and The National Defense Authorization Act of 2012, some officials may believe that they have to power to hold you longer before bringing you to a judge. However, the political price may be too costly for any prosecutor when the public learns that they are denying you your rights in a case where you are accused of breaking the law by feeding the hungry.

Noncooperation during a nonviolent action can also include “going limp,” or refusing to walk with the police. The officers can use pain compliance holds or roughly throw you around when you choose this type of response. The political atmosphere is growing more violent as the power struggle over resources and social control becomes more desperate, and it is possible that the authorities could use harsh tech- niques if you refuse to walk. However, it can be very empowering to retain control over your own body.

For some, walking with the police feels too much like you agree you should be under arrest. Not walking or not giving your name are both empowering tactics, but even cooperating fully once arrested can be very powerful. You know you weren’t doing anything illegal anyway, and it’s unbelievable that you could be arrested for sharing food with the hungry. Of course, DON’T EVER TALK TO THE POLICE ABOUT THE ARREST. The police have not been reading people their Miranda rights for years and the U.S. Supreme Court ruled early in 2012 that suspects are not required to have their rights read so it is up to you to know you have the right to remain silent. They might not only use whatever you say against you in court, they may also use it against the other volunteers in your Food Not Bombs chapter.

While it can be difficult remaining respectful of the humanity of the police and jailers, it can be powerful for both yourself personally and for our movement. After years of arrests and beatings for sharing food in San Francisco, the police refused to follow the orders of their superiors. Our respect for them as people made a huge impression. Efforts to describe our volunteers as terrorists failed. As the economic, environmental and political crisis grows increasingly extreme, it will be more important than ever to maintain our dignity and to influence the police and military to rebel against their superiors.

San Francisco Police prepare to arrest Food N San Francisco Police prepare to arrest Food Not Bombs at start of 1995 gathering at UN Plaza 

Again, do not feel you have to take extreme measures of noncooperation during or after being arrested to be effective. Government officials around the world have become much more repressive since we first wrote about noncooperation in 1992, but noncooperation is still a vital way to exert pressure on a system that shouldn’t be arresting people for feeding the hungry. Call the Media Again, do not feel you have toIn thetake extreme of weeks and days before yourmeasures group plans to risk arrest can email, fax and call the media about your action. noncooperation during or afteryou being arrested to be effecChoose the people who will be your media spokespersons. Be honest about who would be best for this task. Don’t be tive. Government officials around havetheybecome pressured the to pickworld someone because are popular or have some other quality. They must be trustworthy, articmuch more repressive since we first wrote about noncoopSupport People ulate and knowledgeable.

In some cases the government may try to provide an infiltrator to be your media The role of a support is as important being willing eration in person 1992, but asnoncooperation still vital to spokesperson is and as a resulta your messageway could become to risk arrest. It is essential for each person risking arrest to very distorted or even damaging. While preparing, it is work closely with their support person. Support people make exert pressure on a system that shouldn’t arresting people also wise to make abe contact list which you keep handy on sure they do what they can to avoid arrest so they can do varthe day of the action in case there are arrests. This list ious tasks for those arrested. Such tasks include having the should include sympathetic lawyers, support people, the for feeding the hungry. name, birth date and address of those arrested.

The support people need to have the proper contact information before arrests so they can make phone calls to family, friends or employers as needed. They are also responsible for tracking those arrested through the legal system so they are not lost or mistreated; obtaining the exact charges and court dates; contacting the press with the details; managing legal support with attorneys; continued organizing in the community; and covering the tasks those arrested cannot do, such as feeding animals and watering house plants and gardens. It is possible that the detainee may need to have a phone bill paid or a child picked up from school. The support person should also know about medical needs, if any, and information about past legal cases in as far as it might impact the current arrest. Support People jail and the press. It might be helpful to direct the media to your attorneys. Getting coverage in the local press can be very instrumental in building community support.

News reports can be a valuable way to reach the public about the connection of hunger to military spending. Coverage in the media will also attract more volunteers, food donations and many other offers of support. If possible, remember the name of your contact at each media outlet and talk to the same person each time you call. Have your facts and statements ready, such as the number of people arrested, the charges, who you are and why Food Not Bombs was doing whatever it was you were arrested for doing. Remember, however, that you are not trying to con- 

The role of a support person is as important as being willing to risk arrest. It is essential for each person risking arrest to work closely with their support person. Support people make sure they do what they can to avoid arrest so they can do var- sure they do what they can to avoid arrest so they can do various tasks for those arrested. Such tasks include having the name, birth date and address of those arrested.

The support people need to have the proper contact information before arrests so they can make phone calls to family, friends or employers as needed. They are also responsible for tracking those arrested through the legal system so they are not lost or mistreated; obtaining the exact charges and court dates; contacting the press with the details; managing legal support with attorneys; continued organizing in the community; and covering the tasks those arrested cannot do, such as feeding animals and watering house plants and gardens. It is possible that the detainee may need to have a phone bill paid or a child picked up from school. The support person should also know about medical needs, if any, and information about past legal cases in as far as it might impact the current arrest.

It is best if the support person has some idea of how the arrestee plans to respond to the legal system, that is, noncooperation, jail-solidarity, bail solidarity and so on. This way, they can keep everyone informed of the progress of the arrest and be there with support when needed. It is also a good idea to leave with the support person your identification and some money, if you’re arrested, just in case you decide you want to get out. Bombs at start of 1995 gathering at UN Plaza

### all the Media
In the weeks and days before your group plans to risk arrest you can email, fax and call the media about your action. Choose the people who will be your media spokespersons. Be honest about who would be best for this task. Don’t be pressured to pick someone because they are popular or have some other quality. They must be trustworthy, articulate and knowledgeable.

In some cases the government may try to provide an infiltrator to be your media spokesperson and as a result your message could become very distorted or even damaging. While preparing, it is also wise to make a contact list which you keep handy on the day of the action in case there are arrests. This list should include sympathetic lawyers, support people, the jail and the press. It might be helpful to direct the media to your attorneys. Getting coverage in the local press can be very instrumental in building community support.

News reports can be a valuable way to reach the public about the connection of hunger to military spending. Coverage in the media will also attract more volunteers, food donations and many other offers of support. If possible, remember the name of your contact at each media outlet and talk to the same person each time you call. Have your facts and statements ready, such as the number of people arrested, the charges, who you are and why Food Not Bombs was doing whatever it was you were arrested for doing. Remember, however, that you are not trying to convince the press person about what you were doing.

Talk through the press, not to them. Just tell them what it is you want to say and end the conversation. Be polite but firm. Do not let them talk you into saying something trivial or irrelevant because they will often use this unimportant information and ignore all the good things you did say. Two important points some Food Not Bombs spokespeople stress is that food is a right and not a privilege and that sharing free meals and literature are unregulated activities. You can also point out that by redirecting our resources from the military to food, housing, education and healthcare, we could reduce hunger, homelessness and the pain of poverty. 

### Jail Solidarity
After you have been arrested, it can be very inspiring and may influence the local authorities to end their arrests if those detained engage in jail solidarity.

Jail solidarity has been very effective in encouraging the State to end their attempts to stop Food Not Bombs. It is best to discuss and plan this in advance.

You may want to investigate how harsh your local police and sheriff department tends to be to determine what risk you may be taking. When arrested, each person has one of the follow- ing choices: not giving your name (noncooperation), giving your name but refusing bail (bail solidarity), or fully cooperating by giving your name and paying bail. Bail solidarity has been very common with volunteers arrested for sharing food.

Of the over 1,000 arrests of our volunteers in San Francisco, not one was bailed out for a “food arrest.” In many cases, our volunteers were released in less than twenty-four hours. Some have been freed after processing, which can take as little as four hours. If several members of the group are willing either to not cooperate or engage in bail solidarity, then you can begin planning your jail solidarity. As a group, you can negotiate your cooperation for concessions from the jailers. For example, you can bargain for access to a phone, the press, paper and pencil or your lawyers; demand no bail money as a condition for your release (commonly called “personal recognizance”); or try to prevent the segregation of some of the participants. The jail system is not designed to respond to a group, it is designed to isolate and demoralize you.

The stronger you stand together, the sooner jailers will become exhausted and meet your demands or even let you go! Unfortunately, because of the philosophy upon which the jail system operates, the jailers are trained to purposefully be vague and inaccurate as a security measure. You never know whether what they tell you is the truth or not. This keeps you disoriented and unable to trust any of the information you receive.

Therefore, it is best not to believe anything the jailers say. Remain calm and polite, and use any dialogue with the jailers as an opportunity to explain why you believe in the actions of Food Not Bombs. During the years Food Not Bombs volunteers were in and out of the local jail, our friendly, honest interaction with the authorities helped bring the arrests to an end and built respect from areas of the community that might otherwise have never supported our goals. Highlight the ridiculous irony of arresting people who are giving away free food.

In nonviolence theory, this is called “speaking your truth to power.” The jailers are generally not your arresting officer, but what you say can be used in court. Trust yourself and remain committed to the plan the group made before the arrest.

### Courts and trials 

Every country has its legal process. Unless you are sharing food and literature in the United States you are not likely to have any problems. The pattern in the United States can provide an outline for the legal process in other countries with local variations based on historic and cultural traditions. After you are arrested, you will be arraigned, which means you will face a judge who will read out your charges.

You may see the judge in person or on closed-circuit television. Generally you will plead not guilty. After you plead you will be given a trial date or other appearance date. You may get a lawyer at the arraignment.

It is best to seek an activist lawyer before your arraignment so they can represent you. You might also choose to represent yourself, which can be very powerful and provide you with the opportunity to make much more politically powerful statements in court.

Lawyers need to worry about your being convicted and can be more cautious than your case deserves. 

If you know the date, time and location of your arraignment you can organize a rally outside the court house before your court appearance. Most Food Not Bombs groups provide food before every court appearance to stress our right to share food and literature without government interference. Because the media and other institutions take legal proceedings seriously, your arraignment, hearings and trial provides a great platform to highlight how misguided our political and economic system can be. 

After your arraignment, you may have several more steps before trial. You may have discovery, where you will ask for the documents, photos, videos and testimony of the authorities in relationship to your arrest.

You will at least get a police report from the day of your arrest, but it may be possible to get documents of meetings between city officials and the leadership of the police. You may even be able to get copies of the video that was made of their practice arrest in preparation of the real arrest and other planning documents showing that there was more to your being arrested than that you were just sharing food and literature without a permit.

Your case may include depositions where you are questioned under oath before the judge. You may want to answer the questions in as brief a manner as possible or plead the Fifth Amendment where you are making a claim that you will not testify against yourself.

Then you will have the trial. You may have to pick a jury, or sometimes your case may be before a judge. The trial will involve witnesses both for and against you. You get to cross examine the state’s witnesses and the prosecutor will be able to cross examine your witnesses. The simpler you 72 make your case the better. You can make the point that with so many people hungry, it is only reasonable to provide the public with meals. Maybe point out that sharing food is really an unregulated activity and ask where the government plans to stop in controlling the exchange of assistance.

If you learn that a military contractor or soldier was responsible for filing the complaint, you might be able to make a connection between the fact that so much money is being spent on war while people are going hungry. Your statements can be a short and powerful way to support our right to free expression and mutual aid. You may need to return for the verdict, giving you another opportunity for a rally outside the court house and, if convicted, you get yet one more chance to make a statement both outside at your protest before sentencing as well as in court during sentencing.

Only one person was convicted in over 1,000 arrests in San Francisco and we showed up outside the jail with food and literature every morning until the pressure was too much for the legal system and they freed Robert Norse Khan.

You are very unlikely to go through all these steps, but it is good to know what could be ahead if the state tries to stop your group from sharing food and literature. By the time you would be sentenced, your Food Not Bombs chapter will have so much support your group will be sharing meals seven days a week and people in every city near you will have started their own Food Not Bombs chapter in defiance of your trial. Your group will have access to media and the impact will far outweigh the harm done by attempting to prosecute you for your participation in Food Not Bombs. 

### False Allegations of Terrorism
One sign that Food Not Bombs is an effective model of organizing for change is all the effort the United States government has taken to disrupt our movement. Federal authorities wouldn’t waste millions of dollars trying to stop us if they weren’t worried that we might inspire resistance to their policies by the simple sharing of vegan food and difficult to access information under the banner Food Not Bombs. Food Not Bombs volunteers can face other legal issues not directly related to sharing food.

In an attempt to frighten away supporters, our volunteers have been accused of plotting acts of terrorism or arson. Even though our name is Food Not Bombs, and we are dedicated to nonviolent direct action and sharing vegan and vegetarian food; the police, federal agencies and military intelligence units in the United States have created elaborate plans to entrap volunteers and accuse them of being “terrorists.” For the most part their plans have failed. Some of our volunteers have been arrested on terrorism charges, and on occasion, our cooks have been tried, convicted and sentenced years in prison.

While it is hard to believe that the gentle nurturing tasks required of Food Not Bombs activists could be considered terrorist threats, this seems to be the case. Internal government memos and statements of military contractors indicate that they are worried that we may influence American tax payers to start supporting policies that redirect military spending towards education, healthcare and other domestic necessities, costing billions in weapons contracts. Providing food and logistical support at nonviolent protests could be another reason why U.S. officials assert we are a terrorist threat. Food Not Bombs has fed logging protesters at Red Wood Summer in Northern California and provided food to the thousands of people participating in Cindy Sheehan’s Camp Casey outside George W. Bush’s summer home in Crawford, Texas.

Our meals supported many peace, environmental and anti-globalization actions which, unfortunately, could be labeled as “acts of terrorism. ” Food Not Bombs activists in the United States can take some simple steps to protect themselves from being arrested and charged for planning or participating in acts of terrorism.

The fact that we are not considering acts of terrorism can cause our volunteers to make light of statements about arson, bombings and rock throwing, but the FBI and Homeland Security have sent infiltrators to our meetings to talk about using violence. Months later, these statements can appear as evidence that our volunteers were plotting acts of terrorism. When the cases get to court, the prosecutors and media can point out that the accused Food Not Bombs activists didn’t object to the comments made by the informants, “proving” that our volunteers were guilty.

Food Not Bombs volunteers have been charged as terrorists after traveling with several infiltrators that were paid by the federal government to burn down research laboratories, lumber mills, model homes or auto dealerships. Sometimes federal prosecutors were able to get convictions because the Food Not Bombs activists were intimidated from expressing their dedication to nonviolence, fearing that they would be accused of being “weak” and not serious about social change, the wellbeing of animals or real concern for the environment.

The first step is to make it clear that you are not going to participate in acts of violence. If people are joking about using violence or talking about the virtues of acts that could injure or kill people, it is wise to make several statements that Food Not Bombs will not engage in any kind of violent activity.

Point out that Food Not Bombs is dedicated to nonviolence and that anyone considering any other strategies or methods should meet elsewhere. To help protect your friends you might also point out that it is very unlikely that such plans could be concealed from the government. Another step your chapter can take is to include statements about nonviolence on your literature about any direct action you might be planning or supporting.

On occasion, the media and prosecutors will claim that our literature didn’t make any mention that our protest would be nonviolent and use that as “proof” we are terrorists. If your chapter is planning an action, you can protect yourself by including text about nonviolence on your publications.

This can be difficult when working in coalition with groups that might not share our principles of nonviolence, but you could publish Food Not Bombs’ own literature on the action. It isn’t necessary to exclude reference to nonviolent direct action just because people are arguing in support of a diversity of tactics.

Nonviolent resistance is just as valid as other methods. Nonviolent direct action, noncooperation and nonviolent resistance can be very empowering. It takes courage to organize and participate in campaigns of nonviolent struggle. Many government officials have expressed frustration and were forced to capitulate when confronted by a dignified campaign of nonviolence. Nonviolent struggle can build trust between participants and the public.

Campaigns of nonviolent direct action and civil disobedience can be so effective that governments and corporations will try anything to influence our movement into adopting the use of violence. That is one reason Food Not Bombs has been the focus of infiltration and why the authorities rely on agent provocateurs to reduce the impact of nonviolence while sowing fear and alienation. 73 Don’t let people intimidate you into silence.

People can make comments about pacifists being “wimps” or “pussies;” that nonviolence never works; or that you are not really committed to change if you aren’t willing to use rocks, bombs or guns. You might even hear that nonviolence is racist because people of color “have to take up arms,” and that white, first-world people have the luxury to use nonviolence.

Infiltrators or government agents may be talking to some of your volunteers outside Food Not Bombs at places like cafes, clubs or other public locations, seeking to introduce the idea that armed resistance or arson is the only solution.

The state may also promote the idea that you are not serious about peace, animal rights, social justice or the environment if you are not willing to take violent action. Honest discussion of all tactics and methods, including types of violence, is fine, but make it clear that Food Not Bombs is dedicated to nonviolence.

At the same time, it is not wise to make claims of infiltration or accuse someone of being an informant. It is best to not worry about infiltration and to stay focused on the work of Food fine, but make it clear that Food Not Bombs is dedicated to nonviolence.

Just take the simple precautions asking wise to make claims of infiltration of or accuse someone of being an informant. It is best to not worry about that all discussions of violent tactics be some place infiltration and to stay focused on the work of Food Not Bombs.

Just take the simple precautions of asking other than at public meetings, making clear that all discussions ofit violent tactics you be someare place than at public meetings, making it clear you are dedicated to nonviolence, other adding that fact tofactyour dedicated to nonviolence, adding that to your direct action and civil disobedience can be so effective that governments and corporations will try anything to influence our movement into adopting the use of violence.

That is one reason Food Not Bombs has been the focus of infiltration and why the authorities rely on agent provocateurs to reduce the impact of nonviolence while sowing fear and alienation. The FBI raid the Minneapolis Food Not Bombs house before the Republican National Convention in 2008 74 publications and organizing nonviolence trainings.

If you do this, attempts to convict you on terrorism charges will likely fail, and the fear and mistrust that so often destroys movements will be defused. The government can use the fear of infiltration as a way of destroying trust in your community. Food Not Bombs activists can take some very simple actions to make sure they do not fall prey to the U.S. government’s efforts to disrupt our work.

First, stay focused on the fundamentals of Food Not Bombs. Don’t feel guilty about refusing to take violent action. Since the world is facing so many dire crises it might seem rational to consider sabotage, arson or other acts considered violent by the corporate state, but these tactics can cause the public to withdraw any support they may have for our cause. The use of violence also breeds distrust among activists.

As we have learned from people like Edward Snowden, it is nearly impossible to have any secrets in the United States. According to the Washington Post, over eighty billion dollars is spent each year to spy on the enemies of corporate America. A campaign of violence would add to the disempowerment in our community and scare the public into greater support of the authorities. If you feel you must investigate tactics that include taking violent action, consider the entire strategy and how these actions would motivate change.

Are you really ready to live fearing capture? How will you feel if your friends spend their life in prison while you and your friends are portrayed as crazy? Will your actions really inspire the public to rise up and save the earth? How will you feel if you kill someone or if one of your friends is killed? Can you really see yourself coordinating hundreds of people in campaigns of bombings? Are you really able to organize a group of trusted friends to wage a campaign of shooting police or politicians and how will this move society towards addressing the crisis? How will you feel spending the rest of your life in prison, seeing the stress this is putting on your family and friends? While it is also possible you could spend decades in prison for taking nonviolent direct action, you are likely to feel more empowered and have wider support on the outside. Unlike those people who are doing life in prison for a bombing or shooting, if you are sentenced to a long prison term for organizing or participating in a campaign of nonviolent direct action and noncooperation, you have a much greater chance of inspiring popular support and possibly achieving the desired results of your effort, leaving prison before your sentence is up.

In addition, mass nonviolent direct action based on a thoughtful strategy is more likely to be effective. Agent provocateurs can encourage drastic measures, knowing we are knowledgeable about the threats to the environment and economy.

If pressured, you can remind your friends that many of the over twenty Food Not Bombs volunteers in prison were framed for similar acts and that we are dedicated to nonviolent direct action. Suggest your community study the history of nonviolent direct action in books by people like Gene Sharp, Martin Luther King Jr. and others who witnessed first-hand the power of noncooperation and nonviolence.

Another thing to be concerned about is jokes about using violence. These jokes became the evidence used for the arrests of the RNC 8 and many other Food Not Bombs volunteers. If people joke about armed revolution, bombings, rock throwing or other acts of violence at your meetings or while cooking, make it clear that Food Not Bombs is dedicated to nonviolent direct action and ask them to stop. You might remind your chapter that conversations and jokes about using violence have resulted in Food Not Bombs volunteers being framed and sentenced to long prison sentences.

The volunteers that are joking about violence or making statements about the need to use violence are not necessarily infiltrators or police agents, so don’t make any accusations. They may have been influenced by someone they met outside of Food Not Bombs or have read some of the many books promoting the belief that nonviolent direct action doesn’t work. It is best to not worry and stay focused on the work of Food Not Bombs.

The government can use the fear of infiltration as a way of destroying trust in your community. Again, simply remind your chapter that we are dedicated to nonviolent direct action and that we don’t joke or talk about taking violent action while volunteering with Food Not Bombs.

While armed resistance has worked to overthrow governments and change the power structure of some countries, in most cases, the system that resulted from use of violence had to rely on the continued use of violence to retain its authority. For long-term positive change based on democracy and public participation of everyone, nonviolent social change offers the greatest hope. 

## BASIC STEPS BASIC STEPS TO TO EFFECTIVE COMMUNITY ORGANIZING
You can make a banner that hangs over the front of your NTS You can a banner hangs over the frontand of other your table.make You may want tothat table at concerts, lectures table.events, You may wantastoalways table at concerts, lectures and other as well setting up your literature at all events, as well as always setting up your literature at all your regular meals. If you print a stack of at least 100 flyers, you can place a number “19” rubber band around the stack before setting the flyers out on your table.

Number “19” rubber bands have the best tension and are best bought at local stationary stores as the ones from chain stores break with each use. If your volunteers maintain a neat and orderly looking presentation, the public will find out what your group is doing with ease and are more likely to consider your group organized and worth supporting.

You can design your own flyers, download flyers from a Food Not Bombs website or collect literature from other organizations in your community. You can also request stacks of literature from many organizations by visiting their websites, giving them a call or sending them an email. They will be excited that you will be reaching hundreds of people with their message.

The volunteers in your chapter can ask each person passing the table a question related to your present message; ask if they heard about the next event, or if they would like to participate with your group. Even ask their opinion of an issue currently in the news. Your literature table can also include a plate of cookies or other baked goods. Offering cups of hot cider or refreshing sun tea is another way to make your literature table invit- ing.

You may want to include a volunteer sign-up sheet and attract visitors by handing each pedestrian a quarter size flyer about your meals. We have an example of these flyers in the appendix.

## ORGANIZING LITERATURE TABLES, EVENTS MEETINGS, TOURS AND GATHERINGS
It is possible to have an impact on society if you take the time and ask others to help. Often, the simplest activities, organized in a consistent, regular manner; can be the most effective way to encourage political, economic and social change.

A regular literature table can attract volunteers, support for your group’s actions and donations. The people inspired by talking with you at the table can become participants in your well-organized, regular meetings and can help make your group’s meals, events, tours, protests and gatherings more effective.

One main reason Food Not Bombs is a global movement and is as effective as we are is because we staffed so many literature tables in locations where we were able to reach people who had no idea people who shared our ideas existed.

Our literature tables have been the foundation directing people to our meetings, events, tours and gatherings. How to have a successful literature table It almost seems too simple, but a consistent Food Not Bombs literature table is one of the most effective ways to inspire social change. The location and timing can be very important.

Your group can reach the public with information about Food Not Bombs, volunteer opportunities, current issues and community projects by setting up in a high visibility location at a time with the most traffic so your volunteers have an opportunity to meet and talk with as many people as possible.

You may choose a location where you can also hang your banner near your table. 76 One reason you can tell the Food Not Bombs literature table is effective is the effort the police use to try and stop our volunteers from setting up our tables. The banner and literature is often the first things the police take when attempting to stop our meals.

The authorities have spread rumors that the literature and banner aren’t important, claiming that the food is really all that the volunteers should focus on. Food Not Bombs activists in the United States have also reported cases where the police say we can share meals but are not allowed to distribute literature or display our banner.

However, Food Not Bombs is not a charity and is working to change the po- Not Bombs is not a charity and is working to change the political, economic and social systems that perpetuate hunger and poverty. Our right to distribute literature is even protected in the United States by the First Amendment to the Constitution.

If you include your chapter’s contact information on your flyers, you will provide an easy way for people passing by to reach your group in the future. Your literature will also help your group feed more people in the short run by inspiring more volunteers, food donations and popular support.

Don’t let anyone discourage you from always having literature and a banner at your meals. Even if we were not trying to create a culture where no one would be hungry, the literature with your chapter’s contact information is essential to maintaining interest in your meals, attracting more volunteers and food donations. Food Not Bombs groups that don’t bring literature and a banner to their meals often struggle to recruit help, but those groups that do have literature and a banner are more vibrant and find the flyers increase interest in their chapters.

Packing the literature box Your chapter can have a box of literature always ready to take to your meals or to use at concerts, protests or other events. If your group has a well packed literature box always on the ready, you will never arrive at your meal with- out it. Your group can keep your literature neat and inviting by packing the largest publications on the bottom of the literature box, then stacking the next largest literature and putting the smaller stacks on top. If your volunteers place business cards on the bottom of the box and add the larger stacks on top the literature will be bent or torn and may be too mangled to feel good about handing out your materials to the public. Wrap one or two number “19” rubber bands around each stack.

Rocks don’t work as well. When a rock is lifted off a stack of flyers, the wind can blow some of the papers off the table. The authorities might also accuse you of intending to use the rocks as weapons. A stack of 100 wrapped in one or two rubber bands will be heavy enough to keep the wind from blowing your flyers off the table. A stack of 100 will also encourage people to take a copy. At the end of each meal or literature table, place the literature back into your box, large flyers first with each smaller size on top, with the banner folded on top.

Then, the next time you need to table or when you are going to share your regular meal, your literature will be in good condition and it will just be a matter of adding your literature box to the items you are taking out to the meal. Place the largest flyers and posters flat on the bottom and stack the next smaller flyers flat on top of one another. People will not take flyers that are bent and look like trash. 

## HOW TO ORGANIZE A MEETING
Organizing a meeting for any purpose, from starting your local Food Not Bombs group to planning a protest, concert or political campaign, could be the most difficult, yet most important and useful skill described here. Meetings might not seem fun or important, but well-organized, regular meetings provide access to everyone interested in the direction of your group.

They also contribute to distributing responsibility and tasks to more volunteers and can help reduce burnout. Meetings also provide a forum where new ideas, projects and innovations can be proposed, formulated and implemented. Meeting while cooking or sharing food is rarely as productive as when everyone’s attention is focused.

Using the process of consensus will also inspire the most from everyone participating at your meetings. 

### Meeting Step 1: Time, date and location
Ask the venues you would be interested in using if there is a time and day that would be best for your meeting. Then ask the people you want to work with if they can meet at those given times, dates and locations. You may need to provide a couple of options. 

The size and type of location will depend on how many people and what you will be doing at the meeting. Your meeting may include an activity like sign painting or one that requires a special location. The less distraction and the more central the location, the better. 

The most common venues include cafes, bookstores, libraries, student unions or classrooms. Since you may be working with people that live outside, you may find it best to meet in a park or plaza. You may choose a time that is after people get off work, but before the homeless need to return to their campsites or shelters. 

It would be best not to choose a time and day when another related organization is holding their regular meeting. Sometimes you may need to adjust the day or time because of an event that would draw away interest.

### Meeting Step 2: Sample agenda

You can have an agenda planning committee and request agenda You can have an agenda planning committee and request agenda items from everyone you are inviting. Give them a completed copy of the agenda a day or two before the meeting.

This one step can make the conversation and implementation of each agenda item very successful and effective. You can accomplish a great deal by sticking to the agenda and the times. Introduce an agenda item at one meeting and make the decision about that agenda item at one of the next meetings. Some agenda items may be discussed for many meetings before coming to the best decision. Rotate the tasks of facilitator, note keeper, time keeper and, if very organized, door greeters, vibe watchers and the many other possible roles one could fill at a meeting.

Many groups fail to keep notes and this can cause confusion in future meetings. Your chapter may want to return the notes after each meeting to the same volunteer. If that volunteer moves or leaves the group, ask them to pass the notes on to another reliable volunteer.

### Meeting Step 3: Consensus Process
Food Not Bombs started using the process of consensus to make decisions at its very first meeting. Using the consensus process to make decisions has made it possible for people to organize local Food Not Bombs chapters without relying on a headquarters, directors or a leader to start and maintain a local group. Each decision is consented to by all the participants. To arrive at consensus, each proposal is made with the idea that it will accurately reflect the goals and interests of the group, trusting that it will evolve and change as everyone adds their input. It may take several meetings to discuss the proposal before the ideas have come together and the group arrives at consensus. By using this process, the participants are more likely to be committed to implementing each proposal.

In contrast to “Robert’s Rules of Order,” which is used by most groups, there are no winners and losers, and there is not an effort to win the most votes. Instead, the goal is to make the decision that is best for everyone participating. Many groups start their meetings with the facilitator asking for everyone to introduce themselves and then asking for someone to give a brief description of the consensus process. Everyone is invited to participate fully in every meeting. Everyone is free to introduce agenda items and speak to that item. Everyone is also free to speak to every proposal.

The facilitator will introduce the agenda item, asking those making a proposal to explain the details. Then the facilitator will ask for comments and open the floor to each person interested in speaking. Once everyone has spoken or when the time for that agenda item has been used up the facilitator can ask for five, ten, or fifteen more minutes to continue to discuss the agenda item or suggest the group move to the next item.

When the time is up, the facilitator will ask for the proposal to be restated and then ask the group if anyone feels they need to “stand aside” because they can’t support the proposal, but won’t block because their opposition is not based on the proposal being contrary to the values and goals of the group.

If anyone feels the proposal is contrary to the values and goals of the group, they can block the proposal. The facilitator can ask the people blocking what would need to change to make it so they would be willing to lift their block. That agenda item should be placed on the next agenda and a committee might be organized to re-work the proposal. It is possible the block cannot be lifted and the group cannot come to consensus.

Most proposals are never blocked, but it can become necessary to obtain more information and return to the subject at the next meeting. Honoring the time dedicated to each agenda item shows respect for everyone in the group. Some proposals might remain on the agenda for meeting after meeting. Because the process honors everyone’s opinion and time the decisions are likely to be implemented effectively. It can be very helpful to rotate facilitators each meeting to reduce the possibility of any one volunteer feeling that they are being seen as the leader.

The more all volunteers participate in making the decisions, the more dedicated everyone will likely be implementing decisions.

Food Not Bombs groups can organize their own workshops to study the use of consensus in the effort to nurture everyone’s skills at participating. Spokes Councils It might be necessary to seek support and coordination among a number of Food Not Bombs groups or a coalition of grassroots organizations. Emerging global economic, political and environmental emergencies might require considerable inter-group coordination.

The chapter making the proposal can invite the other groups to send two or more representatives from each chapter to a “spokes council” meeting to review the proposal and seek consensus and send that proposal back to each chapter to address at their next meeting. Then the local chapters can adopt the proposal or send an adjusted proposal back to the next “spokes council” meeting. If the spokes council meeting comes to consensus, it is sent back to the local chapters to adopt and implement.

One way to organize a “spokes council” meeting is to announce that your chapter would like to discuss an idea involving more than one chapter and suggest that the subject be included at the next Food Not Bombs Gathering. It has been suggested that Food Not Bombs organize regular regional and global gatherings with a “spokes council” meeting as a regular feature to help coordinate inter-group actions and communication. With so many Food Not Bombs groups active in every area of the world, inter-group coordination might be very effective at influencing social change.

The wave of uprisings in early 2011 show just how important it is to use consensus and the need to develop a strong culture of inclusive decision-making. As oppressive sys- tems fall under popular pressure, your movement can fill the resulting power vacuum with an already well— established democratic community based structure. HOW TO ORGANIZE AN EVENT Planning events could be one of the most often used and important skills you will use as a Food Not Bombs organizer. Your chapter could have monthly, weekly or even daily events and host events on a random schedule. If your group implements every step, your events will be well attended and have the impact your community desires.

### Event Step 1: Venue
Start by contacting all possible venues that might be appropriate to the type of event you plan to have. That may be a cafe, concert hall, a room in the public library, a loft, a squat or outside in a park, plaza, in front of a corporate or government office, or other locations. Agree to a date and time for both the event and preparation.

Depending on your expectations of the event, you might want to set the date and location as far in advance as possible. Large events such as gatherings or festivals can be announced more than a year before to give you enough time to plan and a year before to give you enough time to plan and promote the event. It is best to always allow six or eight weeks from the time your group confirms the date, basic talent and venue so your announcements can be listed in monthly publications.

If there is a particularly important publication to be listed in, you may want to base the date of your event on the deadline of that publication so you can reach as many people as possible that might be interested. Several of your volunteers could visit the venue before agreeing to use it to make sure it will work for your plans. You may need to bring your talent as well. A circus or performance using fire may need a larger space or require special arrangements with the fire department. Find out the details about rent of the venue, including when the deposit is required. 

It may have a contract or require insurance. Food Not Bombs can often have rents and other costs reduced or donated. Ask the manager if they have the equipment required; or if you will need to provide things like video projectors, lights or sound systems. Write down the manager’s name, phone number and email address, and send them an email confirming the dates and times. Remind the manager of the event two weeks and a couple of days before the event.

### Event Step 2: Talent
The talent could be bands, artists, athletes, healers, magicians, a speaker, poets, DJ, films, PowerPoint presentation, skill sharing, singing, drum circle, puppeteers, dancers or dancing, game facilitator or any combination of people, animals, objects or media. You may want to invite someone to film or record the event.

Your talent may require sound or lighting equipment. Send the talent and other participants, such as sound companies, an email to confirm the date, time and location of the event. Also confirm details such as how much they will be paid, what equipment they may need and when you expect them to arrive to set up.

Remind all the talent about the event one week and the day before the event. You may want to give them a call the morning of the event if you believe that would be helpful. Give copies of your posters and flyers to your talent. Invite them to schedule interviews with the media. You may join them at radio programs so you can talk about Food Not Bombs and the talent can talk about their performance.

If they have a song that can be played on air live or as a recording, that can make the interview even more interesting. Event Step 3: Promotion As soon as you have the date, time, location and talent, it is time to draft a Public Service Announcement (PSA). Keep the PSA short enough to be read on the radio in 30 seconds and no more than 60 seconds. Read it out loud and time it. The text of your PSA can become the text of your flyers, emails and announcements on websites. Send an email of this PSA to the local media and your group’s contact list.

Ask a volunteer to design a flyer and post copies announcing the event all over your community. Your volunteers can start posting flyers as soon as your 79 group knows the details and can make a poster.

You can make small quarter-size or half-size flyers to hand out at concerts or other events. Make sure you have posters in as many store windows and on as many bulletin boards as possible. Your volunteers may need to return to every location a week before the event to make sure there is a flyer at each place.

You can also have a stack of flyers on your table at every meal.

You may be able to set up a literature table at other events or at a high visibility location a few days before the event.

Call your local media and make sure the event is listed. Call your local radio stations and ask them if they might like to interview you and the talent on some of their programs. There may be music or feature writers at your local paper that would be interested in interviewing someone from your group or some of the talent.

You might call into local talk shows and tell them a bit about Food Not Bombs and mention that the listeners can find out more at your next event and share the date, time and location on air. 

Colleges and local government offices might have electronic bulletin boards where you can announce your event. Venues might have a space where they announce upcoming events, and some communities have a place where your group is allowed to hang a banner announcing the event.

Your volunteers can visit each media office and hand a copy of the flyer and Public Service Announcement directly to the staff. Your volunteers could also visit the offices of other community groups and invite them to set up a literature table at the event and ask them to post a flyer or include an announcement of the event to their email list. When you email the announcement to your group’s listserv, you can end the post with a request to forward it to their lists.

You can ask the venue and talent to email their lists and add the event to their websites. Your volunteers can also ask to announce the event at other groups’ meetings, during concerts or at rallies and protests.

You may want to send out your Public Service Announcement several times starting as soon as you have the date, venue and talent, a month before the event, two weeks before and during the week of the event.

Always call the media to make sure they have your PSA in hand. Media outlets can get over a hundred PSAs each day and unless the employees are directed to yours it may be lost in the chaos. If you are friendly, they will make a point of putting your PSA at the top of the pile.

This can be a great way to alert your community, not only about the event you are announcing, but it will let the public know there is an active Food Not Bombs group in your community. 80 This is the format most organizations follow for their Public Service Announcements. 


### Event Step 4: The event
Once you have the date, time, location and the talent, it is a good idea to have the contact information of everyone involved including phone numbers and email addresses. Someone in your group can call the venue and talent a week before the event and the day before, making sure they all remember the details. You might want to confirm the time of when you plan to arrive to set up. Your chapter may organize committees to implement the various tasks or simply have “bottom liners” responsible for each task.

Your group can have two or three people responsible for setting up and staffing a literature table. You will also need volunteers to collect the tickets or donations at the door. Your chapter might want to hand each guest a flyer about your meals and your group’s contact information. The volunteers at the literature table can ask each visitor to fill out their name, phone number and email address on your sign-up sheet.

They can also provide each guest with a flyer inviting them to your chapter’s next planning meeting or protest. Hang your largest Food Not Bombs banner in the back of the stage above the heads of the performers. You may want a volunteer to help with stage managing who meets the talent, provides them with the schedule of times and is responsible for making sure each performer is ready to perform and who makes sure the groups get on stage and off stage on time.

That person could also be the “Master of Ceremonies” (MC) or that might be yet another person so the stage manager is free to find lost talent. There may be stage hands helping with the logistics of equipment and props. Your group might want to provide food at the event and will need volunteers to collect, cook and share the food during the event.

This might be a full meal or just desserts and drinks. Your chapter can schedule extra food pick-ups, as well as an additional time and location for cooking in advance of the event. Make arrangements with the management of the venue to make sure it is pos- sible for your volunteers to arrive early to set up the literature table, food, props and banners. As the event comes to the end, your group can start to clean up the venue, picking up trash and collecting the recycling.

As soon as everyone leaves, be prepared to sweep and possibly mop the floor of the venue. Pack up all the equipment and help load the items in the appropriate vehicles. Pack up your literature box with the large flyers on the bottom and smallest items on the top.

Don’t forget your banner. You could design and print out an event planning check list to make sure each task is assigned and completed, and you could keep that list with the notes of your meetings and other reports, flyers and news clippings so new volunteers are able to see what you have done so they don’t have to reinvent the process and can locate contact information for future events. Plan to have a critique of your event at your next meeting.

## HOW TO ORGANIZE A TOUR

Tours are a great way to build the Food Not Bombs movement and encourage support for protests or campaigns. If you are in a band, theater group, circus, or member of a puppet show you could organize a Food Not Bombs tour featuring your performance and encourage more people to participate as volunteers or participate in a future gathering or protest.

You could invite other bands to join the tour and invite local Food Not Bombs chapters to provide food and their local literature at the event. They can also promote your tour’s visit, helping schedule radio and newspaper interviews, posting flyers and announcing your arrival at other concerts and events.

These are the main steps to organizing a tour.

### Tour Step 1

Create a theme When you consider organizing a tour, it is helpful to consider what message you hope to share and the goals you intend to accomplish. Some tours are organized to build interest in a future campaign or mass action. Other tours are organized to encourage interest in an organization or project.

You may have published a book or magazine and want to organize a tour to encourage the public to read your publication. Choose the message you intend to get across during each presentation. Include an outline of the presentation with a pattern of who will do what and for how long. Your outline might include an introduction, a first speaker or act, and other types of media like a DVD or PowerPoint presentation, music, puppets, banners, solar oven or other props.

The elements of each presentation can be organized to motivate your audience to take whatever action you intend. Consider choosing a title that is memorable, catchy and describes the intention of the tour.

In addition to a title with impact, you might want to include a subtitle, descriptive paragraph and troupe name that is clear and supports the intention of the tour. These elements can be used in all your materials from your initial letters to host organizations and venues, to your Public Service Announcements, flyers and website. This part of the process of organizing your tour is fun and creative.

### Tour Step 2
Propose a schedule and route Before contacting groups with your proposal, consider mapping out your route, being sure to give yourself the time to travel from one venue to another. You might propose a tour of one month and plan to perform every other evening.

This would make it possible to have fourteen or fifteen events in fourteen or fifteen different communities. You may want to have more than one event in some of the communities and stay several days. You may plan to arrive in each city before noon the day of your event so you can staff a literature table or speak with the media to promote the event.

Consider these possibilities in the proposed route and write out an itin- erary with the date and city. Traveling from one community to another in order of their location is the most efficient way to plan a tour. You may be planning to drive, ride bikes, take trains, busses, ferries, walk, sail, ride horses or, in some cases, fly, so the choice of dates will depend on your mode of transportation. 

### Tour Step 3
Send proposal to possible hosting groups Email and/or mail the proposed title, theme and elements of the event, with the proposed dates of when you intend to hold the event in their community, to Food Not Bombs groups, other organizations or the management of venues to see if they would be interested in hosting your presentation or performance.

You could provide a list of all cities and dates to everyone you contact so they can have an idea of your route and, therefore, understand why that particular date works best for their community. Include a questionnaire with questions about what they would be able to do if they wanted to host the tour in their community.

Ask if that date would work and if it would be the type of event they would be interested in hosting. You could ask if the date listed was free or hosting. You could ask if the date listed was free or would another date be better. Your questionnaire could also ask if they would they be willing to post flyers around the community to advertise the tour, make arrangements for a venue, contact local media and help with other details required for the event.

You could ask if they would have a place for you to sleep, take a shower or park your vehicle. Suggest some of the ways hosting your event could benefit their organization, venue or community. Ask that they respond by a certain date, and let them know if they choose to host the event that you will provide them with a letter of confirmation, publicity and the other materials necessary for a successful tour.

### Tour Step 4

Confirm dates and venues Ask each Food Not Bombs group, hosting organization or venue to provide you with an email or letter stating that they are willing to host the presentation on the agreed date. Try to get these letters of agreement as soon as you can so you can start to publish the schedule in emails, posters and on your website.

Request the starting time, complete name, address and contact information for the venue so it can be correctly listed on the publicity. Venues may have a website that you can also list in your promotional material.

### Tour Step 5

Promotion of the tour Start by writing a Public Service Announcement. (See the sample above under planning events.) The text of your PSA can be used for your poster, emails and website. Create a contact list of local community groups and media in each city or town you have scheduled to hold your event.

You can post a PDF of your publicity on the tour’s website and include the poster as an attachment in your emails and ask the recipients to print out copies and post them in their community. Your local host can also email their listservs, post the information on their 82 websites, and post flyers around their community. They could email, call and visit their local media, posting flyers in their lobbies.

Ask them to arrange radio and newspaper interviews.

### Tour Step 6 
Logistics In the weeks before you head out on the tour, practice the presentation and collect and pack the props, literature, equipment and other materials you will need on the tour.

It is often helpful to print up business cards and bring copies of sign-up lists so you can collect the names, phone numbers and email addresses of those attending the event. Pack the appropriate amount and type of clothing. You may need to organize some benefit events or write grants to help with the costs. Estimate the cost of transportation, food, phones, materials and equipment you will need to complete the tour.

You will need maps of the area you plan to be traveling through. Create a written list of dates, addresses, and contact people with their phone numbers and emails. Confirm that you will have a place to sleep, a shower or bath, and other details before heading out on the tour. Your host may provide food but you will also need to provide for yourself at times. If traveling out of your country, make sure you have applied for proper visas and that your passport is current.

Determine if it will be necessary to have vaccinations or if you’ll need to bring mosquito netting, heavy clothing or other special items. Make sure you have enough medicine or other items that may be difficult to obtain while on tour. You may need special adapters for electricity or need to have translators and literature in the local languages of the places you intend to travel.

Call or email each host a week before you are scheduled to arrive and the day before your event to let them know your progress. Arrange the time and location of where you will meet your host once you arrive in their community. Consider bringing small gifts for each host and those who provide housing or other support.

### Tour Step 7

After the Tour Send thank you cards to everyone that helped with the tour. Take all items such as posters, photos and other artifacts you might have collected on the tour and create a scrap book. You may want to write an account of the tour or organize an event to report back to your community. You may need to record logistics or plans created during the tour. Make contact with those that signed the contact list.

### HOW TO ORGANIZE A GATHERING

### Gathering Step 1. Propose the gathering at your group’s meeting

Gatherings can be valuable in many ways. It is inspiring to meet activist volunteers from other communities and discover their clever solutions to issues common to many Food Not Bombs groups. It can be a chance to organize inter-group actions and share skills. The principles and projects common to many Food Not Bombs groups were initiated at gatherings.

Food Not Bombs has organized regional, national and world gatherings which have been important in strengthening our movement. The Gathering will be most effective if the last day or afternoon is devoted to a gathering wide discussion where everyone can participate

One idea is to organize a regular annual world gathering with regional gatherings planned for six months or so before. Inter-group coordination of a movement as large and global as Food Not Bombs could inspire positive social change in a way that other groups are not able to achieve. (We have a video of the 1995 International Food Not Bombs Gathering on the Food Not Bombs website at www. foodnotbombs. net/videos. html.)

Include the proposal for a gathering at your chapter’s next meeting and talk about the reason and theme of the gathering, dates, and other details you might want to propose to the other Food Not Bombs chapters. Include the geographic scope of the gathering in your proposal. Consider workshops, meetings, and the focus of the gathering that would be of most benefit to the chapters you plan to invite. Your gathering might be held during the days or weeks preceding another large protest or event for which Food Not Bombs would be providing meals.

Your group might consider choosing a date that is a year or more in the future for world or international gatherings and you need almost that much time for regional gatherings. The more time everyone has to prepare, the more everyone will get from participating.

### Gathering Step 2. Contact other groups to see if they would support the proposal
Once your chapter has come to consensus on the focus, theme, dates and location of the proposed gathering, your group can ask the other Food Not Bombs chapters if they would be interested in participating.

Your chapter could produce a questionnaire to email to the other chapters. Your group might also include these questions on a website detailing the proposed theme, dates and location. Your chapter might ask if the other groups would support the proposed gathering and, if so, what workshops, subjects, entertainment and additions the other chapters might suggest.

The questionnaire might ask about the participants’ need for housing, transportation or other details. Ask that the other chapters respond by a certain date.

### Gathering Step 3: Developing the structure and schedule of the gathering
As your group starts to receive responses to the proposed gathering, your chapter can organize committee meetings. Your group could have finance and fundraising, program, venue, housing, food, transportation, documentation, healthcare, childcare and outreach committees.

At first, most of your volunteers may be on almost every committee, but it won’t be long before your chapter starts getting more volunteers. News of the gathering will inspire additional interest in your chapter. The finance committee can develop a budget and start organizing benefit concerts and other events. The program committee can contact the other chapters to make sure they return the questionnaires. 

They can start to outline the programs by first making a framework for the daily schedule. The venue committee can secure a place to have the gathering. This may be one large facility like a school, place of worship, community center or a collection of spaces. The housing committee could write a letter to the community requesting housing for certain nights and make a list of all the possible places offered.

This list could include the name of those offering the place to stay, their address, phone number, email address and the number of people they can accommodate. The transportation committee might take the information from the questionnaire about the transportation needs of people. If it is a world or international gathering, they may need to notify the participants that they will need to provide information to assist in the application of visas.

The food committee can draft a letter requesting special food donations and create a contact list of all the possible food sources. They might seek a large kitchen located at or near the venue. 

### Gathering Step 4: Outreach

The goal of outreach for a gathering is to attract as much interest from other Food Not Bombs groups as possible. The more input the participating Food Not Bombs chapters have in forming the focus and agenda of the gathering, the more support your gathering will have. After your group emails and posts the proposal for the gathering, then you can call each chapter to remind them to discuss the proposal and ask them to return the completed questionnaire by the deadline.

Your chapter can email an announcement of each planning meeting with its agenda to all participating chapters and invite them to respond in person or by email. Remind the other chapters that they are free to make any proposal. 

It is possible to organize regional meetings to share ideas which can be proposed to the host chapter. A gathering website can be posted with updates on housing, the programs, transportation, healthcare, childcare and other needs.

Ask a volunteer to design a poster announcing the gathering, its dates, location, focus, workshops, entertainment and contact information. You can email a PDF to each chapter so they can include copies on the literature table and post it at other activist offices. You may use the image from the poster on t-shirts and other items.

### Gathering Step 5: Creating the program
The program committee schedules workshops, discussions and the agenda for the “meeting of the whole.” Your chapter could propose a discussion around an urgent issue where inter-chapter cooperation would be important or you could propose a theme you think would help other groups.

More attention to developing a well researched strategy for the future could be discussed. The committee can start to outline the programs by first making a framework for the daily schedule. For instance, opening the morning with yoga or meditation, then breakfast, a morning meeting of the whole, a morning workshop, lunch, an afternoon workshop, break-out meetings, dinner and an evening event.

The break-out meetings might be used to develop proposals for agenda items at a final afternoon plenary or meet- ing of the whole where the gathering could try to come to consensus on the details of each proposal that would then be sent back to all the chapters for discussion and decisions. The workshops could include some proposals directly related to the focus of the gathering and others could introduce the community to skills or issues with a less direct connection. The program committee can make space for discussions of proposals at a meeting of the whole or plenary often held the final day.

This plenary could take an afternoon or an entire day. When a gathering comes to consensus on future actions or other proposals, they are more likely to succeed. As soon as the committee knows what workshops and meetings are proposed, a schedule can be produced as a calendar or chart with the days, times, workshop or meeting names, facilitators and their contact information and the locations. This schedule can be published in the program guide.

The facilitator of each workshop or meeting can also provide a paragraph or two describing the item to be included in the program guide. If the program committee is well organized, it might be possible to publish the schedule and description weeks or even a month or more before the gathering. Copies could be mailed to the participating chapters and posted on the website, providing additional time for everyone to discuss and contem- plate the issues that will be addressed at the gathering.

### Gathering Step 6: Venue, food, housing, transportation, healthcare, childcare and other needs

The venue committee can secure a place to have the gathering. This may be one large facility such as a school, place of worship, community center or a collection of spaces. Make a list of all possible venues and include the contact person, their phone number and physical address. Make an appointment to visit each venue and meet with the people responsible for providing access. Ask about the cost and requirements necessary for securing the use of the facility. Ask if it will be available during the dates of the gathering and if insurance will be required.

If you are able to use the venue, make arrangements to have one of your volunteers be the responsible party to stay in contact with the staff person responsible for the venue during the gathering. That volunteer should talk with the staff person a month, week and day before the gathering about entry to the venue. A venue should have one large room so everyone could participate in a meeting of the whole group and smaller rooms for workshops.

The venue might have outdoor space for camping or be near public transportation. It should be accessible to everyone and free of distractions like noise, extreme temperatures or high winds so everyone can focus on the workshops and meetings. The venue committee should make sure there are enough seats, toilets, video projectors, chalk boards and large sheets of paper with markers. The housing committee should write a letter to the community requesting housing for certain nights and make a list of all the places offered.

This list should include the name of those offering the place to stay, their address, phone number, email address and the number of people they can accommodate. Volunteers can be on hand at the convergence space as the participants arrive so they can direct them to their housing. Each participant can be provided with a page that includes the name, address and phone number of the person offering them housing.

The sheet can also include directions and a map to the housing. The housing committee might be able to secure school dorms, group camping or a gymnasium for the participants to stay. The committee may be able to send the housing details out to the participants in advance of their arrival. This information might indicate that the participants should bring their own sleeping bag, camping mattress or other items as required.

The transportation committee might take the information from the questionnaire about the transportation needs of people. If it is a world or international gathering, they may need to assist in applications for visas. The committee could investigate the visa requirements for people intending to attend from specific countries.

The committee might write a basic visa letter inviting the participants, using their full names and address, to attend the gathering on specific dates for the purpose of working with other Food Not Bombs volunteers towards the goal of the gathering. The visa letter would also state the name, address and phone number of the person responsible for hosting the applicant. A travel itinerary or plane, train or bus ticket may be required.

In some countries you can provide a letter about your gathering to the office that issues visas so they will have this information at the airport when people arrive. Your chapter will require additional food and may want to start collecting non-perishable bulk goods as soon as you have agreed to host the gathering. You can also request that the participating groups bring food to the gathering.

The food committee can draft a letter requesting special food donations and create a contact list of all the possible food sources. Many sources that might not make regular contributions to your weekly meal might make a one-time donation for a special event like the gathering. The committee might seek a large kitchen located at or near the venue. It may be possible to organize an outdoor field kitchen.

Along with bringing food, the participating chapters could bring some of their cooking equipment so it will be possible to provide food to everyone. A trick to feeding very large groups is to make many dishes or courses, instead of serving three items on each plate. Since most people participating in the gathering will be active with Food Not Bombs, the kitchen will be a great place to share ideas about cooking, and you shouldn’t have any trouble finding people to help. Many gatherings will organize a healthcare committee of medics and healers. The committee might designate a room or tent as the healthcare clinic.

The clinic could provide water, aspirins, bandages and simple first aid. Your chapter might ask the community to donate services like acupuncture, massage and herbs. The healthcare committee could contact the local emergency room and ambulance companies to let them know you will be having the gathering, telling them the location and number of people you expect will attend.

It is really helpful to offer childcare. Choose a safe place a short distance from the rest of the gathering so your children can be loud yet not too far from their parents. Organize games, collect children’s books to read out loud, provide costumes for dress up, balls for sports and work with the kitchen to have snacks and meals for the children before the adults. Gathering Step 7: Convergence center and the orientation of the participants As soon as you have a date, seek out a place for your convergence center.

This could be a room near the entrance of the gathering, or if the gathering is held in a number of locations, you may consider securing a space in an easy-to-locate building near public transportation. Try to have your convergence center at the same place as your gathering, if possible.

In some cities it may be more important to locate your convergence center near a subway or bus station and provide directions to larger venues in difficult to find areas of town, particularly if you find it necessary to use spaces located in many areas of the city. Remember, many of the people participating will not be familiar with the host city so make their first destination one that is easy for everyone to find. You can seek a free location with electricity and water or you may find it necessary to rent a space for one month.

The convergence space is where you will be greeting participants and providing them with the program guide, directions to housing and information about other logistics. Many convergence centers will have a notice board for rides from the gathering, announcements of special workshops and meetings that might have been proposed during the gathering. Your Indymedia Center might also be housed at the convergence space.

### Gathering Step 8: Workshops and meetings

Your host chapter will receive many workshop and meeting proposals. Your group can organize a schedule that takes into consideration a number of issues. Your group might not want to schedule workshops that you know will be of interest to the same people at the same time. Your schedule could include one morning, two afternoon and one evening slot.

Important meetings addressing the focus of the gathering or issues that might require the participation of volunteers from all participating Food Not Bombs chapters would be most successful if not scheduled at the same time as other workshops or other distractions. Food Not Bombs gatherings have offered workshops on vegan cooking for 100, how to start a Food Not Bombs chapter, using vegetable oil to power vehicles, composting, organic gardening, lock picking, water purification, train-hopping skills, wild food collection, giant puppet making, event planning, improving your use of consensus decision making, silk screening, stencil making and graffiti, racism and sexism in the movement, weaving, sanitation and food safety and building a strategy for a campaign of nonviolent direct action and nonviolence trainings.

Many gatherings will also have workshops on proposed or current campaigns such as highway blockages, actions to stop mining or logging operations, sovereignty rights, efforts to stop genetically engineered food or protection of animals threatened by hunting or habitat distraction. The subjects of workshops are only limited by the organizers imagination, interest and schedule. organizers imagination, interest and schedule.

The meetings on topics concerning the interaction between Food Not Bombs chapters could be one of the most important features of any gathering. Each participating Food Not Bombs chapter can discuss the proposed agenda items at their local meetings in the weeks and months before the gathering in order to be prepared to offer well considered ideas during the meetings. The gathering could schedule a “meeting of the whole” or “spokes council meeting” near the end of the gathering to consider the ideas generated during the earlier meetings. An experienced facilitator can make the meeting of the whole more productive.

Write down the decisions made at the “meeting of the whole” and make sure they are emailed to all participating chapters and posted on the gathering website so 85 all chapters can consider the proposals. Each chapter can review the proposals and seek consensus on their implementation. The gathering proposals might be modified, changed or blocked by one or more local chapters and returned to a future gathering to be considered before they are implemented.

### Gathering Step 9: Closing and Critique
The participants are likely to build close bonds with one another and be excited to return home to implement the skills and ideas generated during the gathering. Your gathering could end with a closing circle of all participants offering a critique of the gathering. Each activist could make suggestions of what could have been better and note what they believe was successful. They can also provide a short recap of the essential points or decisions addressed during the gathering.

## PLANNING A CAMPAIGN OF NONVIOLENT DIRECT ACTION

There is a proud history of nonviolent direct action that your group can study to help formulate the most effective strategy. As governments and corporations become more sophisticated in the use of violence to resist change, they are providing an opportunity for nonviolent direct action to be more effective than ever. (Author Gene Sharp has studied the history and theory of nonviolence for over fifty years and his books provide valuable information. We also provide a list of books and Gene Sharp’s list of 198 possible actions in the Appendix.) 

Governments and corporations are so fearful of the potential of nonviolent resistance that they are working to create the appearance that nonviolence can’t succeed and that nonviolent direct action is the same as being passive. Nonviolent direct action is far is the same as being passive. Nonviolent direct action is far from being passive and is often the most effective strategy we have to respond to the global economic, political and environmental crisis.

Food Not Bombs can provide a unique addition to any campaign, and that is food. We can feed a protest for as long as is needed, taking away one common limitation faced by people organizing protests. We also provide experience with collective decision making and practice with logistics: two essential skills required for implementing campaigns. There has never been a more important time to develop and implement well planned campaigns of nonviolent direct action.

Careful preparation, the study of nonviolence strategy and persistence can result in success. 

### Campaign Step 1: Identify and Analyze the Issue

Before starting any campaign, you might want to announce the formation of regular organizing meetings by posting flyers and emailing organizations and friends about your intentions. Early meetings can identify the issue and create a system or method to analyze all aspects of it. Identify the groups, companies and individuals that would support or op- pose your position. Consider why the public would either support or oppose each position related to the issue and investigate the relationship of each party to the issue.

You might want to “follow the money” or investigate who would benefit and who would be harmed. What would be the motivation for favoring or opposing any given approach? While the community might have a clear idea of the motivation and positions of each party, it could be wise to draft a public opinion survey to collect opinions and data.

Avoid asking leading questions. That would reduce the value of the data you collect. Your group might seek support from graduate students majoring in the field of public opinion research. Your group can find a list of college departments that might be able to provide assistance in developing your survey from the American Association for Public Opinion Research by visiting their site at www.aapor.org. There are similar organizations in many other countries that can provide access to graduate students. Other valuable organizations include the Institute for Policy Studies, based in Washington D.C., and Public Interest Research Groups (PIRG) started by Ralph Nader and his coworkers.

Many Food Not Bombs groups in Canada are spon- sored by PIRG chapters at local colleges. These projects can help your group clarify and identify the deeper aspects of the issue for your own group and introduce the public to the fact that the concern exists. This information can also be used to draft articles, posters, petitions and talking points.

You can analyze the data collected in your public opinion surveys to see how people view the issue, determine the amount of support or opposition that already exists and what message or information might change the public’s opinion. This can also help your group identify the strengths and weaknesses in your opponent’s position.

While collecting responses to your well drafted survey, determine what actions would motive your opponents to change their position. Can pressure be exerted against the people funding your opponents? 

Could supporters of your opponents be concerned about losing an election? Is it possible that negative publicity could threaten the prosperity of a corporation? What groups of people, government officials or corporate leaders depend on what kind of support and influence is exerted upon these parties to withdraw their support or cooperation?

As you collect this information, your group will become clearer about the strategy and message of your campaign. At first, your group might have an absolute set of goals that need to be achieved before ending the campaign such as ending specific policies, the complete protection of specific rights or habitat or an end to a war or an environmentally damaging commercial enterprise.

As your campaign continues, your pressure and tactics might increase, or your group may determine that even though the original goal was not achieved, the group’s opponents have modified their position enough to be satisfactory to adjust your strategy or even to claim victory and end the campaign. The more clear and specific your demands, the more likely it is that you can build support and win concessions.

Goals like “world peace,” “save the environment” or “love everyone” are so general it would be impossible for an opponent to change their policies and achieve any of these or many other vague demands. A campaign with focus and achievable demands would be more likely to succeed. “World peace” could be more likely be achieved by first organizing a campaign to convert a weapons facility to manufacturing solar power equipment.

Targeting Congress to cut funding for a new generation of nuclear missiles is something that could happen, and once successful, it would inspire confidence that we can take yet another step towards ending militarism. Organizing a campaign to stop the clear cutting of an area of the forest with specific owners, habitat and boundaries or blocking the construction of a nuclear power station provides your group with a clear goal that everyone can recognize.


Being clear about specific goals can then be followed by creating a path towards success. 


Nonviolent direct action at Seabrook Nuclear Power Station in New Hampshire in 1980.


### Campaign Step 2: Outlining the campaign and strategy

In many situations, building public support is essential in changing government policies or corporate behavior. Many successful campaigns start with a period of ior. Many successful campaigns start with a period of education. Literature is produced to explain the issue. Your group might organize a teach-in or public forum, inviting people from all sides of the issue to participate. Your group can organize other educational events like PowerPoint presentations, lectures with informed members of the community, puppet shows, websites, a documentary or staff regular literature tables outside groceries, in public squares and at events.

A petition is also a great tool and, in some cases, is all that is needed to have an impact. Petitions are a simple way to involve the public, build a contact list and show your opponents that you have popular support.

Your group can collect a certain number of signatures, such as half the people in town or 10,000 people, half the students at your school, or so many in one month. Once you reach this number, you can contact the media and organize a public event where you deliver the petitions, showing that the public backs your group’s position.

Sometimes a petition is all you need for your opponent to feel enough pressure to change their policies. This is particularly true if you acknowledge that your opponent has always been well intended and because of so much public support, they are eager to agree with your position. Even if your opponent fails to respond favorably to the petitions, your group can still remain positive that they will come around. Most campaigns employ an educational component during the entire effort even as the group escalates its tactics.

Your opponent’s failure to change, after being presented with a substantial number of signatures from the people whose support they require, can provide your group with a solid foundation for the next more drastic action in your campaign. Increase the severity or militancy of your tactic by measured degrees. After the appeal by petition, consider a vigil outside the opponent’s office, factory or other recognizable and highly visible location.

Your group may announce that you intend to return until the demands are met or for one month, at which time your group will call for a boycott, strike or other escalation of tactics. The more deliberate and measured the increase in pressure, the more public support you can gain. Offer a way for your opponent to change its policies or position while preserving its dignity. If your opponent understands this, you are more likely to succeed at changing their policies. This can be a very difficult position to maintain, particularly if your opponent is generally ruthless, violent and uses dishonest tactics in its defense.

If your opponents cause your group to perceive them without dignity, your struggle could become protracted and the members of your group could feel justified in taking actions that could erode their sense of self respect. It has been a common strategy of the powerful to project themselves as unreasonable and so disrespectful that their opponents believe them to be so evil that any action is justified in resisting their policies.

If they succeed, your group will lose its sense of dignity, take extreme actions that can be used to justify repression and cause your campaign to lose its self respect. If the community sees that your group is maintaining respect and taking persistent dignified action, even though it is facing violence, dishonesty and even prison and death, you will undermine the power of your opponent and draw popular support for your cause.

It is often at the point when an opponent is most brutal and is unable to undermine the self respect and dignity of your group that the opponent believes it must capitulate to the demands of the campaign. A dramatic and even deserved increase in tactics can be used against your campaign and cost you support, so take care not to escalate your tactics until it is clear to the public that your next actions are justified.

### Campaign Step 3: Discipline and persistence
If your group can maintain a disciplined dedication to nonviolence, you are likely to attract support from the general community, and your participants are less likely to lose enthusiasm for the campaign. Your group can maintain discipline by organizing nonviolent preparations in advance of each new escalation in tactics.

Your group might even start the campaign with a day of nonviolence preparation, even though your campaign is not expected to escalate to an action that will require a dedication to nonviolent direct action. Your campaign may last months, years or even decades before achieving the desired results. While the crisis we are currently facing is urgent and it doesn’t seem that we have much time to change the direction of society, it may be necessary to understand that the change we need could require determination and time to be successful. 

## BASIC TYPES OF NONVIOLENT DIRECT ACTIONS 

Governments and corporations have systematically discouraged protests, and made great strides in erasing the knowledge of nonviolent direct action. Textbooks might mention Dr. Martin Luther King, Jr. and his speeches, but there is little about the dynamics of the use of nonviolent direct action to end desegregation and improve the civil rights of Americans. Gene Sharp has a list of 198 types of nonviolent action. We include a list of books in the appendix to help you study more about the history, tactics and methods used throughout history.

In our effort to change society so that everyone has the food they need without the fear of going without, the following can be among the most effective methods. Some of the most basic types of nonviolent direct action are described below. Marches and Funeral Processions Marches travel through the community showing people along your route that you have support for a certain point of view or position.

The first step to organizing a march is to announce the day, time and location to meet. The location, such as a government or corporate office, could be chosen because of its relationship to the issue being addressed by the march. A rally can be organized at the meeting point. People knowledgeable about the issue can speak as people gather. Once it is clear most people intending to participate have arrived, then the march can leave the assembly area. Your march should be led by a banner stating the essential point of the action.

As an organizer, you might want to provide materials so people participating can make their own signs and banners during the speeches. You may end up blocking streets if you have a great many participants, but it can be valuable to let traffic pass so the public can see your message. On the other hand, if you have many supporters, the disruption of traffic can also have impact.

The key is to reach the public through other methods so the disruption is not attributed to construction or obstruction by anti-social elements. The route of the march can be determined beforehand and pass by buildings, factories, meetings or other locations related to the issue. On occasion, if someone is killed during your struggle your group might organize a funeral march. Even if no one has died during the campaign, but people are dying or could die if your actions fail, your group can organize a symbolic funeral march.

The march may end at a location that is the focus of the issue. Vigils, Tent City Protests and Occupations Many times the strongest and most effective action can be the vigil, tent city protest or occupation. Tent city protests ended the rule of Arab dictators in 2011 inspiring a global wave of occupations. If there is one main location such as government or corporate offices that decision makers frequent, you might consider organizing a vigil and tent city protest.

The action can start by simply standing outside the facility with banners, signs and literature. Your vigil might start out being just an hour once a week at the entrance of the target, then escalate to one hour every day, to all day, and then, if there has been no movement in the position of those your protest is directed against, you may consider a twenty-four hour vigil and tent city protest.

Governments and corporations have little tolerance for this type of powerful protest. Your group may need to slowly introduce more elaborate props until you are able to set up the first tent. These props can be a literature table, large banners, giant puppets, flags and cardboard images of tents or symbols representing the intention of the vigil. Provide literature to all those passing. Your largest banners and signs can announce clearly the issue about which you are protesting. Vigils with many worthy messages can have much less impact.

The power of this type of action is persistence. Your group may want to facilitate a nonviolence preparation for the participants. There may be times when the police are called in to remove the vigil and your group will want to be prepared for arrest. Returning to the site after being driven off by the authorities can be very powerful. You may need to return with banners and signs and replace your larger tents and structures once you have retaken the space. Your group might want to offer regular events during the vigil such as concerts, poetry readings and other performances. 

There may be an appropriate time such as shift change or every lunch break where the event will have the most impact. Your group might announce special events on the weekends to invite less committed people to show their support. Many vigils will post a sign announcing the number of days of the action. Your vigil might include people that are fasting and they may wear a sign saying how many days since they last ate.

Fasting can be very dangerous so it is wise to enlist the support of nurses or doctors to monitor the well-being of those who are participating in the fast. Food Not Bombs can provide meals on site so people don’t have to leave. Food Not Bombs has provided food at several successful tent city actions including a twenty-seven day vigil in San Francisco in 1989; the 100 day action during the Orange Revolution in Kiev, Ukraine; Camp Casey in Crawford, Texas; at a 600 day farmer’s tent city protest in Sarajevo and Occupy Wall Street movement in hundreds of cities.

Food Not Bombs joined the protests, setting up a field kitchen on site. Tent city protests were successful in ending the dictatorial rule in Tunisia, Egypt and the other repressive Middle Eastern countries in early 2011. As the global economic, environmental and political crisis grows more urgent, we might consider organizing more tent city protests as they have been very effective in the past.

Occupations and Flash Mobs After trying other tactics, it might be necessary to consider an occupation. Your group might choose to occupy an office, school, factory or your target mayneed to be a construction, mining or logging site. With the foreclosure crisis and increase in homelessness, the occupation of abandoned housing or land could be the most appropriate strategy. Your group could choose to occupy a building because of its symbolism to the issue being addressed. Your group might take over the offices of a government official or school administrator. Workers might occupy their factory during a strike.

Your group’s strategy could involve an element of surprise or use the public announcement of the action’s location for influence. Using text messaging to call a flash mob can be effective, but governments are starting to block cell phone service during times of unrest. Food Not Bombs was born from the founders’ participation in the occupation attempts at Seabrook Nuclear Power Station in New Hampshire.

Announcement of the occupation may have influenced the power company to mobilize a costly defense contributing to it’s being the last nuclear power station to go online in the United States. Food Not Bombs also participated in the founding of Homes Not Jails, taking over abandoned buildings in plain sight.

Dressed as construction employees, the activists would break open the front doors of houses they discovered were empty because of ownership disputes with banks. Once they put their own locks on the front doors, they invited homeless families to meet them at the targeted house at 9:00 AM. The activists would unlock the front door and let the new occupants in and help them clean and repair the house, pretending to be the management.

At the same time, strategic buildings that could not be repaired with the meager resources of Homes Not Jails would be occupied on symbolic dates, with banners hanging from the windows to pressure the authorities to provide housing instead of jails to those living on America’s streets. It may be necessary to have a support group outside the occupied facility that contacts the media, builds community solidarity, promotes the goals of the occupation through flyers and vigils outside the offices of decision makers and provides material assistance to those inside.

Pre-planning sanitation and access to water and food is important. Your group can also organize legal support before the action. Food Not Bombs has provided water, food and material aid to occupants, making it possible for their occupations to survive days, weeks and even, sometimes, years. Blockades and Lockdowns Disrupting the business or activities of those engaged in dangerous, unjust or cruel policies may require an escalation to a blockade or lockdown.

Your group’s strategy might require the blockade of an entrance to a factory, office, retail establishment, construction site or logging or mining road, slowing or stopping the operations. Your group might organize large numbers of participants who agree to arrive at one location, filling the streets or entrances with so many people it is not easy or Citizen’s weapons inspection at Raytheon Missle Systems in Tucson, Arizona possible for the opponents to continue their activities. The blockaders could simply stand or sit in the way.

A standing group might plan to sit or be urged to sit when facing repression. Your group’s strategy might include linking arms or locking one another together or to heavy symbolic objects to slow the removal of the blockage. Sometimes activists have used heavy materials such as old cars, logs and concrete to block access.

Locking yourselves to entrance doors, tractors or other implements needed to continue the offensive activity can be effective. A support team can promote the purpose of the action, speak with the media, coordinate with lawyers and provide the blockade with food, water and other materials. Risking Arrest Sharing Free Meals When Food Not Bombs was first arrested for making a “political statement” by feeding the hungry, observers suggested that this was “America’s Salt March,” after the marches organized by Gandhi in India.

When a state or corporation attempts to stop a basic activity like feeding the hungry or gathering sea salt, it can provide your community with an opportunity to expose larger truths. Nonviolent noncooperation with orders to interfere with basic acts of survival can be a powerful way to inspire resistance.

Instead of letting the police walk off with our food, we can simply hold on to our containers of soup, salad, rice, beans, pasta and bread. Your group can divide the food into two smaller portions that are expected to be seized and a last large portion that can feed everyone that comes to eat. Your group can also make temporary “Food Not Bombs” banners for each expected act of police interference. The banners are often the first item taken by the authorities.

The police might arrest several volunteers for sharing food without a permit and take the banner and food. Your group can return with another small amount of food and temporary banner and the police might interfere again.

After the second wave of arrests and confiscations, your group can return with the rest of the food, your regular banner and literature, and proceed to feed everyone with little concern that the police will make more arrests that day. If they do return and seize your food and banner, then your group knows that they must divide the food and banners into fours. Your group can take video and photos of the arrests and confiscations, which can be helpful in court and in the publicity of the issue. It is helpful to organize legal support people who work with the lawyers and follow those arrested through the system.

They can speak with the media, lawyers and public about the issue while those arrested are in custody. They can also contact employers, water gardens and house plants and feed and walk pets. Each court appearance is another opportunity to share food outside the courthouse. If volunteers are convicted and sentenced to jail, it is possible to continue the pressure by sharing meals every morning outside the courthouse, informing the public that the court had the audacity to jail someone for feeding the hungry.

This can undermine the authority of the legal system and cause judges to release the imprisoned food sharer. Strikes and Walkouts Strikes are the withholding of labor or cooperation with an employer or institution. Along with failing to participate in the tasks or activities required by the institution or employer, your associates and supporters can organize a picket line at the site of the conflict or in visible public locations with banners, signs and literature about the issue for which you are striking.

A sit-down strike is one where the workers or students refuse, not only to participate in their work or studies, but also to leave the place of employment or school. A general strike is one where all workers and students refuse to work or study. A local strike can build to a general strike. Before calling a strike, you may want to take less dramatic steps to prepare the community with education about the conflict and provide opportunities for the issue to be resolved. A support team can help coordinate legal and logistical assistance.

A strike will often be able to last until its demands are met if Food Not Bombs provides food to the families of those on strike. Food Not Bombs Relief Efforts Food relief isn’t often considered a method of nonviolent direct action, but the inability of governments to respond to emergencies has made it necessary for Food Not Bombs to fill that void.

In doing so we also highlight the real priorities of the authorities. Food Not Bombs often finds itself at the center of economic, political or environmental crisis. Our volunteers can be the first to respond to disasters. Your group may live in an area prone to hurricanes, tornados, cyclones, earthquakes, floods, blizzards, fires, drought or other natural phenomenon that could require an emergency response.

Your community might face a political or economic crisis, providing Food Not Bombs with an opportunity to provide assistance. Your chapter can prepare for disasters by organizing benefit events to buy bulk rice, beans, oats, flour and other dry goods. It can also be helpful to have a few large propane stoves, although your group can cook on fires fueled by scrap wood, coal or other flammable materials. Your chapter might include disaster relief as an agenda item at your meetings to discuss plans on preparation and implementation of local relief efforts.

Try to imagine cuts in communication, water, power and other resources. Your group might want to store fresh water, solar electric generating equipment, bulk dry goods and first aid equipment. Consider organizing classes in first aid, water purification and sanitation. Food Not Bombs can often respond to crisis at times when larger institutions are not able.

Relief organizations, like the American Red Cross, will sometimes find it difficult to provide assistance and will give survivors our phone number so we can direct them to the locations of our meals.

In the first few months after Katrina, we received dozens of calls from people seeking food, telling us that the American Red Cross directed them to our program. Large institutions may be required to follow government regulations and issues of legal liability. Their hierarchical command structure can also slow down their ability to respond quickly to disasters. Food Not Bombs is local, flexible and free from political or legal restrictions.

In 1989, the San Francisco Bay area was hit by a huge earthquake. San Francisco Food Not Bombs was preparing its regular dinner on an apartment stove when the earthquake rolled through the city. Gas and electricity were suddenly cut off.

Fortunately, the volunteers had propane stoves and were prepared to cook outside. They collected all their equipment and drove down to Civic Center Plaza where the group expected to share dinner. They set up their tables and stoves and finished cooking dinner. Grocery stores and produce markets lost power to their walk-in refrigerators and some called Food Not Bombs to retrieve their perishable food. Hundreds of additional people showed up for dinner.

The police had arrested the servers the day before, but this time they joined the line of hungry, shaken San Franciscans seeking dinner. The American Red Cross finally arrived in the wealthy Marina District three days after the earthquake. Until then, San Francisco Food Not Bombs provided meals to hundreds of people. Food Not Bombs also provided meals to the survivors of the 1994 Northridge Earthquake in Southern California and to survivors of the 2010 earthquake in Chile.

In August 2005, Food Not Bombs volunteers learned that a hurricane threatened the gulf coast of the United States. As soon as the hurricane was named they posted a Katrina page on their website and started an email conversation on their listservs. Hartford, Connecticut Food Not Bombs packed its blue school bus with food and equipment and started off towards New Orleans. Houston Food Not Bombs prepared to feed the refugees flooding into the Astrodome and Convention Center. Tucson Food Not Bombs sent a bus load of food, volunteers and equipment. Survivors and supporters called Food Not Bombs’ toll free number.

The volunteers organized contact lists of drivers, food collections and volunteers by routes. The website was updated many times a day with details of kitchen locations and other logistics. Hundreds of Food Not Bombs volunteers mobilized, traveling to the Gulf to set up kitchens, free survivors from the attics of their homes and provide food and support to communities ignored by the government and institutional relief organizations. Survivors had an opportunity to have direct experience of the principles of Food Not Bombs. Food Not Bombs groups have provided food and logistical support for political and economic crisis. The election in the Ukraine was stolen, and Food Not Bombs provided meals to protesters of the Orange Revolution in November 2004 to January 2005.

Volunteers also provided meals to striking farm workers in Sarajevo during a 600 day tent city protest. They fed striking auto workers in Korea and shared meals at protests during the 2008 economic collapse of Iceland. Your chapter can make a huge impact by preparing to respond to local disasters. Food Not Bombs is one of the few movements that can respond swiftly to ease the suffering of survivors.

Our rapid response can encourage community self-reliance and provide an example that the core principle of Food Not Bombs might be a good substitute for corporate and government domination.

## FOOD NOT BOMBS PROJECTS
Food Not Bombs has inspired a number of “do it yourself” projects. These projects share many principles with Food Not Bombs including a critique of the economic system, dedication to collective decision making, and a desire to provide a direct service or perform a task that introduces the public to our philosophies of a non-hierarchical, decentralized, social organization that encourages self-reliance and an independence from corporate and government domination.

The most common projects are Food Not Lawns, Homes Not Jails, Free Radio Stations, Indymedia, Really Really Free Markets, and Bikes Not Bombs. Food Not Lawns Food Not Bombs has been recovering abandoned lots to plant organic gardens for over twenty years, often calling them spiral gardens. We used rubble to build spiral mounds that provide micro climates. Volunteers organize festivals at an abandoned lot, bringing rakes, shovels and other tools and free meals for the gardeners. Food Not Bombs invites the neighborhood to participate.

They might post flyers around the community. They remove the garbage from the lot, recycling what they can. Once cleared, they would start a compost, turn the soil and lay out the beds. The next festival might include the planting of flowers, vegetables and fruit trees. A schedule of watering might be organized and the community might hold weekly weeding and harvesting parties. Food Not Bombs volunteers recovered lots in several California communities in the late 1980s and early 90s. They also helped revive the famous garden in People’s Park Berkeley.

New Brunswick Food Not Bombs started a community garden and produced a documentary detailing the progress and joy of organic gardening. Many other chapters also started local gardens, some on recovered land and others in cooperation with local schools and community centers. Food Not Bombs volunteers have also supported many other community gardens not initiated by their chapters.

In the late 1990s, Eugene’s Food Not Bombs volunteer, Heather Flores, and her friends were working in their community garden. Truck loads of sod passed them daily on their way to become lawns in Las Vegas, Los Angeles and Phoenix. Heather was already inviting the community to help her garden. She brought the garden surplus to the Food Not Bombs meal. Seeing trucks of lawn heading to the desert as thousands of people were going hungry was too much to bear. Food Not Lawns was born. 92 Food Not Bombs volunteers could see the logic.

Soon Food Not Bombs groups were starting Food Not Lawns gardens. Chapters have started gardens in over 200 cities. A Food Not Lawns gathering was held in San Diego, California. Peterborough, Canada Food Not Bombs started a garden that became a weekly news story in the local paper. Heather Flores wrote the book, Food Not Lawns, providing a detailed plan on how to bring your community together to plant their own Food Not Lawns organic garden. Homes Not Jails Housing foreclosures are at record highs in many parts of the world.

Still, many other people have never considered buying a house, and many millions live outside, unable to afford shelter of any kind. The United States faced a foreclosure crisis in the 1990s. City governments started programs designed to drive the homeless out as hundreds of buildings stood empty. The San Francisco police arrested Food Not Bombs volunteers to silence their defense of the city’s homeless. Laws against people panhandling, sleeping outside and sitting on sidewalks were introduced. The mayor started his matrix program confiscating homeless people’s shoes, blankets, and other belongings. Many were arrested for “quality of life crimes” such as sleeping in parks. 

New people arrived at the Food Not Bombs meal explaining that they had been evicted from a low cost hotel across the street from the Glide Memorial Church soup kitchen, and now had no place to live. We also learned that the mayor would be celebrating Thanksgiving with a photo opportunity serving turkey to the hungry at Glide.

Food Not Bombs talked with the San Francisco Tenants Union about organizing an action to protest the evictions and hypocrisy of the mayor. Activists broke into the empty hotel the night before Thanksgiving. They brought food, water, blankets and banners. When the mayor arrived for his photo-op, the activists emerged from the hotel windows with banners. One said Homes Not Jails. The group had also broken into another abandoned building a block away. Food Not Bombs asked several families that frequented their meals if they would be interested in free housing. They were excited.

Food Not Bombs suggested they meet them at a social service office near the empty building. They did and the group walked through the Tenderloin to their new home. The Homes Not Jails activists had put their own lock on the door so the families slipped in quickly once the door was unlocked. After that success, Homes Not Jails started a program of riding bikes through the city, writing down the addresses of every abandoned building.

They would take the addresses to the tax office at City Hall to find out who owned the structures. If a family owned the building, it was taken off the list, but if the building was in foreclosure and banks were suing one another for the mortgage, that building was listed. Volunteers would travel through the streets with crow bars, bolt cutters, locks and hasps. They would break open each building and put a new lock on the door.

Then, at dinner, Food Not Bombs would ask if anyone wanted a free place to live. They were instructed to meet the Homes Not Jails activists at a specific address at 9:00 AM the next morning. Once everyone had arrived, the Homes Not Jails volunteers, dressed in hard hats with tool belts, would unlock the door and let everyone in. They would also bring cleaning supplies, paint and tools to help make the abandoned house livable.

The new tenants were given a lease to show the police if they happened to question their legality. Neighbors were pleased that the abandoned building was finally being rented. According to the book, No Trespassing, by Anders Corr, San Francisco Homes Not Jails had locks on hundreds of buildings and nearly 100 were occupied at times throughout the city.

If a family lost their place, Homes Not Jails helped them move to one of their other locations. Homes Not Jails started in other American cities. Boston Homes Not Jails started on Thanksgiving 1995 and organized four public takeovers in two years. There have been a number of Homes Not Jails groups organized in Washington, D.C. One of the last groups started in June of 2000, taking over a building at 304 K Street NE in February 2001.

Three activists were arrested but found not guilty by a jury. Asheville Homes Not Jails started organizing actions in their North Carolina community in the winter of 2002. Food Not Bombs groups in Washington, D.C., Boston and other cities started their own Homes Not Jails programs during the Savings and Loan crisis of the 1990s. Food Not Bombs supported Take Back the Land actions in Florida and tent city occupations in California as housing foreclosures started to force families onto the streets. Soon, Homes Not Jails was starting a new burst of action responding to the foreclosure crisis that started in 2008.

San Francisco Homes Not Jails continues to occupy property as the housing foreclosure crisis grows. On Homes Not Jails takeover in San Francisco April 4, 2010, Homes Not Jails took over the former home of Jose Morales at 572 San Jose Avenue in the Mission District. The now 80 year old Mr. Morales spoke about his fourteen year struggle to stay in the home he had lived in for fourty-three years. As is often the case, Food Not Bombs provide the food for the occupiers and their supporters. On July 20, 2010, San Francisco Homes Not Jails occupied the Hotel Sierra, a fourty-six unit building in the Mission that had been abandoned for over a year.

Free Radio The corporate media mostly ignores our movement, but in some occasions it distorts our efforts, or it wages propaganda campaigns against the communities that Food Not Bombs supports. So volunteers in the San Francisco Bay Area figured out how to build low power FM Radio Transmitters. Seizing the air waves was similar to seizing abandoned housing and defying orders against sharing food with the hungry. We had been hearing about Mbanna Kantako and Black Liberation Radio in Springfield, Ohio, which was broadcasting reports and commentary about the crisis of police violence in the community.

Stephen Dunifer talked with Kantako to see how difficult it was. Stephen was trained as an electrical engineer and figured he would be able to build a transmitter. He started organizing workshops in 1993, inviting ac- 93 tivists to learn how to produce transmitters at home. Richard Edmondson was inspired by the classes and joined Stephen in building two radio stations: Free Radio Berkeley and San Francisco Liberation Radio. Stephen explained that, “in order to apply for an FM broadcast license, the station had to be at least a Class A station with a minimum power level of around 200 watts or so. With the removal of the Class D allocation there was no licensing process for stations under that power level.

Until the enactment of the FCC LPFM in 1999, no licensing process existed for stations operating with less power than the minimum required for Class A stations” so we made transmitters that broadcast below 200 watts, obtained microphones, mixers, a power supply, tape players, coaxial cable, batteries and other materials. We climbed up into the hills above the bay to broadcast the news and information that was ignored by even the most progressive radio stations. Free Radio Berkeley started broadcasting news about anti-homeless attacks, old labor songs, and information about protests in the Bay Area on April 11, 1993. San Francisco Liberation Radio first broadcasted from an apartment on Clement Street, San Francisco on May 1, 1993, but soon moved to the hills around the west side of the city.

Before long, the station moved into fixed locations broadcasting from the homes of local activists. San Francisco Liberation Radio was anizing workshops in 1993, inviting ac- stopped by the San Francisco Police un stopped by the San Francisco Police under orders of 94 the FCC on September 22, 1993 after broadcasting from Twin Peaks, ending the mobile era. The FCC tried to get the federal court to grant an injunction forcing Free Radio Berkeley off the air, but on January 20, 1995, April 14, 1996; and yet again, on November 12, 1997, Federal Judge Claudia Wilkin declined the FCC’s petition against the station.

The ruling angered the National Association of Broadcasters (NAB). KFOG Radio engineer, Bill Ruck, reported on Free Radio Berkeley and San Francisco Liberation Radio at the National Association of Broadcasters conference, showing photos he had taken of the apartment housing San Francisco Liberation Radio. Free Radio Berkeley was broadcasting twenty-four hours a day, seven days a week at 104. 1 FM with fifty watts of power until June 16, 1998, when Federal Judge Claudia Wilken granted the FCC their injunction, silencing a vibrant voice of the local community. ble low watt FM radio stations wer event.

As news spread about the pos the FCC on September 22, 1993 after broadcasting from the airwaves, activists were soon broa Twin Peaks, ending the mobile era. ofThe FCC tried to get low watt stations. the federal court to grant an injunction forcing Free Radio Judge Claudia Wilken’s ruling in t the door for FCC repression, Berkeley off the air, but on Januaryopened 20, 1995, April 14, stations by granting low power lice companies on the sam 1996; and yet again, on November 12,churches 1997, and Federal Judge tivists were broadcasting, knocking Claudia Wilkin declined the FCC’s petition the stations off theagainst air. Activists applied for little success, and a new wave of free tion. The ruling angered the National Association of Broadthe new rules setting their frequenci dialBill left empty by the reported FCC. casters (NAB).

KFOG Radio engineer, Ruck, We can take backLiberation the air waves by c on Free Radio Berkeley and San Francisco Berkeley. While they don’t broadca Radio at the National Association of eager Broadcasters to provide everyconfercommunity and antenna. A new wav A new station, Berkeley Liberation emerged to re- oftransmitter ence, showing photosRadio, he had taken the apartment housance may reemerge. New FCC polici place Free Radio Berkeley, defying the federal injunction. apply a low power FM radio perm ing FCC San Francisco Liberation Radio. FreeforRadio Berkeley The issued “notices of apparent liability,” fining the was broadcasting twenty-four hours a day, seven days a week at 104. 1 FM with fifty watts of power until June 16, 1998, when Federal Judge Claudia Wilken granted the FCC their injunction, silencing a vibrant voice of the local community. A new station, Berkeley Liberation Radio, emerged to replace Free Radio Berkeley, defying the federal injunction. The FCC issued “notices of apparent liability,” fining the operators tens of thousands of dollars, amounts so high there was no possibility of the government getting even a fraction. Food Not Bombs volunteers and their friends were starting their own micro-powered FM stations, including Radio Mutiny, Free Radio Santa Cruz and many others.

Food Not Bombs took a five watt transmitter on the 1994 “Rent Is Theft” tour, broadcasting each evening’s cooking demonstration and presentation to transistor radios placed around the room where the audience sat. Instructions and diagrams on how to build transmitters and assemble low watt FM radio stations were distributed at each event. As news spread about the possibility of reclaiming the airwaves, activists were soon broadcasting on hundreds of low watt stations. Judge Claudia Wilken’s ruling in the summer of 1998 opened the door for FCC repression, silencing many of the stations by granting low power licenses to conservative churches and companies on the same frequencies our activists were broadcasting, knocking many free radio stations off the air. Activists applied for the new licenses with little success, and a new wave of free radio stations defied the new rules setting their frequencies to locations on the dial left empty by the FCC. We can take back the air waves by contacting Free Radio Berkeley.

While they don’t broadcast anymore, they are eager to provide every community with a low-powered transmitter and antenna. A new wave of free radio resistance may reemerge. New FCC policies make it possible to apply for a low power FM radio permit, but many of those permits are being issued to large, extremely conservative religious broadcasters taking the frequencies once used by the free radio movement. Indymedia The decentralized nature of the World Wide Web was making many technological advances, and its decentralized feature reflected the organizational structure of Food Not Bombs. Corporate media were championing anti-homeless campaigns, illegal wars and the exploitation of the environment, animals and workers.

Corporate media also distorted or ignored efforts by community groups like Food Not Bombs. The San Francisco chapter proposed Food Not Bombs hold a second International Gathering. Food Not Bombs volunteers were being arrested several times a week sharing meals at United Nations Plaza in San Francisco. At the same time Mayor Jordan announced he would be hosting the fiftieth anniversary celebration of the founding of the UN in June of 1995.

He planned to dedicate a monument to the Universal Declaration of Human Rights several feet from where he was directing the police to make the arrests. There was broad support for a second gathering and San Francisco Food Not Bombs announced it would host the ten day event during the United Nations Celebration.

Several computer programmers that volunteered with Food Not Bombs suggested they could use the Web to post news of the gathering to the world. They secured several computers and set up an Internet connection in the convergence center and announced the founding of the D.I.Y. media project, Indymedia. A daily newsletter about the gathering was posted and emailed to a Food Not Bombs listserv, developed by volunteers in Toronto. The San Francisco Police seized cases of the Universal Declaration of Human Rights from Food Not Bombs, made arrests of participants delivering food to the gathering, rounded up activists during marches and detained people who occupied abandoned military housing at the Presidio.

At the same time, volunteers facilitated workshops on cooking, gardening, the building of transmitters, housing occupations, the use of vegetable oil to power diesel vehicles, consensus decision making, the creation of Indymedia centers, and racism, sexism and homophobia. Over 600 Food Not Bombs volunteers participated and many shared time in the local jail. Many others witnessed the possibilities of Indymedia.

Food Not Bombs volunteer James Ficklin produced the documentary, Food Not Bombs International Gathering ‘95, showing volunteers working at the first Indymedia Center in the convergence center near United Nations Plaza. Activists started Indymedia centers in a number of communities soon after the San Francisco gathering.

Programmers in Australia designed self publishing software so media activists could upload reports in text, photos, sound and video. By 1998, volunteers were putting up Indymedia websites in many major cities of North America, Europe and Australia. An Indymedia Center was organized on November 24, 1999, to cover the protests against the World Trade Organization in Seattle, Washington.

The Seattle 95 IMC offered a grassroots view of the protests and countered the corporate media’s spin. The small network of Indymedia Centers started to grow after Seattle, and two years later, there were eighty-nine Indymedia websites reporting from thirty-one countries and the Palestinian Territories. Temporary IMC centers started to be a regular feature of most antiglobalization actions, social forums and other large actions. On occasion, the police would attack the Indymedia Centers. Police shut down the satellite feed from the IMC coverage of the Democratic National Convention in Los Angeles on August 15, 2000 and violently attacked volunteers at the IMC center at the G8 Summit in Genoa, Italy in July 2001.

Indymedia video was used in the trial of the activists charged during the protest. On October 27, 2006, New York–based Indymedia journalist, Brad Will, was killed covering the strike and occupation in the city of Oaxaca, Mexico. Each Indymedia is collectively organized. The volunteers have various policies on the content, but gen- erally; unless the information is clearly racist, sexist, homophobic or otherwise extremely disrespectful; anyone can post text, images, photos, sound files or video. Some collectives monitor their sites more than others, but each site posts clear guidelines that seek to provide as open access as possible.

There is a central site that posts news from all over the world and provides access to local Indymedia sites organized by region. That site can also focus attention on specific sites covering prominent protests, uprisings or other major news events. News, in seven languages, is linked to the central site. It is possible for anyone to submit material to their local Indymedia site as well as the central site. Food Not Bombs volunteers are often active with their local Indymedia center. It is also possible to start an Indymedia center in your community and your chapter is encouraged to post news about your activities to your local site.

Really Really Free Market The first Really Really Free Market I heard of was organized around 2001 by Food Not Bombs volunteers in New Zealand taking the 1960s Height Ashbury Free Store idea outside to their local park. The volunteers organized areas of free clothing, music al- bums, furniture, appliances, books and other items across the lawn in a local park. Christchurch Food Not Bombs held markets four times a year. Food Not Bombs also provided free meals.

The idea soon spread to Food Not Bombs groups in Asia. The Jakarta Really Really Free Market drew people from islands all over Indonesia. Volunteers not only provided free merchandise, they also offered free hair- 96 cuts and medical attention. The first Really Really Free Market in the United States was held during the protests against the Free Trade of the Americas Agreement summit in Miami in 2003, providing a unique response to the exploitive trade policies proposed by American rulers. A Really Really Free Market was also held in Raleigh, North Carolina, in solidarity with the Miami action.

Many Food Not Bombs groups had been providing free vegan meals as part of their participation in Ad Busters’ local Buy Nothing Day actions but, after the Free Trade of the Americas Agreement Summit in Miami, many groups added Really Really Free Markets to their annual activities. A number of Food Not Bombs chapters started quarterly or monthly Really Really Free Markets. Wilmington, North Carolina Food Not Bombs always included a Really Really Free Market at every meal. There have also been Really Really Free Markets held in memory of volunteers who have died or in solidarity with anti-globalization actions in other parts of the world. Your local Food Not Bombs group can attract more support by organizing regular Really Really Free Markets.

To make the day even more interesting ask local bands to play music and encourage other types of entertainment to participate. The Really Really Free Market is becoming a popular response to materialism, promoting sharing and the ideals of the gift society. Bikes Not Bombs In 1979, Carl Kurz traveled to New England from Austin, Texax to participate in the actions to stop the Seabrook Nuclear Power Station in New Hampshire.

After arriving he started working at the bicycle repair collective in Cambridge, Massachusetts and visited the Food Not Bombs house at 195 Harvard Street. Food Not Bombs co-founder, Mira Brown, also loved bike repair and soon Carl and Mira were talking about how they could use this interest for social change. The U.S. had a blockade of Nicaragua, making delivery of items like food and fuel difficult at the same time the Reagan administration waged the brutal Contra War against the new ministration waged the brutal Contra War against the new Sandinista government. Mira and Carl decidedcould they could Sandinista government.

Mira and Carl decided they tions in other parts of the world. Your local Food Not Bombs group can attract more support by organizing San Francisco Food Not Bombs helps with the Really Really Free Market Critical Mass ride in Barcelona, Spain 97 encourage resistance to the embargo by sending bike parts to Nicaragua.

They called their idea Bikes Not Bombs, and started collecting old bikes, frames, wheels and other parts. Carl announced the founding of Bikes Not Bombs and by 1984, he was traveling to Nicaragua with bicycles and setting up workshops throughout the country teaching bike assembly and repair. Bikes Not Bombs delivered hundreds of bicycles to Nicaragua and trained Nicaraguans how to build and maintain their clean, fuel-free transportation.

Bikes Not Bombs set up a workshop in Boston in 1990 and started providing bikes to low-income children in the area. They also trained over 16,000 young people in bike safety and organized projects in Central America, Africa, the Caribbean and New Orleans, donating over 25,000 bikes to people in those communities. Several collectives connected to the Boston Bikes Not Bombs workshop started in the United States, assisting Carl’s project.

At the same time, Food Not Bombs activists not aware of Carl and the Bikes Not Bombs workshop in Boston started local Bikes Not Bombs chapters all over the world, collecting, repairing and providing free bikes to low-income people in their communities. Bikes Not Bombs volunteers organized their chapters using the same principles they had adopted with their local Food Not Bombs groups. They would use consensus to make decisions, volunteered their time and shared the bikes for free. Some Bikes Not Bombs chapters offer bicycle repair classes after the Food Not Bombs meals, while others organize free bike repair clinics promoted at the meal held at a volunteers’ garage or back yard.

Once a local Food Not Bombs chapter is established, the volunteers often add projects like Bikes Not Bombs, Food Not Lawns community gardens, Homes Not Jails, Really Really Free Markets, Indymedia Centers and low powered FM radio stations to their effort to build a sustainable future. This model of building collectives that provide for all our needs based on the principles adopted by Food Not Bombs can create a foundation for long-term social change.

Bikes Not Bombs provides the transportation aspect of our decentralized DIY (Do It Yourself) community. If we can organize local Bikes Not Bombs collectives, grow our own food in Food Not Lawns community gardens, share items at Really Really Free Markets, report our own news and house our friends in Homes Not Jails squats, we can also provide for many other items needed by the community. This kind of change is not easy for corporate or government interests to co-opt and is the most powerful way for us to replace the current failing political and economic systems.