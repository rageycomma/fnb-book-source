# Preface

Once upon a time there was a small group of friends who thought they could change the world. They lived in a run-down house in a rough neighborhood. They had very little money and no political power at all. They had nothing to support their bold belief, other than youthful optimism and a marginal grip on ‘reality.’ With no heat,they sat around the stove through cold winter nights and talked about their ideas. They made plans. They dreamed out loud.

One day they cooked a pot of soup and went downtown. They set up a makeshift table and served that soup to hungry people, right in front of a big fat bank. The warm and well-dressed wealthy walked on by, pretending not to see. I was there that day. I brought a drum and played it. It was the only thing I could think of to do, besides occasional turns at the table, serving soup. Drumming was something I could do well and I happened to have a drum. So I played until my fingers bled, believing that, if I didn’t give up, I could make those people see us. Then maybe they would do something about the terrible injustice of hunger that was right before their eyes. 

Thirty years later: I walked across the street to take a break from work. It was a warmish spring day, though there was still plenty of snow on the mountains. I bought a cup of tea and read flyers at the café, mostly announcements for concerts here in Durango and nearby Telluride. One of them had a familiar logo, a purple fist wielding an orange carrot. ‘Food Not Bombs Co-Founder, Keith McHenry, to speak at Fort Lewis College,’it proclaimed. ‘Lunch will be served.’ I pulled out my cell phone and called my old friend. 

“You’re coming to Durango?” 

“I was going to call you, it’s been real busy around here.”

“You have to stay at my place.” 

“Sure, meet me at the college.” 

On the designated day I went to the college but Keith was not there. He had been delayed on his way from San Francisco. A handful of young people stood around a folding table with tea and soup. They looked a bit concerned. A small crowd waited on the benches nearby. After a moment of internal debate I approached the table. 

“Listen, if you need someone to fill in, I can talk about Food Not Bombs. I’m a co-founder too.” 

The kids were grateful, the crowd was polite, and I made it through the impromptu speech without too many side-trips into my own, personal stories. Of course, I could not talk about anything Food Not Bombs has done in the past ten years, since I left San Francisco and moved to this quiet mountain town. But before that time I was always in proximity to the action. 

I could talk about the long lines of homeless people waiting in front of City Hall, the arrests and beatings of volunteers, the buckets of soup and bags of bread hurled onto the sidewalk and smashed under officers’ boots. We had a radio station back then, unlicensed of course, from which we would broadcast Keith’s calls from prison. With each arrest, Food Not Bombs grew bigger. Oppression was like a fertilizer, every time that boot stamped down it only served to till the earth and push the seeds of resistance deeper. New groups sprouted up all over the world. When I left San Francisco, I knew Food Not Bombs didn’t need me anymore; it had a life of its own.

​<img src="introduction-moscow.jpg" alt="introduction-moscow" style="width: 800px;" />
People come to eat with one of the Moscow Food Not Bombs chapters in the Russian capital


<img src="introduction-carolina.jpg" alt="introduction-carolina" style="width: 800px;" />
Food Not Bombs volunteers risk arrest sharing vegan meals in Orlando, Florida

After the speech, I hung out with the local FNB group for a while. Young, clear-eyed and kind-hearted, they had abundant ideas for ways to build a better world. For the past decade, I have been concerned with building sheds, fences and garden beds. I have been writing, trying to make a difference with language; words are my drumbeats now. I had forgotten about Building with a big B. That’s when you take a bold idea and step out into the world with it, to make it real by the sheer force of belief and action. That’s what these kids were doing. I was proud of them, but didn’t say so, hoping not to seem like a dotty old grandma.

Keith arrived late that night. He drove an aging, blue school bus, painted with flowers and ‘Food Not Bombs’ on its side. It rattled down the dirt road under moonlight, past silent fields, looking only a little out of place. It was great to see him again. We talked about our lives; he had been touring Europe, meeting Food Not Bombs groups and speaking in Paris, Berlin, Amsterdam. I had been doing the usual - working, building (small b), and writing writing writing—boom boom boom. We spoke of old friends, that long-ago group of dreamers. We didn’t all stay with Food Not Bombs of course. We followed our individual paths. We became teachers, writers, dancers, clowns, even parents. The one common thread is that we’re all still trying to make the world a better place.

Not long after Keith’s visit, I went down to the riverside park on a Sunday where Durango FNB serves their weekly meal. The picnic tables were well populated, not only with people eating; this particular group had initiated a ‘Free Market.’ Blankets were spread on the grass, with items that you could just take if you wanted. I got a couple of books and some seed potatoes for the garden. Then I grabbed a plate and joined a few of the older folks on a bench.

“Isn’t this wonderful?” A woman asked me. I nodded, mouth full. 

“It’s so nice to see young people doing something good with their lives. It makes me think there’s still hope for this crazy world.” 

“Mmm Hmm,” I replied. “That’s the best thing about it.” 

The simple act of sharing is a powerful force. It is the opposite of greed. It exposes the lie of scarcity and necessary deprivation. If we can do away with greed, we will do away with hunger, poverty and war. Greed will not be wished out of existence, but we can shove it out with the practice of radical, public sharing. This book is a map, not the actual road. Take it and blaze your own trail to a better world. Let your dreams guide you and your heart always have the final say. If you do the best you can with whatever you have, you WILL find your way. Thirty years ago, that was just a bold belief, now I know it’s true. 

Have a good time, bon voyage. . . and thank you.



Jo Swanson

Food Not Bombs Co-founder

Durango, Colorado, August 24th, 2010





<img src="introduction-lithuania.jpg" alt="introduction-lithuania" style="width:850px;" />

​													Food Not Bombs sharing food at a protest for the right to squat empty buildings in Lithuania

## After the Riots

My young Serbian guide, Rebel Mouse, pried open a crack in the metal fence that surrounded the old brick mansion in central Belgrade. I followed him down a garden path, over piles of bricks to a skeleton of a stairway and up to the top floor. After stumbling around in the dark, Rebel Mouse suggested we enter one of the doors. Five young Food Not Bombs volunteers were sitting on water soaked mattresses in the only warm room of the building they called Rebel House. They warmed their hands over an electric heater. A dim yellow bulb lit our conversation which quickly turned to war. It started with their interest in the movie Bowling for Columbine. “Was Michael Moore’s movie true? Do Americans have guns?” they asked.

I had just finished Moore’s latest book, *Dude, Where’s My Country?* and reported he was supporting Wesley Clark for President. Emma was seventeen, dimly lit face framed with waves of natural red hair. She was dedicated to Food Not Bombs when she wasn’t busy with her courses in medicine. “Wesley Clark for President? You must be kidding? Wesley Clark destroyed our country!” Emma couldn’t contain herself.

“I went to stay at my mother’s apartment the first night of the war. She lives on the tenth floor. We sat nervously watching the war start on TV. My mother started to rock back and forth in her chair. I never saw her like this. As images of jets and missiles crossed the television screen she rocked faster and faster. Sirens were blaring outside. Then suddenly the apartment building rocked. The reporter announced that cruise missiles had destroyed a radio tower in the outskirts of Belgrade.

Then an explosion and everything went dark. My mother screamed that we had to get to the basement. I took her hand and led her down the stairs, feeling the wall with my other hand. Other residents were also stumbling down the stairs. A few minutes after we arrived in the basement, there was another explosion and screams at the door. Someone opened the metal latch and my uncle fell into the room covered in blood. Shards of glass sticking out of his face.”

Emma was also working as an intern at a hospital. “There are over 700 children in our hospital. The depleted uranium dropped on Serbia caused these children to be born without arms, legs, eyes . . . one child has an arm growing out of the top of his head. Americans should see what Wesley Clark has done.” They shared story after story about surviving the war, watching cruise missiles lumber slowly above the streets of Belgrade until they found their target. Everyone lost friends and family. Then I asked to use their toilet. “We use the crater in the room across the hall. Watch out that you don’t fall in.” Their toilet was made by a misfired cruise missile that crashed through the roof and failed to explode.

Back home in Taos, New Mexico, I was sharing lunch at the plaza. I noticed a friend, Mary, sitting with her bowl of rice and vegetables sobbing, tears streaming down her cheeks. 

“Keith, I can’t take it anymore. I just can’t believe we could lose our home.” She wiped the tears from the right side of her face. I sat next to her and gently held her. 

“We had tried the Obama Administration’s 'Making Home Affordable Program', but it wasn’t any help. Our application was shared with every mortgage modification company and they have been calling night and day with false promises of help.” Her husband couldn’t get work. No one was hiring builders and he was growing more frustrated each day. Her marriage was strained to the breaking point. She worried for her two young children. Her American dream had become the American nightmare.

This book was written to encourage people like yourself to participate in Food Not Bombs. To work to end the tragic brutality recounted in stories like those shared by Emma; to end an economic and political system that causes the suffering of people like my friend Mary. I hope you will be inspired to rush out and take action as soon as you finish reading this book. This book also comes out at a time when people are rising up against the austerity programs that are increasing hunger and poverty. 

High food prices are sparking riots and driving dictators from their thrones. While the "Arab Spring" gave hope to millions it has also made it clear that the most important actions can happen "after the riots," when corporate power seeks to replace the fallen government with another one that they also control. 

The old communist dictators became the new democratic leaders with the blessings of global capital. Young activists face the same crisis in North Africa and the Middle East. Filling the power vacuum created by natural, economic or political crisis with a compassionate, community-based system where everyone participates and no one goes hungry or lives in poverty, is the central objective of Food Not Bombs. 

More than collecting, cooking and sharing free food with the hungry and at protests, Food Not Bombs volunteers are practicing working together using consensus and implementing their visions independent of government or corporate control. This is one reason why we are considered a threat. Our efforts might be small, but they are the foundation of any sustained transition to a self-governed community. Everyone needs food. Rioters need food. Communities freed from corporate domination will need to eat and the skills required to collect and share food can be translated into the growing of food, providing safe fresh water, providing shelter, healthcare, education, entertainment and all the things a healthy, free community would desire. Our groups strive to make decisions using consensus so that everyone has access to determining the direction of the community. 

We seek to build interest in our ideas by always displaying our Food Not Bombs banner and providing literature at every meal to encourage dialogue on the subjects most affecting the public. We also provide healthy food, making it possible for people to be free to follow their dreams. Thereare many actions you can take. You don’t have to volunteer with our group,but you might start by participating with the Food Not Bombs movement until you find what moves you the most.

If your community doesn’t have a local group you can initiate a chapter. There are organic gardens to cultivate, homeless families to house,and exploitive policies and damaging corporate activities to stop. There are environmental, peace and social justice campaigns to plan, and activists to feed. There is an emergency and it will take everyone working together in earnest to implement the will take everyone working together in earnest to implement the
changes necessary for a sustainable future. The more people that practice these skills with Food Not Bombs ,the better prepared we will be to support one another after the riots and the less likely it will be in vain and our movement will be co-opted.

You can make a difference. When my friends and I first started Food Not Bombs, we couldn’t have imagined the impact it would have thirty years later. We couldn’t pay our rent, but we had the enthusiasm and desire to confront the policies of the Reagan Revolution. We tried to have the most impact possible on society by making the most of what little we had: time and imagination.

If we had any hope of changing society we had to make as powerful an impression as we could on as many people as our resources would allow. Our message would be ignored if it was confined to an office. We wouldn’t motivate anyone if we didn’t get their attention. So, we set out to show it was possible to feed the hungry, tasty, vegetarian meals under the banner Food Not Bombs while performing colorful spectacles illuminating the critical issues of the day to live music. To back up our ideas with deeds, we recovered soon to be discarded food and provided free groceries to hundreds of New England’s hungriest families at housing projects, soup kitchens and shelters. We provided meals to protesters and helped organize marches, rallies and other actions to protest the policies of Reagan and his corporate masters. It worked.

Before long, we attracted volunteers, food, donations, and invitations to share food at protests from Maine to Washington, DC. Our daily, dependable food collection and distribution built credibility. We walked our talk. At first we thought it would be fun, but experiencing the gratitude of the people we fed couldn’t have been more rewarding. We thought we might wake a few people up to the idea that our world would be a lot better if we redirected
some of our military spending towards domestic priorities, but we sure couldn’t have predicted that in thirty years there would be volunteers organizing for social change and feeding the hungry with Food Not Bombs groups in over 1,000 communities around the world.

People really got interested in starting local Food Not Bombs groups when we faced intense police interference. First, when police made nearly 100 arrests in San Francisco in 1988. With each wave of arrests and beatings after that, there came another wave of new Food Not Bombs groups. Every campaign against Food Not Bombs inspired the creation of more groups, and existing chapters responded by adding meals to their schedule or by organizing Homes Not Jails housing takeovers. Arrests in communities around California inspired more groups all over the world.

New arrests in other states were followed by the formation of more groups in every corner of the world. Economic crisis, strikes, wars, earthquakes hurricanes repressive laws free trade agreements racist attacks, animal abuse and threats to the environment motivated people to participate in Food Not Bombs. The arrests in Orlando, Florida and the support of cyber activists calling themselves the Peoples Liberation Front attracted more interest in Food Not Bombs. 

Most importantly, our literature tables and meals shared under the banner Food Not Bombs invited conversation and participation in our movement. The message of Food Not Bombs has travelled throughout the world by word of mouth, flyers, videos, fanzines, the Internet, web, music, news reports and, most importantly, by example. The joy of sharing free food with the hungry has inspired volunteers to overcome the obstacles of personal poverty and bureaucracy. The dream of a world at peace with abundance can seem within reach while doing the work of Food Not Bombs. Food Not Bombs is an antidote to the sense of hopelessness that many people can feel with the magnitude of today’s economic, political and environmental crisis. 

The joy of sharing food and working for social change is an inspiration for volunteers around the world. Both volunteers and the people that depend on our food have expressed that Food Not Bombs has changed their lives. A young mother with three girls came across the Food Not Bombs table I was staffing outside the Food Conspiracy Co-op on Fourth Avenue in Tucson, Arizona, in the days before the United States launched its “Shock and Awe” attack on Iraq. “Food Not Bombs! You saved my life!” As one of her girls tugged on her blouse to go and the other two squirmed, she opened her purse and pulled out a twenty dollar bill and placed it into our donation can. “I was on the way to the bridge over the Sacramento River to throw my girls and myself to our death when I happened upon your people sharing food in the park. They were so nice and treated us with respect. The other agencies humiliated me and I couldn’t take it anymore, but Food Not Bombs was different, so I gave up my plan to end my life.”

The three principles of Food Not Bombs provide our strength and make it possible for people to organize local chapters in a variety of cultures and economic situations. In particular, most social movements that start in the United States are quickly crushed or co-opted by corporate interests or the government  but this has not happened with Food Not Bombs. Our decentralized, non-hierarchical structure and use of consensus for direct democracy has protected Food Not Bombs from co-optation. The founders of Food Not Bombs thought that there might be a way to encourage the public to seek an end to war and poverty, with a living theater and mutual aid on the streets. No lengthy theories and long winded speeches to bore the public. 

We also made sure there would never be any charismatic leaders for the authorities to discredit or leadership for them to replace. Food Not Bombs is about action, reliability, respect, trust and relationships in the community. We are about making sure everyone is free to express their best self and has the food, clothing, healthcare and housing they deserve. In short, we were searching for a way to reach a public unfamiliar with alternative ways of organizing society and of relating to our fellow animal and human beings. Every bowl of free food that a Food Not Bombs volunteer shares with their community is a step in that direction.

I started writing this book in Washington, D.C., during the first years of the Obama administration, sitting in an air-conditioned cafe, on breaks from baking bread in a solar oven on Pennsylvania Avenue outside the White House to encourage, as our banner said, “The Change We Knead Now!” Congress was creating a national healthcare bill. America was struggling with unemployment at levels near those of the Great Depression, and over three million families foreclosed on their homes. There were reports of a global food crisis, food riots and escalating wars against Afghanistan and Pakistan.

In November 2009, I received news of the police confiscating a Food Not Bombs banner during the weekly meal in Flagstaff, Arizona, and reports that health inspectors had ordered the Lancaster, Pennsylvania chapter to stop feeding the hungry after a land-mine manufacturer made a complaint to city officials. Police took plates of food out of the hands of hungry people who were hoping to eat with Food Not Bombs in Concord, California. The city of Orlando took Food Not Bombs to the eleventh circuit court of appeals in Atlanta after the district court ruled the city had violated the group’s right to free expression and ordered the city to pay our attorneys $200,000. 

But Food Not Bombs lost the appeal on April12, 2011.The court ruled that the First Amendment rights of Food Not Bombs were “protected” since the Orlando law let us share food and literature twice a year per park. Phil and the other Florida activists called me in Taos reporting that the police had raided the Ft. Lauderdale Food Not Bombs house and that cities all over the state were about to pass laws against the sharing of meals with the hungry. I toured Florida in May 2011, helping each chapter collect, cook and share vegan meals. I also spoke to audiences about the history and principles of Food Not Bombs.

While free, I spent my nights sleeping in my 1987 Chevy van, drifting off to sleep listening to the news on the BBC: drone attacks mixed with environmental crisis including floods, droughts and extreme weather followed by reports of famine, economic failures in Europe, the United States and threats of another economic recession or even depression. The city of Orlando started arresting Food Not Bombs volunteers on June 1, 2011. I was arrested a second time on June 22nd and spent 17 days in the freezing cold Orange County jail. The news only became more dire once I was liberated from my media-free stay behind bars. One crisis after another encouraged me to complete this book and strengthen the Food Not Bombs movement.

Each year Food Not Bombs activists meet at regional and national gatherings and share news about the increase in people coming to eat and stories of new laws designed to drive poverty out of sight. Activists at the 2008 gathering in Nashville were already alarmed by poverty,but a few months after we met, the American economy crashed. As I travelled to help local Food Not Bombs groups, I saw more and more families arriving at our meals looking like ghosts from Dorothea Lange’s Great Depression photos. 

Children with smudged faces and large frightened eyes clung to their mother’s legs. “Food Not Bombs saved my son’s life,” explained one woman. “If it wasn’t for the Food Not Bombs kids in Gainesville,he would not have survived to birth.” People that had been living average middle class suburban lives were showing up to eat, having moved in with their families or friends after foreclosing on their homes. Some people reported that they were camping at the state park or told us they ate at Food Not Bombs so they would have enough money to pay their mortgage. 

The picture is even more devastating globally. In 2011,the number of people who went to bed hungry grew from 800 million to over a billion in less than a year, not because it was impossible to grow enough for all to eat,but because of the selfish policies of corporate leaders and the governments they control. Over 25,000 people perish each day unable to get enough food. According to the World Bank the cost of the seven most important food staples increased from December 2006 to March 2008 by 71 percent on average.

Rice and grain prices increased by 126 percent, forcing families in the poorest countries to spend between 60 to 80 percent of their income on food. Sir John Holmes, undersecretary general for humanitarian affairs and the UN’s emergency relief coordinator, noted in 2008, that, “The security implications [of the food crisis] should also not be underestimated as food riots are already being reported across the globe. Current food price trends are likely to increase sharply both the incidence and depth of food insecurity.”

The June 16, 2010 edition of The Guardian (United Kingdom) published a report on the future of food prices, saying that, “Food prices are set to rise as much as 40 percent over the coming decade amid growing demand from emerging markets and for biofuel production, according to a United Nations report today which warns of rising hunger and food insecurity.” Later that summer, fires and droughts in Russia reduced the country’s wheat harvest, causing the government to declare that they would not export any of that year’s harvest. 25 percent of the Russian wheat harvest was lost in 2010. 

Wheat prices climbed 50 percent in the two months after the United Nations announcement, in June, of their concern that food costs would increase by 40 percent during the next few years. Pakistan lost half a million tons of wheat as well as much of its rice crop because of the 2010 catastrophic floods. 

World rice prices increased in 2010 after 15 to 20 percent of Pakistan’s summer crop was destroyed in that country’s worst flood in a generation. There were droughts and then floods in Niger. The United Nations claims more than seven million people in Niger face food shortages because of the floods.
Eleven million people were facing starvation in Eastern Africa in the summer of 2011. These are signs that climate change is already contributing to hunger. It was reported, “The world may be on the brink of a major new food crisis caused by environmental disasters and rampant market speculation, the UN was warned today at an emergency meeting on food price inflation,” at the emergency meeting of the United Nations Food and Agriculture Organization on September 24, 2010. The “Food Outlook Report”, issued in November 2010 by the UN FAO, predicted prices would increase dramatically to levels as high as the levels in 2007 and 2008 that sparked riots. 

On January 5, 2011, news headlines announced “Food Prices Just Hit An All Time High.” World hunger was increasing. Commodities speculation, the increased use of food for ethanol, droughts, floods and extreme temperatures from the changing climate, and finally, corporate claims of ownership to tens of thousands of years of genetic history by patenting seeds were driving up food prices and hunger.

The cost of food is increasing from the seeds to the plate. The increased use of genetically engineered crops is one of the factors most contributing to the global rise in hunger. Corn seed prices rose 32 percent in 2009, and soybean seeds were up 24 percent from 2009 to 2010. Many farmers are not able to grow their own
seeds and are now forced to buy new seeds every season from companies like Monsanto. These seeds often require special chemicals to germinate and additional designer chemicals are required as fertilizers, herbicides and pesticides. Farmers are under pressure to borrow huge sums from banks to purchase the chemicals and equipment required in their contracts with suppliers and buyers.

Tens of thousands of farmers are forced off their land each year as they find it impossible to pay their loans. Hundreds of farmers commit suicide each year, notably in India, distraught over losing a way of life passed on from generation to generation. Thousands of acres of fertile land are sold to housing speculators or retail malls just to pay off loans incurred trying to farm genetically engineered produce.
With little in the way of competition, seed prices will increase. Just ten global corporations control 67 percent of the commercial seed industry with half of that being controlled by Monsanto, DuPont, and Syngenta. “We now believe that Monsanto has control over as much as 90 percent of (seed genetics). 

This level of control is almost unbelievable,” said Neil Harl, agricultural economist at Iowa State University, who has studied the seed industry for decades. “The upshot of that is that it’s tightening Monsanto’s control, and makes it possible for them to increase their prices long-term. And we’ve seen this happening the last five years, and the end is not in sight.” If a farmer tries to harvest their own seeds they can be sued by Monsanto. The company claims they get as many as 500 tips a year about farmers harvesting their own seeds or otherwise failing to pay Monsanto for seeds that may have been
contaminated with their genetic information. 

Percy Schmeiser might be the most famous farmer of Monsanto’s victims after the company contaminated his canola crop, canola that was handed down from his father and grandfather, cultivated for nearly a hundred years on his family’s farm in Bruno, Saskatchewan. Homan McFarling was also sued by Monsanto for planting seeds he had saved from the year before at his 5,000—acre farm in Shannon, Mississippi. Some farmers have even been sentenced to jail for “hiding” seeds from Monsanto, seeds contaminated by Monsanto.

This contamination is not limited to commercial farmers. Hopi elders claim that 30 percent of their ancient corn may have been cross pollinated with genetically engineered crops. Most genetically engineered crops are subsidized by U.S. taxpayers and fed to animals for use in fast food establishments or low-quality, packaged meals. According to the latest United Nations studies, industrial agriculture is responsible for nearly 70 percent of global freshwater consumption, using 38 percent of all land used for human purposes and causing 19 percent of the world’s greenhouse gas emissions. This is largely due to corporate farming of subsidized meat. United Nations FAO’s report “Livestock’s Long Shadow” claims that 18 percent of all greenhouse stock’s Long Shadow,” claims that 18 percent of all greenhouse gas emissions can be linked to animal agriculture.

These are only some of the examples that hunger and poverty is an increasingly urgent crisis. At the same time I was working on this book, the United States Congress was busy drafting a Federal Food Safety law. The U.S. Centers for Disease Control and Prevention estimates that 76 million people in the United States get sick every year with food-borne illness, resulting in over 5,000 deaths nationwide. Massive industrial farming operations are responsible for most cases of illness from food. Even so, the new Food Safety laws do not address the most dangerous aspects of industrial agriculture. 

Millions of animals living in cramped conditions fed genetically engineered grains provide the perfect environment to promote disease in giant stockyards, airless poultry sheds, and high-speed slaughter facilities. The Food Safety Modernization Act was drafted with the aid of Michael Taylor, the Monsanto executive responsible for the company’s campaigns to block the labelling of genetically engineered foods and conceal the dangers of rBGH growth hormones in dairy cattle. He not only helped draft the bill, but President Obama appointed him to be America’s first Food Safety Czar. The Food Safety Modernization Act is supported by corporate interests like the Snack Food Association, General Mills and Kraft Foods North America while opposed by the National Family Farm Coalition and the Small Farms Conservancy.

The cost of compliance for small organic farmers could force them out of business or underground at a time when many Americans are starting to support community based agriculture, buying their food at farmers markets as well as cultivating their own gardens. Some of the requirements might be both expensive and harmful to the nutrition of organically grown food. These policies also support the system that transports food an average of 1,500 miles from farm to plate.

Disruptions in access to oil are already leading to food shortages with an increase in cost of healthy meals, food shortages only worsen. Many of these regulations require large amounts of money for compliance and favor industrial food producers, while threatening to bankrupt local agriculture. The Food Safety Modernization Act could contribute to an increase in the cost of food and may reduce its nutritional value as well. As food corporations seek more power, more people will go hungry, and what food people can afford will not be any safer to consume.

While hunger, homelessness and poverty increase, military spending is at an all time high. World military expenditures were estimated to be $1.531 trillion in 2009. The United States spent over $800 billion on its war against Iraq, while it increased its spending to $6.7 billion per month in Afghanistan in 2010. Obama asked for even more military spending in 2011 while suggesting he would cut social security, medicare and other social services in a "budget compromise" with the Tea Party Republicans. 

Over 44 million Americans depended on food stamps as their country spent billions on its military. The global environment and economy are in crisis.
We are facing unimaginable horrors under the current corporate and political leadership. Corporations are free to buy American elections. European governments are following America’s example, cutting essential services while bailing out the transnational corporations.

Chaos rules in every corner of the earth. By the time you read, this things could be so much more dire that these facts might look good. There has never been a more important time for people to participate in the work of Food Not Bombs. This book is intended to replace our first book, Food Not Bombs, How to Feed the Hungry and Build Community, reflecting the changes Food Not Bombs has experienced over the past three decades. Food Not Bombs has grown to a global movement, so the details that once focused on the United States now seek to include all areas of the world from the ingredients in our recipes, legal issues that Food Not Bombs volunteers might face, to the data about hunger and food waste. 

Since we published our first book, there have been many changes in technology so this book contains information on how it is possible to use the web, internet and mobile phones as tools in the organizing of your local Food Not Bombs chapter. People have also suggested that the book would be more useful if it included recipes for smaller numbers of people; therefore, we have included recipes for groups of four to six people. During my travels, I also found that volunteers in other countries used different measurements so I try to include all the major ways of measuring out the ingredient in each recipe. 

I briefly cover all thirty years of Food Not Bombs in this volume but intend to write a more detailed account in a second book. This book also includes a timeline of major events in the history of Food Not Bombs and samples of flyers, forms and other resources your local group can use to be more effective.

So many volunteers have dedicated years of their own lives to working with Food Not Bombs that many people are qualified and experienced enough to write books about the movement from their perspective. This book is in no way meant to be the “official manual” or last word about Food Not Bombs. 

Food Not Bombs is a living, dynamic global project, and this book reflects some of what I have experienced during my thirty years of volunteering with this inspiring movement. I have done what I can to incorporate the many lessons, ideas and innovations people have shared with me over the decades. 

This book also reflects information I’ve been blessed to receive from Food Not Bombs activists in many areas of the world and from my work helping people start local chapters or assisting with the many problems groups encounter. It is an honor assisting with the many problems groups encounter. It is an honor to be a part of Food Not Bombs, and I hope you will be inspired to participate and consider dedicating your time and ideas to seeking an end to war, poverty, exploitation, and the destruction of the environment; while building a sustainable future free of coercion, violence, and suffering.

After seeing all that has been possible so far, who knows what the Food Not Bombs movement will achieve in the future? From our humble start sharing vegetarian meals during our performances in Harvard Square and daily food distribution to the residents of public housing in the Boston Area, Food Not Bombs has become a global movement sharing food and literature in over 1,000 communities.

By organizing Homes Not Jails housing occupations, free radio stations, Food Not Lawns community gardens, and setting up Really Really Free Markets in local parks,we are on our way to building the world we know is possible. Is it possible that Food Not Bombs will move from being a colorful subculture to becoming part of the foundation of a new society of peace, justice and wellbeing? Time will tell. Food Not Bombs can provide some direction after the riots, replacing the corporate dominated system with one that respects the ideas and rights of everyone in our community.

We have the possibility of building a world where everyone is safe and has all they require. This is our time to make something better and lasting We hope you will join us after something better and lasting. We hope you will join us after the riots and help us make a world with food not bombs.


Keith McHenry

Food Not Bombs co-founder


<img src="introduction-berlin.jpg" alt="introduction-berlin"  style="width:850px;" />			

Food Not Bombs shares lunch outside the Cologne Cathedral in Germany  

