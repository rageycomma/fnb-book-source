Illustrated, designed and written by Food Not Bombs co-founder Keith McHenry All photos taken by Food Not Bombs volunteers with exception of back cover picture of the author taken by Henry Grossman.

Preface by Food Not Bombs co-founder Jo Swanson. 

Copyright © 2012, 2013, 2014, 2015 by Keith McHenry. All rights reserved. 

ISBN 978-1-937276-06-5 

All Food Not Bombs activists can reproduce the materials in this book for use in their work and are invited to contact the Food Not Bombs office listed below to share news about the use of the content. All other inquiries regarding requests to reprint all or part of *HUNGRY FOR PEACE, How you can help end poverty and war with Food Not Bombs* should be sent to: 



**See Sharp Press** 

P.O. Box 1731

Tucson, AZ 85702 - 

USA 520-628-8720

www.seesharppress.com 



You can contact Food Not Bombs at: 



**Food Not Bombs** 

P.O. Box 424 Arroyo Seco, NM - USA 

1-800-884-1136 

www.foodnotbombs.net 



McHenry, Keith, 1957- Hungry for peace : how you can help end poverty and war with Food Not Bombs / Keith McHenry–Tucson, Ariz. : See Sharp Press, 2012. 

188p. ; 28 cm. 

ISBN 978-1-937276-06-5



Abstract: The de facto how-to manual of the international Food Not Bombs movement, which provides free food to the homeless and hungry and has branches in countries on every continent except Antarctica, this book describes at length how to set up and operate a Food Not Bombs chapter. The guide considers every aspect of the operation, from food collection and distribution to fund-raising, consensus decision making, and what to do when the police arrive. It contains detailed information on setting up a kitchen and cooking for large groups as well as a variety of delicious recipes. Accompanying numerous photographs is a lengthy section on the history of Food Not Bombs, with stories of the jailing and murder of activists, as well as pre-made handbills and flyers ready for photocopying.—Publisher’s Description. 

1. Food not Bombs (Organization) 2. Food relief. 3. Peace movements. 4. Hunger—Prevention. 



363.85