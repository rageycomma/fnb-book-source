## Materials for starting

### Cooking

|         Equipment         |                     Usage                      |                Where to source                | Min Quantity | Unit cost (£) | Type of payment | Total cost |
| :-----------------------: | :--------------------------------------------: | :-------------------------------------------: | :----------: | :-----------: | :-------------: | :--------: |
| Stainless Steel Stock pot | Cooking curries and other large-scale recipes. | Catering supply companies, homebrew companies |      1       |    £75.00     |     One-off     |   £75.00   |
|         Knife set         |               Cutting vegetables               |                 Supermarkets                  |      1       |    £30.00     |     One-off     |   £30.00   |
|       Chef's knife        |               Vegetable cutters                |                 Supermarkets                  |      1       |     £5.00     |   Per chopper   |   £5.00    |
|      Chopping board       |               Cutting vegetables               |                 Supermarkets                  |      1       |     £5.00     |   Per chopper   |   £5.00    |
|       Cooking tray        |          Cooking vegetables, pastries          |                 Supermarkets                  |      1       |     £3.00     |     One-off     |   £3.00    |
|  Vegetable speed peeler   |               Peeling vegetables               |                 Supermarkets                  |      1       |     £4.00     |     One-off     |   £4.00    |
|       Measuring jug       |          Measuring liquid quantities           |                 Supermarkets                  |      1       |     £1.50     |     One-off     |   £1.50    |
|        Tin opener         |           For opening tins and shit            |                 Supermarkets                  |      1       |     £3.00     |     One-off     |   £3.00    |
|           Whisk           |          For whisking things and shit          |                  Supermarket                  |      1       |     £3.00     |     One-off     |   £3.00    |
|       Wooden spoon        |              For stirring things               |                  Supermarket                  |      1       |     £3.00     |     One-off     |   £3.00    |
|         **Total**         |                                                |                                               |              |               |                 |  £132.50   |

### Common items

|          Equipment          |                         Usage                          |                       Where to source                        | Min Quantity | Unit cost (£) | Type of payment | Total cost |
| :-------------------------: | :----------------------------------------------------: | :----------------------------------------------------------: | :----------: | :-----------: | :-------------: | :--------: |
|        Folding Table        | Food prep, washing,  cooking, literature, drinks table |               Camping & catering supply stores               |      3       |    £40.00     |     One-off     |  £120.00   |
|         Cooler box          |                      Storing food                      |               Camping & catering supply stores               |      2       |    £100.00    |     One-off     |  £200.00   |
| 25 Litre Bucket with Spigot |                 Dispensing cold drinks                 |                    Homebrew supply shops                     |      1       |    £30.00     |     Initial     |   £30.00   |
|           Bucket            | Washing vegetables, cutting vegetables, washing dishes | Restaurants using large plastic buckets, warehouses, wholesalers. |      12      |     £0.00     |       N/A       |   £0.00    |
|   Plastic washing basket    |                 Storing washed plates                  |                Discount stores, supermarkets                 |      2       |    £10.00     |     Initial     |   £20.00   |
|            Tray             |                    Storing pastries                    |                Discount stores, supermarkets                 |      1       |     £5.00     |     Initial     |   £5.00    |
|          **Total**          |                                                        |                                                              |              |               |                 |  £375.00   |

#### Outdoor cooking

|             Equipment             |            Usage            | Where to source | Min Quantity | Unit cost (£) | Type of payment | Total cost |
| :-------------------------------: | :-------------------------: | :-------------: | :----------: | :-----------: | :-------------: | :--------: |
|          Gas Square Ring          |        Cooking food         |  Gas suppliers  |      4       |    £30.00     |     One-off     |  £120.00   |
| Gas (Purchase of container & gas) | Providing heat to cook food |  Gas suppliers  |      1       |    £65.00     |     Initial     |   £65.00   |
|           Gas (Refill)            | Providing heat to cook food |  Gas suppliers  |      1       |    £30.00     |    Repeating    |   £30.00   |
|             **Total**             |                             |                 |              |               |                 |  £215.00   |

