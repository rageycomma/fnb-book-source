# Chapter 1

## Solidarity Not Charity

**The principles of Food Not Bombs**

1. The food is always vegan or vegetarian rich or poor, stoned or sober.
2. Food Not Bombs has no formal leaders autonomous and makes decisions using the consensus process.
3. Food Not Bombs is dedicated to nonviolent social change.

The name Food Not Bombs states our most fundamental principle: society needs to promote life, not death. Implement the positive and end cooperation with the negative. Live in a world of abundance and stop fearing a future of scarcity. Celebrate with love, not hate; cooperation instead of domination; and compassion, not exploitation. 

Food Not Bombs devote our time and resources for the real security of food, shelter, education and healthcare instead of on weapons, military forces, prisons and social control. By sharing free food without restrictions, we illustrate the fact that there is an abundance of the things we need and that scarcity is a fiction that benefits a small minority. By sharing free meals under the banner Food Not Bombs, the founders of Food Not Bombs seek to educate the public with a message about the national priorities of the United States, pointing out that half the Federal budget was spent on the military, including debts from past wars, while millions went hungry every day.

Our society condones, and even promotes, violence and domination. Authority and power are primarily derived from the threat and use of violence. This affects our everyday lives through the constant threat of crime, domestic violence, police repression, war and even the threat of total annihilation from nuclear war and policies that speed damaging effects of global climate change. Such constant exposure to violence, including the threat of it, leads many people to feel hopeless, helpless, and to have low self-esteem. 

Economic exploitation is another common form of violence, and the fear of poverty and homelessness causes many to work long hours for low wages under stressful or dangerous conditions. All of this would be unnecessary if it were not for the need of the wealthy to dominate and control the public so they can maintain their power and prosperity.  The use of bombs is the ultimate tool of repression.

The principle goal of Food Not Bombs is to mobilize the people to withdraw their cooperation from this system of violence and coercion. To change society so no one is forced to be hungry. Food Not Bombs has never been considered a charity. Our volunteers are dedicated to taking nonviolent direct action for human rights, animal liberation, the environment and an end to exploitation and war.

The economic and political systems are themselves violent. Poverty is their most pervasive form of violence, and one expression of the violence of poverty is hunger. Over a billion people struggle to have enough to eat because of the decisions of business and government leaders. Trade agreements and laws forcing genetically engineered seeds and chemicals on farmers, commodity speculation, and taxpayer subsidies to agribusiness directly increase hunger.

[^ch1_ttip_gmos]: In the United States, former trade deal, the Transatlantic Trade and Investment Partnership (TTIP) with Europe, attempted to reduce weaken Europe's regulation of Genetically-Modified Organisms (GMOs), to increase profits for American corporations. The European Union had significantly stronger regulation on the use of these GMOs in foods, and the suggestion to lower these regulations failed, and the trade deal was considered obsolete in early 2019. 

The absence of democracy and access to information are the leading cause of hunger and poverty, not drought, pests and floods; and, therefore, the solution to ending world hunger is the dismantling of our political and economic systems. Of course, we need to make sure everyone has enough to eat today, but if we really want to end hunger we need more than charity, we need to withdraw our cooperation from those institutions responsible for global poverty and create our own democratic, self-sustainable communities.

The Food Not Bombs movement provides food and logistical support to activists protesting war, poverty, exploitation and domination, while replacing that abusive culture with one of abundance, cooperation, equality and peace. Our volunteers are working to replace an unsustainable political and economic system with a decentralized democratic set of grassroots solutions that address the real needs of everyone. Food Not Bombs is an organization devoted to developing positive personal, political, and economic alternatives. Revolutionaries are often depicted as working for the overthrow of the government by any means necessary. Food Not Bombs groups spend more resources building a sustainable future then attacking the current system, ready to help with a new vision, ready to create a world “after the riots.”

However, this does not mean we never struggle to end militarism and consumerism. By simply exerting our basic rights to free speech and association, we expose the exploitive violent nature of the political and economic system. In 1988, corporate and government leaders in the United States started to fear that our message and ideas could become popular and threaten their control so they organized a campaign of arrests, beatings, disinformation and litigation in an attempt to silence us.

After months of negotiating with the authorities, it became clear that they feared our message and fully meant it when the San Francisco Police told the media that they didn’t mind that we were feeding the hungry, but what did concern them was that we “are making a political statement, and that’s not allowed.”

It cannot be stressed enough that Food Not Bombs is not a charity and is working to inspire a dramatic change in society. Sharing food for free without restriction is a revolutionary act in a culture devoted to profit. Sharing food, clothing, time and compassion with no expectations has a powerful political impact. As the global economy and environment crash from one emergency to another, more people are discovering the folly of seeking wealth on the stock exchange or of relying on pensions, guns or gold and silver to provide security.

People really need safe food, water, air, shelter, clothing and, most importantly, community. Food Not Bombs volunteers are building new alternatives and life-affirming structures from the ground up. We want to replace the consumerist death culture with a cooperative culture of “Daycare Not Warfare,” “Clean Water Not Chemical Weapons,” “Food Not Lawns,” “Homes Not Jails,” “Really, Really, Free Markets,” “Bikes Not Bombs” and “Health Care Not Warfare.” 

The Food Not Bombs model can be applied to all aspects of our community. As community after community experiences one crisis after another, more people are adopting the principles that have made it possible for Food Not Bombs to flourish for over three decades.

Food Not Bombs volunteers respond to poverty and lack of self-esteem in at least two ways: 

Firstly, we provide food in an open, respectful way to whoever wants it without restriction, rich or poor, sober or not. We will not make people jump through any bureaucratic hoops designed to control, humiliate and often punish people without money. And Secondly, we invite people who receive the food to become involved in participating in the collection, cooking or sharing of the food. 

Food Not Bombs volunteers work in solidarity with many members of their community and encourage everyone’s participation in all aspects of our local chapters, including help with decision making. People eating with Food Not Bombs should never feel that they are in any way inferior to those who are sharing the food. We are all equal. 

This isn’t charity. This provides an opportunity for people to regain their power and recognize their ability to contribute and make a change. This could be one of the most important ways Food Not Bombs contributes to social change.

The idea of food recovery, or food “recycling,” is not the invention of Food Not Bombs. Individuals have been “gleaning,” 

[^ch1_gleaning]: The ancient tradition of collecting leftover crops from farms which are unused or surplus to requirements, thought to originate from the Jewish Torah and Christian Old Testament. In the modern day, crops are taken from "unprofitable" fields which are not harvested, or leftovers are taken from fields which were not intended to be sold. 

“dumpster diving,” or “skipping” to find food for a long time. From the “Diggers” of San Francisco in the last years of the 1960s to the “Diggers” of 1638 on Saint Georges Hill in England, and back tens of thousands of years to our hunting and gathering ancestors, people have been gleaning for food. Food Not Bombs is just a bit more organized and systematic about recovering surplus food. 

As a result, our volunteers can have more success at collecting larger amounts food, making it possible to make it available to more people. As the price of food increases, store owners are starting to poison their discarded products, locking dumpsters, paying security guards to keep people from receiving what has been thrown out, and installing trash compactors to discourage this practice. 

According to data published in 2009, over 1.3 billion tonnes of food was discarded yearly, roughly 20% of the world's food production, and enough to provide over 1 billion people with their full nutritional needs. In that same year, Oxfam published a study indicating over 1 billion people were going hungry. The same trend showed up in the United States too, with reports from the US Department of Agriculture indicating that close to 15% of the country was food insecure for most of the year, 

[^ch1_food_insecure]: Economic Research Report Number 108 - November 2010 - "Household Food Security in the United States, 2009", Nord, M., Coleman-Jensen, A., Andrews, M., and Carlson, S. (2009) United States Department of Agriculture. 
[^ch1_food_insecure_7_months]: Nord, et al. States "Typically, households classified as having very low food security experienced the condition in 7 months of the year, for a few days in each of those months."

while 31% of all food produced was wasted.

[^ch1_food_waste_141_trillion]: United States Department of Agriculture, et al. (2010) "*Estimates of Food Loss at the Retail and Consumer Levels*"

Food Not Bombs volunteers have overcome these obstacles by talking with produce workers, bakers and the owners of the smaller independent shops and by organizing the owners of the smaller independent shops, and by organizing the collection of their surplus. Even with our system of collection, Food Not Bombs groups find that some of the food is just not fit to eat and must be composted. 

So the final destination for some of what our volunteers collect ends up in the compost piles at local community gardens, but not until all the edible food is distributed to the public. Therefore, it is a radical political act in today’s wasteful society to recover large amounts of food in an organized and consistent manner to share with the hungry.

Although Food Not Bombs does not have a strict political platform, it has become identified with a general platform over the years. The three principles of Food Not Bombs were first formally suggested and adopted at the 1992 Food Not Bombs International Gathering in San Francisco. 

The first principle we developed was that the food is always vegan or vegetarian and shared with anyone without restriction whether they are drunk or sober, rich or poor. 

The second, was that each local group is independent and autonomous, has no leaders and uses consensus to make decisions, and that there is no president, headquarters, national office or board of directors.

And finally, the third principle was that every Food Not Bombs group is dedicated to taking nonviolent direct action and social change. Every individual and group chooses its own values and politics within these broad set of principles. 

This chapter presents some of this philosophy from the author’s own perspective gained from thirty years of interacting with Food Not Bombs activists from all over the world, but does not represent an “official” critique. It makes it possible for every chapter to adapt to the local conditions and time in which they are operating, while providing a form of continuity and political philosophy at the core of our effort to change society. 

## A New Society 

### Food is a right, not a privilege.  

Like many other people, we are concerned about the direction in which the world is headed. Domination, violence, and killing seem to be the predominant choices of those with the most power in our society. This is what we often refer to as a culture of death. Acceptance of war, nuclear annihilation, environmental destruction, and genocide are widespread in popular culture, religious institutions, think tanks, corporate board rooms and the halls of government. More than ever, this death culture is pushing the idea that it is necessary for young people to join the army and kill to have peace. 

We have a society that suggests we can shop our way to a sustainable environment and poison our bodies to health. Peace through the threat of war is impossible because using the threat of destruction as a way to prevent war is nothing but domination.

It is not lost upon us that the major contribution to stopping bombs is our withdrawal from the economic and political structures of the culture of death. As individuals, many of us engage in war-tax resistance. As an organization, we operate outside the dominant economic paradigm. We do not operate for a profit; in fact, we operate with very little money compared the value of the food we distribute. We generally ignore the authorities, having as little contact with them as possible; but, as we want exposure for our life-affirming alternatives, we never attempt to hide our intentions. 

It is unlikely that our plans and intentions could be hidden from the authorities, and in fact, public knowledge of our plans for non-violent direct action can become an essential aspect of our strategy. Dedication to our principles and an understanding of ourselves and our organizations as equals to the authorities are also essential to our ability to succeed at influencing positive social change. 

As Jonathan Schell wrote in the story "*The Other Superpower*":

> “The new superpower possesses immense power, but it is a different kind of power: not the will of one man wielding the 21,000-pound MOAB bomb but the hearts and wills of the majority of the world’s people.”  



<img src="chapter1-atlanta-mlk.jpg" alt="chapter1-atlanta-mlk" style="width: 850px;" />

Atlanta Food Not Bombs shares food near the birth place of Martin Luther King, Jr.  

<img src="chapter1-ethiopia.jpg" alt="chapter1-ethiopia" style="width: 850px;" />

People waiting for Addas Ababa Food Not Bombs near Saint Georges Church in Ethiopia  



### Non-violence in Theory

> “Don’t ask me how to burn down a building. Ask me how to grow watermelons or how to explain nature to a child. ”  

— Radical animal liberation activist Rod Coronado

Nonviolent resistance and non-cooperation can be the most effective way to achieve long-lasting, positive social change. There is dignity in nonviolent resistance, a dignity needed to sustain change. To be effective, it is often necessary to have large numbers of supporters and be persistent. Your intentions should be clear to both the institutions resisting change and the people you intend to attract as supporters. Honesty and truth are your most important allies. 

While often difficult, compassion and respect for your opponents combined with truth and honesty are essential to undermining the power of even the most ruthless and inhumane institutions. The longer and more violent the repression, the harder it is to remain compassionate, but by retaining your integrity in the face of extreme conditions, you will often attract increased popular support and weaken the resolve of those forces hired to end your efforts. 

Your participants will also maintain their sense of pride and increase their feeling of empowerment the longer they remain dedicated to nonviolence. non-violence means responding to situations of injustice with action. However, non-violence should not be confused with being passive. Withholding support and not cooperating with institutions and policies of violence, exploitation and injustice is a principal technique of non-violent resistance.

Just because your participants are dedicated to non-violence, you can’t expect the authorities to restrain their violence. Often the state will increase its violence if it believes your campaign is becoming successful, but as repression grows so will your support. What might seem like months and maybe years of failure can change suddenly.

Our chapter, *San Francisco Food Not Bombs* persisted in sharing food every week for seven years of near daily arrests that became violent; and, in 1995, the local media, which had originally been very critical of our position, announced support for their work and ridiculed city officials for wasting money and resources on stopping our meals. Their reports reflected the perspective of corporate and political leaders in the Bay Area that had come to see that it was not possible to stop Food Not Bombs.

Their persistence and dedication to non-violence attracted public support. Our volunteers would not give up, knowing that, if we did, future efforts to silence Food Not Bombs groups in other cities could seem possible. The San Francisco Police officers hired to arrest and beat us withdrew their support for the campaign against Food Not Bombs and started to see themselves as allies of our volunteers against those ordering the repression.

Seven years of building relationships with the officers caused the department leaders to first issue an order to “stop fraternizing” with our volunteers, and, once it became clear that they could not count on their patrolmen and women to continue arresting and beating us with enough enthusiasm they called off the whole project.

The officers grew to see we were honest, caring people and not the anti-American criminals bent on disobeying the law out of self-interest as they had been told by their superiors. Corporate and government leaders ended the campaign in order to protect their illusion of control; worried that if it became clear to the public that our persistence and relationships with the police had worked, more sectors of the community might withdraw their support for their authority. Imagine if the patrol officers were perceived by the public as refusing orders -- what would be next? It is extremely important that we act in a manner which is consistent with our values.

We want a future safe from violence and exploitation. It is never in our interest to use violence against the police or others. Campaigns of violence, even against the most unethical opponents, can be very disempowering -- even if it is successful, it creates a new institution that relies on violence to protect its authority. 

If the power changes hands after a campaign of nonviolence, it is more likely that the new institutions will have popular support and maintain their power through consent of the people. On the practical side, the dominant power usually can muster significantly more violent force than we can. The authorities strive to engage their opponents in a realm where they have the advantage, such as armed conflict. 

But, more philosophically, we don’t want to use power for domination in our efforts for social change. Imagine if San Francisco Food Not Bombs adopted a strategy of throwing rocks at the police when they came to arrest us. Instead of the public understanding our message that the government and corporations are intentionally redirecting resources towards the military while letting thousands go without food, the impression would have been that the police were justified in using violence to protect themselves and the community from criminals who have no respect for the public, let alone for the police.

The media reported extensively for years about how violent our volunteers were after several frustrated activists tossed bagels over a line of riot police to hungry people blocked from getting to the food. We want to create a society based upon human rights and human needs, not dependent on the threat and use of violence. We do not want to dominate. We want to seek the truth and support each other as we work to resolve conflicts without violence.

Even the food we choose to serve is an expression of our commitment to non-violence. We try to avoid using any animal products because we see the damage it does not only to the animals being exploited, but to ourselves, the environment and the economy. Mainstream food production is an inherently violent process, involving cruel living conditions and the slaughter of millions of animals and the poisoning of the air, water, soil and our own bodies with chemical fertilizers, pesticides and genetically engineered food. 

The meat and dairy industries control government policies that primarily serve their own financial interests and not those of the public. We couldn’t work for peace and ignore the violence of corporate food production that defines living beings as commodities and products to be manufactured and sold for profit. Our commitment to non-violence also extends to working to end the violence and pain of hunger and poverty; the fear of not being able to provide for oneself or one’s family and friends; the violence that over a billion people barely survive every day as they seek enough nutrition to live another day; food that is withheld or too costly to purchase so corporate leaders can maximize their profits and power.

### Non-Violence in Practice

Food Not Bombs is a unique example of non-violence in practice. When we were first arrested, supporters noted that sharing free food with the hungry is America’s version of India’s “Salt Marches.”

[^ch1_non_violence_salt_marches]: The Salt Marches were acts of civil disobedience against British rule in India initiated by Mahatma Gandhi in India in the 1930s. Indians disobeyed British laws which forced Indians to buy salt from Britain, by collecting en-masse the naturally-accumulated salt from Indian beaches.

Even in 1988, hunger in America had become a national embarrassment. Arresting volunteers for sharing vegan meals with the hungry was a graphic example of the misguided policies of corporate and political leaders in the United States. Sharing vegan food in defiance of the state requires popular participation, just as with any other form of nonviolent resistance. Food Not Bombs adopted the use of consensus to make decisions as a key part of our practice of nonviolence.

As an organization, we strive to be very inclusive. Our decision-making process invites a diversity of participants to shape the direction of each Food Not Bombs chapter. There is room for all respectful political perspectives and for everyone to express themselves. For some, the decision to work for Food Not Bombs is a total change in lifestyle. For others, the decision is expressed through a commitment to life-affirming values while continuing to work at a job for pay in mainstream society.

Still we are seeking to replace the current system based on violence. We try to value individuals for the contribution they offer, without any expectation that they be completely divorced from the status quo. The practice of non-violence includes respect for all cultural backgrounds. Our world is multicultural. Social and political structures should be sensitive to this reality. Challenging racism, classism, gender bias, homophobia and other oppressive behaviors is essential to creating a life-affirming, self-sustaining world.

Everyone needs to be engaged in multicultural work, and this includes the members of Food Not Bombs, as well as those with whom we come in contact, both on the street and within the other service and political organizations with which we work.  

